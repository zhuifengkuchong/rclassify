[
  "Part 1.0:-------------Part 1: ---------------",
  "Part 1: Document Elements: ",
  [
    {
      "Id": "DOCUMENT",
      "Type_30": {
        "onload": "function () {\n\t\t\tconsole.warn(\"window1.onloaded\");\n\t\t\ttimerFunction = function() {\n\t\t\t\tdocument.getElementById(\"outputTimerFunction\").innerHTML = \"timerFunction is called\";\n\t\t\t}\n\t\t}"
      },
      "Type_40": {},
      "Type_1": {},
      "Type_2": {}
    },
    {
      "Id": "HEAD"
    },
    {
      "Id": "Lu_Id_meta_1",
      "Element": "[object HTMLMetaElement]",
      "HTML": "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" id=\"Lu_Id_meta_1\">",
      "Type_1": {},
      "Type_2": {}
    },
    {
      "Id": "Lu_Id_title_1",
      "Element": "[object HTMLTitleElement]",
      "HTML": "<title id=\"Lu_Id_title_1\">Case4</title>",
      "Type_1": {},
      "Type_2": {}
    },
    {
      "Id": "BODY"
    },
    {
      "Id": "outputTimerFunction",
      "Element": "[object HTMLDivElement]",
      "HTML": "<div id=\"outputTimerFunction\"></div>",
      "Type_1": {},
      "Type_2": {}
    },
    {
      "Id": "Lu_Id_script_1",
      "Element": "[object HTMLScriptElement]",
      "HTML": "<script id=\"Lu_Id_script_1\">\n\t\tfunction image1Loaded() {\n\t\t\tconsole.warn(\"image1.onloaded\");\n\t\t\tsetTimeout(function() { timerFunction(); }, 10000);\n\t\t}\n\t\tvar timerFunction = 0;\n\t</script>",
      "Type_1": {},
      "Type_2": {}
    },
    {
      "Id": "Lu_Id_iframe_1",
      "Element": "[object HTMLIFrameElement]",
      "HTML": "<iframe src=\"case4_i.html\" tppabs=\"http://pswlab.kaist.ac.kr:81/case4_i.html\" id=\"Lu_Id_iframe_1\"></iframe>",
      "Type_1": {},
      "Type_2": {}
    },
    {
      "Id": "Lu_Id_br_1",
      "Element": "[object HTMLBRElement]",
      "HTML": "<br id=\"Lu_Id_br_1\">",
      "Type_1": {},
      "Type_2": {}
    },
    {
      "Id": "Lu_Id_script_2",
      "Element": "[object HTMLScriptElement]",
      "HTML": "<script src=\"dummy.js\" tppabs=\"http://pswlab.kaist.ac.kr:81/dummy.js\" id=\"Lu_Id_script_2\"></script>",
      "Type_1": {},
      "Type_2": {}
    },
    {
      "Id": "Lu_Id_script_3",
      "Element": "[object HTMLScriptElement]",
      "HTML": "<script id=\"Lu_Id_script_3\">  \n\t\twindow.onload = function() {\n\t\t\tconsole.warn(\"window1.onloaded\");\n\t\t\ttimerFunction = function() {\n\t\t\t\tdocument.getElementById(\"outputTimerFunction\").innerHTML = \"timerFunction is called\";\n\t\t\t}\n\t\t}\n\t\tfunction button1Clicked() {\n\t\t\tdocument.getElementById(\"outputButton1\").innerHTML = \"Button1 is clicked\";\n\t\t}\n\t\tfunction button2Clicked() {\n\t\t\tdocument.getElementById(\"outputButton2\").innerHTML = \"Button2 is clicked\";\n\t\t}\n\t\tfunction button3Clicked() {\n\t\t\tdocument.getElementById(\"outputButton3\").innerHTML = \"Button3 is clicked\";\n\t\t}\n\t</script>",
      "Type_1": {},
      "Type_2": {}
    },
    {
      "Id": "Lu_Id_script_4",
      "Element": "[object HTMLScriptElement]",
      "HTML": "<script src=\"dummy2.js\" tppabs=\"http://pswlab.kaist.ac.kr:81/dummy2.js\" id=\"Lu_Id_script_4\"></script>",
      "Type_1": {},
      "Type_2": {}
    },
    {
      "Id": "Lu_Id_button_1",
      "Element": "[object HTMLButtonElement]",
      "HTML": "<button onclick=\"button1Clicked()\" id=\"Lu_Id_button_1\">Button1</button>",
      "Type_1": {
        "onclick": "function onclick(event) {\nbutton1Clicked()\n}"
      },
      "Type_2": {},
      "Value": ""
    },
    {
      "Id": "Lu_Id_button_2",
      "Element": "[object HTMLButtonElement]",
      "HTML": "<button onclick=\"button2Clicked()\" id=\"Lu_Id_button_2\">Button2</button>",
      "Type_1": {
        "onclick": "function onclick(event) {\nbutton2Clicked()\n}"
      },
      "Type_2": {},
      "Value": ""
    },
    {
      "Id": "Lu_Id_button_3",
      "Element": "[object HTMLButtonElement]",
      "HTML": "<button onclick=\"button3Clicked()\" id=\"Lu_Id_button_3\">Button3</button>",
      "Type_1": {
        "onclick": "function onclick(event) {\nbutton3Clicked()\n}"
      },
      "Type_2": {},
      "Value": ""
    },
    {
      "Id": "outputButton1",
      "Element": "[object HTMLDivElement]",
      "HTML": "<div id=\"outputButton1\"></div>",
      "Type_1": {},
      "Type_2": {}
    },
    {
      "Id": "outputButton2",
      "Element": "[object HTMLDivElement]",
      "HTML": "<div id=\"outputButton2\"></div>",
      "Type_1": {},
      "Type_2": {}
    },
    {
      "Id": "outputButton3",
      "Element": "[object HTMLDivElement]",
      "HTML": "<div id=\"outputButton3\"></div>",
      "Type_1": {},
      "Type_2": {}
    },
    {
      "Id": "Lu_Id_img_1",
      "Element": "[object HTMLImageElement]",
      "HTML": "<img src=\"image3.jpg\" tppabs=\"http://pswlab.kaist.ac.kr:81/image1.jpg\" onload=\"image1Loaded()\" id=\"Lu_Id_img_1\" width=\"80\" height=\"80\">",
      "Type_1": {
        "onload": "function onload(event) {\nimage1Loaded()\n}"
      },
      "Type_2": {}
    }
  ],
  "Part 2.0:-------------Part 2: ---------------",
  "Part 2: Global Variables: ",
  {
    "name": "",
    "history": {},
    "locationbar": {},
    "menubar": {},
    "personalbar": {},
    "scrollbars": {},
    "statusbar": {},
    "toolbar": {},
    "status": "",
    "closed": false,
    "length": 1,
    "opener": null,
    "frameElement": null,
    "navigator": {
      "mozPay": null,
      "mozContacts": {},
      "mozApps": {}
    },
    "external": {},
    "applicationCache": {},
    "screen": {},
    "innerWidth": 1916,
    "innerHeight": 433,
    "scrollX": 0,
    "pageXOffset": 0,
    "scrollY": 0,
    "pageYOffset": 0,
    "screenX": 0,
    "screenY": 24,
    "outerWidth": 1916,
    "outerHeight": 851,
    "caches": {},
    "mozInnerScreenX": 0,
    "mozInnerScreenY": 123,
    "devicePixelRatio": 1,
    "scrollMaxX": 0,
    "scrollMaxY": 0,
    "fullScreen": false,
    "onwheel": null,
    "ondevicemotion": null,
    "ondeviceorientation": null,
    "ondeviceproximity": null,
    "onuserproximity": null,
    "ondevicelight": null,
    "console": {},
    "sidebar": "$Ref : external",
    "crypto": {},
    "onabort": null,
    "onblur": null,
    "onfocus": null,
    "oncanplay": null,
    "oncanplaythrough": null,
    "onchange": null,
    "onclick": null,
    "oncontextmenu": null,
    "ondblclick": null,
    "ondrag": null,
    "ondragend": null,
    "ondragenter": null,
    "ondragleave": null,
    "ondragover": null,
    "ondragstart": null,
    "ondrop": null,
    "ondurationchange": null,
    "onemptied": null,
    "onended": null,
    "oninput": null,
    "oninvalid": null,
    "onkeydown": null,
    "onkeypress": null,
    "onkeyup": null,
    "onloadeddata": null,
    "onloadedmetadata": null,
    "onloadstart": null,
    "onmousedown": null,
    "onmouseenter": null,
    "onmouseleave": null,
    "onmousemove": null,
    "onmouseout": null,
    "onmouseover": null,
    "onmouseup": null,
    "onpause": null,
    "onplay": null,
    "onplaying": null,
    "onprogress": null,
    "onratechange": null,
    "onreset": null,
    "onresize": null,
    "onscroll": null,
    "onseeked": null,
    "onseeking": null,
    "onselect": null,
    "onshow": null,
    "onstalled": null,
    "onsubmit": null,
    "onsuspend": null,
    "ontimeupdate": null,
    "onvolumechange": null,
    "onwaiting": null,
    "onmozfullscreenchange": null,
    "onmozfullscreenerror": null,
    "onmozpointerlockchange": null,
    "onmozpointerlockerror": null,
    "indexedDB": {},
    "onerror": null,
    "onafterprint": null,
    "onbeforeprint": null,
    "onbeforeunload": null,
    "onhashchange": null,
    "onlanguagechange": null,
    "onmessage": null,
    "onoffline": null,
    "ononline": null,
    "onpagehide": null,
    "onpageshow": null,
    "onpopstate": null,
    "onunload": null,
    "localStorage": {},
    "LU_document_onDOMContentLoaded": false,
    "LU_window_onload": false,
    "Lu_Fire_EventListner": true,
    "Lu_Attach_EventListner": true,
    "Lu_Attach_Timer": false,
    "Lu_Ajax": true,
    "Lu_src": true,
    "ehs": {
      "onload": "function () {\n\t\t\t// consoleLog_Lu(\"Non-racing event fire : \" + EventString);\n\t\t\tcurr_eh.call(arguments);\n\t\t\tcheck_event_handler_change();\n\n\t\t}"
    },
    "ehs_original": {
      "onload": "function () {\n\t\t\tconsole.warn(\"window1.onloaded\");\n\t\t\ttimerFunction = function() {\n\t\t\t\tdocument.getElementById(\"outputTimerFunction\").innerHTML = \"timerFunction is called\";\n\t\t\t}\n\t\t}"
    },
    "ehs2": {},
    "ehs2_original": {}
  },
  {
    "sessionStorage": {},
    "localStorage": {},
    "document.cookie": "__utma=1.1136014283.1421265277.1461355552.1461552076.16; __utmz=1.1421265277.1.1.utmcsr=(direct)|utmccn=(direct)|utmcmd=(none); _ga=GA1.1.1136014283.1421265277; __CT_Data=gpv=72; utag_main=v_id:014d5097bb390022bb7e40df73e80704c003600900b6f$_sn:40$_ss:0$_st:1462490249115$_pn:5%3Bexp-session$ses_id:1462488237915%3Bexp-session; s_pers=%20ttcp%3D1431663767597%7C1431663767597%3B%20s_visit%3D1%7C1440443991767%3B%20c_days%3D1440707037724%7C1535315037724%3B%20c_days_s%3DFirst%2520Visit%7C1440708837724%3B%20gpv_pageName%3Dcah%253Acorp%253Aus%253Aen%253Ahome%253A%7C1440709909062%3B%20s_c21%3Dus%257Ccorp%257Ces%257Chomepage%7C1461045027306%3B%20s_lv%3D1461124208065%7C1555732208065%3B%20s_lv_s%3DLess%2520than%25201%2520day%7C1461126008065%3B%20s_vnum%3D1462075200290%2526vn%253D5%7C1462075200290%3B%20s_invisit%3Dtrue%7C1461126008067%3B%20gpv_p17%3Dus%257Cabbvie%257Cjack%257C500%257Czhanglu623%2520seperate%257Cwww.abbvie.com%257Crace5c.txt%7C1461147009357%3B%20gpv_pn%3Dhttps%253A%252F%252Fwww.allstate.com%252Fhome%252Fhome.aspx%7C1461167229151%3B%20s_getNewRepeat%3D1461165429152-Repeat%7C1463757429152%3B%20s_nr%3D1461178489804-Repeat%7C1492714489804%3B%20gpv%3DHome%2520Page%7C1461180289840%3B%20s_vs%3D1%7C1461180290011%3B%20s_lastvisit%3D1461563119798%7C1556171119798%3B%20s_fid%3D7A5FDAAE98B123F2-0ED8CC237A3EE080%7C1524636000104%3B%20gpv_p6%3DHome%2520Page%7C1461565800269%3B; _sdsat_session_count=6; _sdsat_lt_pages_viewed=963; AMCV_21097FBB541195770A4C98A4%40AdobeOrg=673125628%7CMCMID%7C73759239731729645599035899866035322921; __ar_v4=OOWFVELTQVHCJNG3IDXM4D%3A20160420%3A3%7CEIOID5APZFCJZHURYDZIKP%3A20160420%3A3%7CLPMYMOMIJNCPBH5QZUV4YH%3A20160420%3A3%7CV2XJSNBLARAEZESW752CB2%3A20160418%3A1187%7CV6HSXHR75BEALJQGSMTE2O%3A20160418%3A1187%7CJ2T76SSDFNDZBFKKXKHY2F%3A20160418%3A1187%7CQSFOSFVOXNBDBLELKDTAQY%3A20160421%3A6%7CBLGFEZB2WJA75OHGCHPN6G%3A20160421%3A6%7C3GQW7AUH5RFHJEGFQTOGTE%3A20160421%3A6; style=null; _hjUserId=37318fb6-1f2b-4247-a498-d847e879dfd1; http://contentz.mkt922.com/lp/static/js/com.silverpop.iMAWebCookie=04e75c4b-29e6-c59a-030d-ddb75055aed6; SessionPersistence=CLIENTCONTEXT%3A%3DvisitorId%253D; __qca=P0-2048204550-1461181445795; __g_u=231758831350769_1_0_1_5_1461637314313_0; RES_TRACKINGID=764020943841630; ClrSSID=1461205316065-12124; ClrOSSID=1461205316065-12124; ClrSCD=1461205316066; insight_locale=en_US; iv=8f71b80a-d869-4830-a378-3391de6aa3cc; trwv.uid=kllysrvs-1461217204108-87513bc8%3A1; WT_FPC=id=27bec73d574be11e71e1461210113514:lv=1461210146472:ss=1461210113514; mp_f29d8eea04ddba6fe1bbf4e3f736122c_mixpanel=%7B%22distinct_id%22%3A%20%221543781d4cfa05-06a1718f8b4c9d-76266750-1b1a04-1543781d4d086a%22%2C%22%24initial_referrer%22%3A%20%22%24direct%22%2C%22%24initial_referring_domain%22%3A%20%22%24direct%22%7D; reliable_visitor=true; _msuuid_g1jsgou050=25922941-0CDE-4978-8E85-A775CD1899D3; v2nd=1461266164292"
  },
  "Part 3.0:-------------Part 3: ---------------",
  "Part 3.1: window.alert messages: ",
  "",
  "Part 3.2: framework error message: ",
  "",
  "Part 3.3: console.log message: ",
  "",
  "Part 3.4: console.warn message: ",
  "\"image1.onloaded\",\"window1.onloaded\"",
  "Part 3.5: console.error message: ",
  "",
  "Part 3.6: RacingEventDoneSequence: ",
  "Lu_Id_button_2__onclick,outputButton2__outputButton2__parsed",
  "Part 3.7: myVars.notExecutedEvent: ",
  {
    "Lu_Id_button_2__onclick": false,
    "outputButton2__outputButton2__parsed": false
  },
  "Part 3.8: The race info: ",
  {
    "race4": {
      "varName": "Tree[0x7f563b48e458]:outputButton2",
      "event1": {
        "id": "Lu_Id_button_2",
        "type": "onclick",
        "loc": "9",
        "executed": false,
        "eventString": "Lu_Id_button_2__onclick"
      },
      "event2": {
        "id": "outputButton2",
        "type": "outputButton2__parsed",
        "loc": "12",
        "executed": false,
        "eventString": "outputButton2__outputButton2__parsed"
      }
    }
  }
]