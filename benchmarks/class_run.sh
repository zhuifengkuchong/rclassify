!/bin/bash
set -x
FILE=$1
DIR=`dirname $FILE`
NAME=`basename $FILE`


echo step : instrument races into webpage
rm -rf $DIR/*html2
./init1_run.sh $1


echo step: classify
./classify.sh $DIR
