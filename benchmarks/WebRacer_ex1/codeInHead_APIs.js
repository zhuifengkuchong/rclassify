//var addEventListener_Lu = EventTarget.prototype.addEventListener;
//// store original
//EventTarget.prototype.addEventListener = function(type, fn, capture) {
//	this.addEventListener_Lu = addEventListener_Lu;
//	// step 1. check
//	var on_type = "on" + type;
//	var newfn;
//
//	print("-->addEventListener: " + getID(this) + " ->  " + type
//			+ "----------------------------------------\n");
//	print("Fn: "+ fn.toString());
//	// print("--Id : " + this + " , id: " + getID(this));
//	//
//	// print("--Is Document: " + isDocument(this));
//	// print("--Is Window : " + isWindow(this));
//	//
//	// print("--Type : " + on_type);
//	// print("--Fn :" + fn.toString());
//
//	// print("-----------------------------------------------------------------\n");
//
//	var EventString;
//	EventString = getID(this);
//
//	EventString = EventString + "__" + on_type;
//
//	// consoleLog_Lu(eventString + ": \n" + fn.toString());
//
//	if (!this.ehs) {
//		this.ehs = new Object();
//	}
//	if (!this.ehs_original) {
//		this.ehs_original = new Object();
//	}
//
//	if (!this.ehs2) {
//		this.ehs2 = {};
//	}
//	if (!this.ehs2_original) {
//		this.ehs2_original = {};
//	}
//
//	if (!this.ehs2[type]) {
//		this.ehs2[type] = [];
//	}
//
//	if (!this.ehs2_original[type]) {
//		this.ehs2_original[type] = [];
//	}
//
//	var fnString = "";
//	// fnString = fn.toString();
//	if (this.ehs2_original[type].indexOf(fn.toString()) == -1) {
//		// consoleLog_Lu("New eh2 added: " + type + " on : " + EventString);
//		this.ehs2_original[type].push(fn.toString());
//	} else {
//		// consoleLog_Lu("Old eh2 exist: " + type + " on : " + EventString);
//	}
//
//	var whetherAttach = true;
//	var waitForParse = false;
//	var this_done = false;
//	if (typeof myVars.notExecutedEvent[EventString] != "undefined") {
//		print("!!Racing event detected: " + EventString);
//
//		if (typeof myVars.toWaitLists[EventString] != "undefined") {
//			for (toWait in myVars.toWaitLists[EventString]) {
//				if (myVars.notExecutedEvent[myVars.toWaitLists[EventString][toWait]]) {
//					consoleWarn_Lu("Pre scan: " + EventString
//							+ " , need to wait for");
//					consoleWarn_Lu("--->>: "
//							+ myVars.toWaitLists[EventString][toWait]);
//					consoleWarn_Lu(" ");
//
//					if (myVars.toWaitLists[EventString][toWait]
//							.endsWith("_parsed")) {
//						waitForParse = true;
//					}
//
//				}
//			}
//		}
//
//		// /////////////////////////////////////////////////////////////////////////
//
//		function replace_e_Lu() {
//			// if (myVars.notExecutedEvent[EventString] == false)
//			// return;
//			consoleLog_Lu("EventString Trying to execute: " + EventString);
//			var Postpone_count = 1;
//
//			innerJob();
//
//			var waitForParse = false;
//
//			function innerJob() {
//
//				if (this_done == true) {
//					return;
//				}
//
//				var finishedWaiting = true;
//				// consoleLog_Lu(EventString);
//				if (typeof myVars.toWaitLists[EventString] != "undefined") {
//					for (toWait in myVars.toWaitLists[EventString]) {
//						if (myVars.notExecutedEvent[myVars.toWaitLists[EventString][toWait]]) {
//							consoleWarn_Lu("Attemp : " + Postpone_count
//									+ ", Runtime:" + EventString
//									+ " , need to wait for");
//							consoleWarn_Lu("--->>: "
//									+ myVars.toWaitLists[EventString][toWait]);
//							consoleWarn_Lu(" ");
//
//							Postpone_count++;
//							finishedWaiting = false;
//						}
//					}
//				}
//				if (Postpone_count > MAX_Postpone_count || finishedWaiting) {
//					consoleLog_Lu("--EventString is firing: " + EventString);
//					if (!myVars.toWaitLists[EventString]) {
//						consoleWarn_Lu(EventString
//								+ " , doesn't need to wait, Executed!!!");
//					} else {
//						consoleWarn_Lu(EventString
//								+ " , finished waiting, executed!!!");
//					}
//					// clearInterval(interval1);
//
//					consoleWarn_Lu("1-From : "
//							+ JSON.stringify(myVars.notExecutedEvent), null, 2);
//
//					RacingEventDone(EventString);
//					// myVars.notExecutedEvent[EventString] = false;
//
//					consoleWarn_Lu("To   : "
//							+ JSON.stringify(myVars.notExecutedEvent), null, 2);
//					consoleWarn_Lu(" ");
//					this_done = true;
//
//					try {
//						fn.call(arguments);
//					} catch (err) {
//						console.error(err.message);
//					}
//
//				}
//
//				if (finishedWaiting == false) {
//
//					myVars.postponedEvent.push(innerJob);
//					var interval1 = setTimeout_Lu(function(event) {
//						handlePostponedEvent();
//					}, timerInterval);
//				}
//
//			}
//
//		}
//
//		this.ehs2[type].push(replace_e_Lu.toString());
//		print("The event handler for has been replaced as racing event!");
//
//		if (on_type == "onload" || on_type == "onDOMContentLoaded") {
//			// if (waitForParse) {// if this is on onload or onDOMContentLoaded,
//			// // and need to wait for parsing event, then have
//			// // to auto-fire it.
//			// fireEventListnerAfterRegisteration = true;
//			// whetherAttach = false;
//			// } else {// else, means we are waiting for eh events, don't have
//			// to
//			// // fire that early, it will fire by itself
//			// fireEventListnerAfterRegisteration = true;
//			// whetherAttach = false;
//			// }
//			// fireEventListnerAfterRegisteration = false;
//			// whetherAttach = true;
//
//			if (!isWindow(this)) {
//				fireEventListnerAfterRegisteration = true;
//				whetherAttach = false;
//			} else {
//				fireEventListnerAfterRegisteration = false;
//				whetherAttach = true;
//			}
//
//		} else {// other type of eh will try-fire immediately
//			fireEventListnerAfterRegisteration = true;
//			whetherAttach = true;
//		}
//
//		if (myVars.DoNotFireEvent.indexOf(EventString) != -1) {
//			consoleWarn_Lu("Event handler Race! do not attach! Do not fire!!");
//			fireEventListnerAfterRegisteration = false;
//			whetherAttach = false;
//		}
//
//		if (fireEventListnerAfterRegisteration) {
//			consoleWarn_Lu("--Automatically Firing EH: " + EventString);
//
//			// var Lu_Fire_EventListner = true;
//			// var Lu_Attach_EventListner = true;
//			// var Lu_Attach_Timer = true;
//			// var Lu_Ajax = true;
//			// var Lu_src = true;
//
//			if (Lu_Fire_EventListner) {
//				consoleLog_Lu("****racing event fire : " + EventString);
//
//				if (EventString == "Lu_DOM__onDOMContentLoaded") {
//					document.addEventListener_Lu("DOMContentLoaded",
//							replace_e_Lu, false);
//				} else {
//					replace_e_Lu.call(arguments);
//				}
//
//			}
//			// this.addEventListener_Lu(type, new_fn, capture);
//		} else {
//			consoleWarn_Lu("--Do Not Automatically Firing EH: " + EventString);
//		}
//
//		if (whetherAttach) {
//			consoleWarn_Lu("--Attach: " + EventString);
//			this.addEventListener_Lu(type, replace_e_Lu, capture);
//		} else {
//			consoleWarn_Lu("--Do not attach : " + EventString);
//		}
//
//		fireEventListnerAfterRegisteration = true;
//
//	}
//
//	else {
//		// consoleLog_Lu("Not found, go default!");
//		function replace_e_Lu() {
//			// consoleLog_Lu("Non-racing event fire : " + EventString);
//			fn.call(arguments);
//			check_event_handler_change();
//		}
//		this.ehs2[type].push(replace_e_Lu.toString());
//		// print(EventString + " : has been replaced as default other event!");
//		this.addEventListener_Lu(type, replace_e_Lu, capture);
//		// print(element.ehs[eh_type].toString());
//	}
//
//	// ///////////////////////////////////////////////////////////////////////////////////
//
//}
//
//var removeEventListener_Lu = EventTarget.prototype.removeEventListener;
//// store original
//EventTarget.prototype.removeEventListener = function(type, fn, capture) {
//	print("-------------------------removeEventListener: " + type
//			+ "----------------------------------------\n");
//	this.removeEventListener_Lu = removeEventListener_Lu;
//	// step 1. check
//	var on_type = "on" + type;
//	var newfn;
//	// print("Id : " + this.id);
//	// print("Type: " + on_type);
//	// print("Fn :" + fn.toString());
//
//	if (!this.ehs2) {
//		this.ehs2 = {};
//	}
//	if (!this.ehs2[type]) {
//		// this.ehs2.push(type);
//		this.ehs2[type] = [];
//	}
//
//	if (!this.ehs2_original) {
//		this.ehs2_original = {};
//	}
//	if (!this.ehs2_original[type]) {
//		this.ehs2_original[type] = [];
//	}
//
//	if (this.ehs2_original[type].indexOf(fn.toString()) == -1) {
//		// print("remove eh: " + type + " From : " + this.id);
//		this.ehs2_original[type].pop(fn.toString());
//
//	} else {
//		// print("fail to remove eh" + type + " From : " + this.id);
//	}
//	//
//	// print(this.ehs2[type].indexOf("hi"));
//	// print(this.ehs2[type].indexOf(fn.toString()));
//
//	this.removeEventListener_Lu(type, fn, capture);
//}
//
//var appendChild_Lu = Node.prototype.appendChild;
//// store original]
//Node.prototype.appendChild = function(newElement) {
//	this.appendChild_Lu = appendChild_Lu;
////	consoleLog_Lu("---------------------------------------appendChild----------------------------------------\n");
//	// consoleLog_Lu("Node is: \n" + newElement.outerHTML);
//
//	// consoleLog_Lu("node.nodeValue is : \n" + newElement.outerHTML)
//	if (newElement.classList && !newElement.classList.contains("Lu_Inst")) {
//
//		if (newElement.tagName.toLowerCase() == 'script') {// this is for
//			// script-generated-script,
//			// using appendChild
//			if (newElement.src) {
//				consoleLog_Lu("Node is: \n" + newElement.src);
//				var original_src = newElement.src;// store the original src
//				var new_id = "";// give an id if there isn't
//				var pseudo_id = "";
//				// if (!newElement.hasAttribute("id")) {
//				var src_split = original_src.split("/");
//				new_id = "_script_" + src_split[src_split.length - 1];
//				// new_id = new_id.replace(/\./g, '_')
//				// newElement.setAttribute("id", new_id);
//				// } else {
//				// consoleWarn_Lu("woo woo woo, something is wrong!!!");
//				// new_id = newElement.id;
//				// }
//				var RacingEvent = new_id + "__onclick";
//				if (myVars.notExecutedEvent[RacingEvent]) {
//
//					consoleWarn_Lu("Instrument the script generated script!!!");
//					newElement.setAttribute("id", new_id);
//
//					newElement.removeAttribute("src");// remove the src from
//					// the script
//
//					newElement.onclick = function() {
//						consoleWarn_Lu("src reattached to element:\n"
//								+ newElement.outerHTML);
//						newElement.src = original_src;
//					};
//					newElement.setAttribute("finish_when_loaded", "true");
//					consoleLog_Lu(newElement.outerHTML);
//
//					check_event_handler_change();
//
//					newElement.addEventListener("load", function() {
//						consoleWarn_Lu(new_id + "-----------srcOnloaded");
//						consoleWarn_Lu("From : "
//								+ JSON.stringify(myVars.notExecutedEvent));
//						consoleWarn_Lu(myVars.notExecutedEvent[RacingEvent]);
//						RacingEventDone(RacingEvent);
//						// myVars.notExecutedEvent[RacingEvent] = false;
//						consoleWarn_Lu("To   : "
//								+ JSON.stringify(myVars.notExecutedEvent));
//						consoleWarn_Lu(" ");
//						console.warn("Async script is loaded, the src = "
//								+ original_src);
//					}, false);
//
//					// newElement.click();
//
//				}
//
//			}
//		}
//
//	}
//
//	this.appendChild_Lu(newElement);
//}
//var insertBefore_Lu = Node.prototype.insertBefore;
//// store original
//Node.prototype.insertBefore = function(newElement, referenceElement) {
//	this.insertBefore_Lu = insertBefore_Lu;
////	consoleLog_Lu("---------------------------------------insertBefore----------------------------------------\n");
//
//	// consoleLog_Lu("Node is: \n" + newElement.outerHTML);
//
//	// consoleLog_Lu("node.nodeValue is : \n" + newElement.outerHTML)
//	if (newElement.classList && !newElement.classList.contains("Lu_Inst")) {
//
//		if (newElement.tagName.toLowerCase() == 'script') {// this is for
//			// script-generated-script,
//			// using appendChild
//			if (newElement.src) {
//
//				consoleLog_Lu("Node is: \n" + newElement.src);
//
//				var original_src = newElement.src;// store the original src
//				var new_id = "";// give an id if there isn't
//				var pseudo_id = "";
//				// if (!newElement.hasAttribute("id")) {
//				var src_split = original_src.split("/");
//				new_id = "_script_" + src_split[src_split.length - 1];
//				// new_id = new_id.replace(/\./g, '_')
//				// newElement.setAttribute("id", new_id);
//				// } else {
//				// consoleWarn_Lu("woo woo woo, something is wrong!!!");
//				// new_id = newElement.id;
//				// }
//				var RacingEvent = new_id + "__onclick";
//				if (myVars.notExecutedEvent[RacingEvent]) {
//
//					consoleWarn_Lu("Instrument the script generated script!!!");
//					newElement.setAttribute("id", new_id);
//
//					newElement.removeAttribute("src");// remove the src from
//					// the script
//
//					newElement.onclick = function() {
//						consoleWarn_Lu("src reattached to element:\n"
//								+ newElement.outerHTML);
//						newElement.src = original_src;
//					};
//					newElement.setAttribute("finish_when_loaded", "true");
//					consoleLog_Lu(newElement.outerHTML);
//
//					check_event_handler_change();
//
//					newElement.addEventListener("load", function() {
//						consoleWarn_Lu(new_id + "-----------srcOnloaded");
//						consoleWarn_Lu("From : "
//								+ JSON.stringify(myVars.notExecutedEvent));
//						consoleWarn_Lu(myVars.notExecutedEvent[RacingEvent]);
//
//						RacingEventDone(RacingEvent);
//						// myVars.notExecutedEvent[RacingEvent] = false;
//						consoleWarn_Lu("To   : "
//								+ JSON.stringify(myVars.notExecutedEvent));
//						consoleWarn_Lu(" ");
//					}, false);
//
//					// newElement.click();
//
//				}
//
//			}
//		}
//
//	}
//
//	this.insertBefore_Lu(newElement, referenceElement);
//}
//
//var setTimeout_Lu = window.setTimeout;
//// store original
//window.setTimeout = function(fn, delay) {
//	// print("-------------------------setTimeout----------------------------------------\n");
//	// consoleLog_Lu(fn.toString());
//	// if (fn) {
//	// consoleLog_Lu("is a function");
//	// } else {
//	// consoleLog_Lu("is not a function");
//	// }
//
//	if (Lu_Attach_Timer == true) {
//		setTimeout_Lu(fn, delay);
//	} else {
//		// consoleLog_Lu("Do not setTimeOut");
//	}
//}
//
//var setInterval_Lu = window.setInterval;
//// store original
//window.setInterval = function(fn, delay) {
//	// print("-------------------------setInterval----------------------------------------\n");
//	// consoleLog_Lu(fn.toString());
//	// if (fn) {
//	// consoleLog_Lu("is a function");
//	// } else {
//	// consoleLog_Lu("is not a function");
//	// }
//
//	if (Lu_Attach_Timer == true) {
//		setInterval_Lu(fn, delay);
//	} else {
//		// consoleLog_Lu("Do not setInterval");
//	}
//}
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
////<script class="Lu_Inst" id="Lu_Inst_In_Head" src="codeInHeadInit0.js"></script>
////</head>
////<body></body>
////<head id="Lu_Id_head_1"> 
////<script>
////function isDocument(obj) {
////	if (obj instanceof HTMLDocument) {
////		return true;
////	}
////	if (Object.prototype.toString.call(obj) == "[object HTMLDocument]") {
////		return true;
////	}
////	return false;
////}
////
////function isWindow(obj) {
////	if (obj == window) {
////		return true;
////	}
////	if (Object.prototype.toString.call(obj) == "[object Window]") {
////		return true;
////	}
////	return false;
////}
////
////function getID(obj) {
////	if (obj == null)
////		return null;
////	if (isWindow(obj))
////		return "Lu_window";
////	if (isDocument(obj))
////		return "Lu_DOM";
////	if (obj.id)
////		return obj.id;
////	else
////		return null;
////}
////
////var addEventListener_Lu = EventTarget.prototype.addEventListener;
//////store original
////EventTarget.prototype.addEventListener = function(type, fn, capture) {
////	this.addEventListener_Lu = addEventListener_Lu;
////	// step 1. check
////	var on_type = "on" + type;
////	var newfn;
////
////	console.log("-->addEventListener: " + getID(this) + " ->  " + type
////			+ "----------------------------------------\n");
////
////}
////</script>