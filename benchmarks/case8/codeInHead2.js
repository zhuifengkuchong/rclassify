if (typeof jQuery != 'undefined') {
	Lu_jQuery_ajax = jQuery.ajax;
	jQuery.ajax = function(e, n) {
		console.log("jQuery.ajax");

		var ajax_url = e.url;
		var ajax_fn = e.success;
		var ajax_async = jQuery.ajaxSettings.async;

		console.log("Url   : " + ajax_url);
		console.log("Fn    : " + ajax_fn);
		console.log("Async : " + ajax_async);

		Lu_jQuery_ajax(e, n);

	}
}
var lu_ajax_open = window.XMLHttpRequest.prototype.open;

window.XMLHttpRequest.prototype.open = function(arg1, arg2, arg3) {
	console.log("window.XMLHttpRequest.prototype.open");
	this.lu_ajax_open = lu_ajax_open;

	var ajax_url = arg2;
	var ajax_fn = this.onreadystatechange;
	var ajax_async = arg3;

	console.log("Url   : " + ajax_url);
	console.log("Fn    : " + ajax_fn);
	console.log("Async : " + ajax_async);

	this.lu_ajax_open(arg1, arg2, arg3);
}
