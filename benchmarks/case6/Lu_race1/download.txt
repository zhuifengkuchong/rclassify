[
  "Part 1.0:-------------Part 1: ---------------",
  "Part 1: Document Elements: ",
  [
    {
      "Id": "DOCUMENT",
      "Type_1": {},
      "Type_2": {}
    },
    {
      "Id": "HEAD"
    },
    {
      "Id": "Lu_Id_meta_1",
      "Element": "[object HTMLMetaElement]",
      "HTML": "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" id=\"Lu_Id_meta_1\">",
      "Type_1": {},
      "Type_2": {}
    },
    {
      "Id": "Lu_Id_title_1",
      "Element": "[object HTMLTitleElement]",
      "HTML": "<title id=\"Lu_Id_title_1\">Case6</title>",
      "Type_1": {},
      "Type_2": {}
    },
    {
      "Id": "BODY"
    },
    {
      "Id": "Lu_Id_button_1",
      "Element": "[object HTMLButtonElement]",
      "HTML": "<button onclick=\"button1Clicked()\" id=\"Lu_Id_button_1\">Button1</button>",
      "Type_1": {
        "onclick": "function onclick(event) {\nbutton1Clicked()\n}"
      },
      "Type_2": {},
      "Value": ""
    },
    {
      "Id": "Lu_Id_script_1",
      "Element": "[object HTMLScriptElement]",
      "HTML": "<script id=\"Lu_Id_script_1\">\n\n\t\tfunction button1Clicked() {\n\t\t\tconsole.log(\"------------button1Clicked()\");\n\t\t  document.getElementById(\"outputButton1\").innerHTML = \"Button1 is clicked\";\n\n\t\t}\n\n\t</script>",
      "Type_1": {},
      "Type_2": {}
    },
    {
      "Id": "outputButton1",
      "Element": "[object HTMLDivElement]",
      "HTML": "<div id=\"outputButton1\"></div>",
      "Type_1": {},
      "Type_2": {}
    },
    {
      "Id": "Lu_Id_div_1",
      "Element": "[object HTMLDivElement]",
      "HTML": "<div style=\"display: none;\" id=\"Lu_Id_div_1\"> \n   <br id=\"Lu_Id_br_1\">0123456789101112131415161718192021222324252627282930313233343536373839404142434445464748495051525354555657585960616263646566676869707172737475767778798081828384858687888990919293949596979899100101102103104105106107108109110111112113114115116117118119120121122123124125126127128129130131132133134135136137138139140141142143144145146147148149150151152153154155156157158159160161162163164165166167168169170171172173174175176177178179180181182183184185186187188189190191192193194195196197198199200201202203204205206207208209210211212213214215216217218219220221222223224225226227228229230231232233234235236237238239240241242243244245246247248249250251252253254255256257258259260261262263264265266267268269270271272273274275276277278279280281282283284285286287288289290291292293294295296297298299300301302303304305306307308309310311312313314315316317318319320321322323324325326327328329330331332333334335336337338339340341342343344345346347348349350351352353354355356357358359360361362363364365366367368369370371372373374375376377378379380381382383384385386387388389390391392393394395396397398399400401402403404405406407408409410411412413414415416417418419420421422423424425426427428429430431432433434435436437438439440441442443444445446447448449450451452453454455456457458459460461462463464465466467468469470471472473474475476477478479480481482483484485486487488489490491492493494495496497498499500501502503504505506507508509510511512513514515516517518519520521522523524525526527528529530531532533534535536537538539540541542543544545546547548549550551552553554555556557558559560561562563564565566567568569570571572573574575576577578579580581582583584585586587588589590591592593594595596597598599600601602603604605606607608609610611612613614615616617618619620621622623624625626627628629630631632633634635636637638639640641642643644645646647648649650651652653654655656657658659660661662663664665666667668669670671672673674675676677678679680681682683684685686687688689690691692693694695696697698699700701702703704705706707708709710711712713714715716717718719720721722723724725726727728729730731732733734735736737738739740741742743744745746747748749750751752753754755756757758759760761762763764765766767768769770771772773774775776777778779780781782783784785786787788789790791792793794795796797798799800801802803804805806807808809810811812813814815816817818819820821822823824825826827828829830831832833834835836837838839840841842843844845846847848849850851852853854855856857858859860861862863864865866867868869870871872873874875876877878879880881882883884885886887888889890891892893894895896897898899900901902903904905906907908909910911912913914915916917918919920921922923924925926927928929930931932933934935936937938939940941942943944945946947948949950951952953954955956957958959960961962963964965966967968969970971972973974975976977978979980981982983984985986987988989990991992993994995996997998999 \n   <br id=\"Lu_Id_br_2\"> \n  </div>",
      "Type_1": {},
      "Type_2": {}
    },
    {
      "Id": "Lu_Id_br_1",
      "Element": "[object HTMLBRElement]",
      "HTML": "<br id=\"Lu_Id_br_1\">",
      "Type_1": {},
      "Type_2": {}
    },
    {
      "Id": "Lu_Id_br_2",
      "Element": "[object HTMLBRElement]",
      "HTML": "<br id=\"Lu_Id_br_2\">",
      "Type_1": {},
      "Type_2": {}
    },
    {
      "Id": "Lu_Id_button_2",
      "Element": "[object HTMLButtonElement]",
      "HTML": "<button onclick=\"button2Clicked()\" id=\"Lu_Id_button_2\">Button2</button>",
      "Type_1": {
        "onclick": "function onclick(event) {\nbutton2Clicked()\n}"
      },
      "Type_2": {},
      "Value": ""
    },
    {
      "Id": "outputButton2",
      "Element": "[object HTMLDivElement]",
      "HTML": "<div id=\"outputButton2\"></div>",
      "Type_1": {},
      "Type_2": {}
    },
    {
      "Id": "Lu_Id_div_2",
      "Element": "[object HTMLDivElement]",
      "HTML": "<div style=\"display: none;\" id=\"Lu_Id_div_2\"> \n   <br id=\"Lu_Id_br_3\">0123456789101112131415161718192021222324252627282930313233343536373839404142434445464748495051525354555657585960616263646566676869707172737475767778798081828384858687888990919293949596979899100101102103104105106107108109110111112113114115116117118119120121122123124125126127128129130131132133134135136137138139140141142143144145146147148149150151152153154155156157158159160161162163164165166167168169170171172173174175176177178179180181182183184185186187188189190191192193194195196197198199200201202203204205206207208209210211212213214215216217218219220221222223224225226227228229230231232233234235236237238239240241242243244245246247248249250251252253254255256257258259260261262263264265266267268269270271272273274275276277278279280281282283284285286287288289290291292293294295296297298299300301302303304305306307308309310311312313314315316317318319320321322323324325326327328329330331332333334335336337338339340341342343344345346347348349350351352353354355356357358359360361362363364365366367368369370371372373374375376377378379380381382383384385386387388389390391392393394395396397398399400401402403404405406407408409410411412413414415416417418419420421422423424425426427428429430431432433434435436437438439440441442443444445446447448449450451452453454455456457458459460461462463464465466467468469470471472473474475476477478479480481482483484485486487488489490491492493494495496497498499500501502503504505506507508509510511512513514515516517518519520521522523524525526527528529530531532533534535536537538539540541542543544545546547548549550551552553554555556557558559560561562563564565566567568569570571572573574575576577578579580581582583584585586587588589590591592593594595596597598599600601602603604605606607608609610611612613614615616617618619620621622623624625626627628629630631632633634635636637638639640641642643644645646647648649650651652653654655656657658659660661662663664665666667668669670671672673674675676677678679680681682683684685686687688689690691692693694695696697698699700701702703704705706707708709710711712713714715716717718719720721722723724725726727728729730731732733734735736737738739740741742743744745746747748749750751752753754755756757758759760761762763764765766767768769770771772773774775776777778779780781782783784785786787788789790791792793794795796797798799800801802803804805806807808809810811812813814815816817818819820821822823824825826827828829830831832833834835836837838839840841842843844845846847848849850851852853854855856857858859860861862863864865866867868869870871872873874875876877878879880881882883884885886887888889890891892893894895896897898899900901902903904905906907908909910911912913914915916917918919920921922923924925926927928929930931932933934935936937938939940941942943944945946947948949950951952953954955956957958959960961962963964965966967968969970971972973974975976977978979980981982983984985986987988989990991992993994995996997998999 \n   <br id=\"Lu_Id_br_4\"> \n  </div>",
      "Type_1": {},
      "Type_2": {}
    },
    {
      "Id": "Lu_Id_br_3",
      "Element": "[object HTMLBRElement]",
      "HTML": "<br id=\"Lu_Id_br_3\">",
      "Type_1": {},
      "Type_2": {}
    },
    {
      "Id": "Lu_Id_br_4",
      "Element": "[object HTMLBRElement]",
      "HTML": "<br id=\"Lu_Id_br_4\">",
      "Type_1": {},
      "Type_2": {}
    },
    {
      "Id": "Lu_Id_script_2",
      "Element": "[object HTMLScriptElement]",
      "HTML": "<script id=\"Lu_Id_script_2\">\n\n\t\tfunction button2Clicked() {\n\t\t\tconsole.log(\"------------button2Clicked()\");\n\t\t\tdocument.getElementById(\"outputButton2\").innerHTML = \"Button2 is called\";\n\n\t\t}\n\n\t</script>",
      "Type_1": {},
      "Type_2": {}
    }
  ],
  "Part 2.0:-------------Part 2: ---------------",
  "Part 2: Global Variables: ",
  {
    "name": "",
    "history": {},
    "locationbar": {},
    "menubar": {},
    "personalbar": {},
    "scrollbars": {},
    "statusbar": {},
    "toolbar": {},
    "status": "",
    "closed": false,
    "length": 0,
    "opener": null,
    "frameElement": null,
    "navigator": {
      "mozPay": null,
      "mozContacts": {},
      "mozApps": {}
    },
    "external": {},
    "applicationCache": {},
    "screen": {},
    "innerWidth": 1916,
    "innerHeight": 433,
    "scrollX": 0,
    "pageXOffset": 0,
    "scrollY": 0,
    "pageYOffset": 0,
    "screenX": 0,
    "screenY": 24,
    "outerWidth": 1916,
    "outerHeight": 851,
    "caches": {},
    "mozInnerScreenX": 0,
    "mozInnerScreenY": 123,
    "devicePixelRatio": 1,
    "scrollMaxX": 0,
    "scrollMaxY": 0,
    "fullScreen": false,
    "onwheel": null,
    "ondevicemotion": null,
    "ondeviceorientation": null,
    "ondeviceproximity": null,
    "onuserproximity": null,
    "ondevicelight": null,
    "console": {},
    "sidebar": "$Ref : external",
    "crypto": {},
    "onabort": null,
    "onblur": null,
    "onfocus": null,
    "oncanplay": null,
    "oncanplaythrough": null,
    "onchange": null,
    "onclick": null,
    "oncontextmenu": null,
    "ondblclick": null,
    "ondrag": null,
    "ondragend": null,
    "ondragenter": null,
    "ondragleave": null,
    "ondragover": null,
    "ondragstart": null,
    "ondrop": null,
    "ondurationchange": null,
    "onemptied": null,
    "onended": null,
    "oninput": null,
    "oninvalid": null,
    "onkeydown": null,
    "onkeypress": null,
    "onkeyup": null,
    "onload": null,
    "onloadeddata": null,
    "onloadedmetadata": null,
    "onloadstart": null,
    "onmousedown": null,
    "onmouseenter": null,
    "onmouseleave": null,
    "onmousemove": null,
    "onmouseout": null,
    "onmouseover": null,
    "onmouseup": null,
    "onpause": null,
    "onplay": null,
    "onplaying": null,
    "onprogress": null,
    "onratechange": null,
    "onreset": null,
    "onresize": null,
    "onscroll": null,
    "onseeked": null,
    "onseeking": null,
    "onselect": null,
    "onshow": null,
    "onstalled": null,
    "onsubmit": null,
    "onsuspend": null,
    "ontimeupdate": null,
    "onvolumechange": null,
    "onwaiting": null,
    "onmozfullscreenchange": null,
    "onmozfullscreenerror": null,
    "onmozpointerlockchange": null,
    "onmozpointerlockerror": null,
    "indexedDB": {},
    "onerror": null,
    "onafterprint": null,
    "onbeforeprint": null,
    "onbeforeunload": null,
    "onhashchange": null,
    "onlanguagechange": null,
    "onmessage": null,
    "onoffline": null,
    "ononline": null,
    "onpagehide": null,
    "onpageshow": null,
    "onpopstate": null,
    "onunload": null,
    "localStorage": {},
    "LU_document_onDOMContentLoaded": false,
    "LU_window_onload": false,
    "Lu_Fire_EventListner": true,
    "Lu_Attach_EventListner": true,
    "Lu_Attach_Timer": false,
    "Lu_Ajax": true,
    "Lu_src": true
  },
  {
    "sessionStorage": {},
    "localStorage": {},
    "document.cookie": "__utma=1.1136014283.1421265277.1461355552.1461552076.16; __utmz=1.1421265277.1.1.utmcsr=(direct)|utmccn=(direct)|utmcmd=(none); _ga=GA1.1.1136014283.1421265277; __CT_Data=gpv=72; utag_main=v_id:014d5097bb390022bb7e40df73e80704c003600900b6f$_sn:40$_ss:0$_st:1462490249115$_pn:5%3Bexp-session$ses_id:1462488237915%3Bexp-session; s_pers=%20ttcp%3D1431663767597%7C1431663767597%3B%20s_visit%3D1%7C1440443991767%3B%20c_days%3D1440707037724%7C1535315037724%3B%20c_days_s%3DFirst%2520Visit%7C1440708837724%3B%20gpv_pageName%3Dcah%253Acorp%253Aus%253Aen%253Ahome%253A%7C1440709909062%3B%20s_c21%3Dus%257Ccorp%257Ces%257Chomepage%7C1461045027306%3B%20s_lv%3D1461124208065%7C1555732208065%3B%20s_lv_s%3DLess%2520than%25201%2520day%7C1461126008065%3B%20s_vnum%3D1462075200290%2526vn%253D5%7C1462075200290%3B%20s_invisit%3Dtrue%7C1461126008067%3B%20gpv_p17%3Dus%257Cabbvie%257Cjack%257C500%257Czhanglu623%2520seperate%257Cwww.abbvie.com%257Crace5c.txt%7C1461147009357%3B%20gpv_pn%3Dhttps%253A%252F%252Fwww.allstate.com%252Fhome%252Fhome.aspx%7C1461167229151%3B%20s_getNewRepeat%3D1461165429152-Repeat%7C1463757429152%3B%20s_nr%3D1461178489804-Repeat%7C1492714489804%3B%20gpv%3DHome%2520Page%7C1461180289840%3B%20s_vs%3D1%7C1461180290011%3B%20s_lastvisit%3D1461563119798%7C1556171119798%3B%20s_fid%3D7A5FDAAE98B123F2-0ED8CC237A3EE080%7C1524636000104%3B%20gpv_p6%3DHome%2520Page%7C1461565800269%3B; _sdsat_session_count=6; _sdsat_lt_pages_viewed=963; AMCV_21097FBB541195770A4C98A4%40AdobeOrg=673125628%7CMCMID%7C73759239731729645599035899866035322921; __ar_v4=OOWFVELTQVHCJNG3IDXM4D%3A20160420%3A3%7CEIOID5APZFCJZHURYDZIKP%3A20160420%3A3%7CLPMYMOMIJNCPBH5QZUV4YH%3A20160420%3A3%7CV2XJSNBLARAEZESW752CB2%3A20160418%3A1187%7CV6HSXHR75BEALJQGSMTE2O%3A20160418%3A1187%7CJ2T76SSDFNDZBFKKXKHY2F%3A20160418%3A1187%7CQSFOSFVOXNBDBLELKDTAQY%3A20160421%3A6%7CBLGFEZB2WJA75OHGCHPN6G%3A20160421%3A6%7C3GQW7AUH5RFHJEGFQTOGTE%3A20160421%3A6; style=null; _hjUserId=37318fb6-1f2b-4247-a498-d847e879dfd1; http://contentz.mkt922.com/lp/static/js/com.silverpop.iMAWebCookie=04e75c4b-29e6-c59a-030d-ddb75055aed6; SessionPersistence=CLIENTCONTEXT%3A%3DvisitorId%253D; __qca=P0-2048204550-1461181445795; __g_u=231758831350769_1_0_1_5_1461637314313_0; RES_TRACKINGID=764020943841630; ClrSSID=1461205316065-12124; ClrOSSID=1461205316065-12124; ClrSCD=1461205316066; insight_locale=en_US; iv=8f71b80a-d869-4830-a378-3391de6aa3cc; trwv.uid=kllysrvs-1461217204108-87513bc8%3A1; WT_FPC=id=27bec73d574be11e71e1461210113514:lv=1461210146472:ss=1461210113514; mp_f29d8eea04ddba6fe1bbf4e3f736122c_mixpanel=%7B%22distinct_id%22%3A%20%221543781d4cfa05-06a1718f8b4c9d-76266750-1b1a04-1543781d4d086a%22%2C%22%24initial_referrer%22%3A%20%22%24direct%22%2C%22%24initial_referring_domain%22%3A%20%22%24direct%22%7D; reliable_visitor=true; _msuuid_g1jsgou050=25922941-0CDE-4978-8E85-A775CD1899D3; v2nd=1461266164292"
  },
  "Part 3.0:-------------Part 3: ---------------",
  "Part 3.1: window.alert messages: ",
  "",
  "Part 3.2: framework error message: ",
  "",
  "Part 3.3: console.log message: ",
  "\"------------button1Clicked()\"",
  "Part 3.4: console.warn message: ",
  "",
  "Part 3.5: console.error message: ",
  "",
  "Part 3.6: RacingEventDoneSequence: ",
  "Lu_Id_script_1__Lu_Id_script_1__parsed,Lu_Id_button_1__onclick",
  "Part 3.7: myVars.notExecutedEvent: ",
  {
    "Lu_Id_script_1__Lu_Id_script_1__parsed": false,
    "Lu_Id_button_1__onclick": false
  },
  "Part 3.8: The race info: ",
  {
    "race1": {
      "varName": "Window[26].button1Clicked",
      "event1": {
        "id": "Lu_Id_script_1",
        "type": "Lu_Id_script_1__parsed",
        "loc": "2",
        "executed": false,
        "eventString": "Lu_Id_script_1__Lu_Id_script_1__parsed"
      },
      "event2": {
        "id": "Lu_Id_button_1",
        "type": "onclick",
        "loc": "1",
        "executed": false,
        "eventString": "Lu_Id_button_1__onclick"
      }
    }
  }
]