!/bin/bash
set -x
FILE=$1
DIR=`dirname $FILE`
NAME=`basename $FILE`

echo step: Init0 instrumentation
./init0.sh $FILE


echo step : run webkit
./webkit.sh $FILE
rm $DIR/ER_actionlog_$NAME
cp /home/jack/src/webkit/ER_actionlog $DIR/ER_actionlog_$NAME
#./scripts/auto_explore_site.sh file://${FILE}_init0.html >& log


echo step : EventRacer races
./eventRacer.sh



echo step : copy races 
time ./copy_race.sh $DIR
rm $DIR/EventRacer_log

rm -rf $DIR/DebugInfo
mkdir $DIR/DebugInfo

cp /home/jack/src/EventRacer/EventRacer.log $DIR/DebugInfo/EventRacer.log
cp /home/jack/src/EventRacer/log $DIR/DebugInfo/log
cp /home/jack/src/EventRacer/DebugInfo/RaceLevels $DIR/DebugInfo/RaceLevels
cp /home/jack/src/webkit/DebugInfo/token $DIR/DebugInfo/token
cp /home/jack/src/webkit/DebugInfo/id $DIR/DebugInfo/id

