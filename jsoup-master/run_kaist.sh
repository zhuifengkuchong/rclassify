#!/bin/bash
#set -x

./run.sh /kaist/case1/ case1.html >& case1.log
./run.sh /kaist/case2/ case2.html >& case2.log
./run.sh /kaist/case3/ case3.html >& case3.log
./run.sh /kaist/case4/ case4.html >& case4.log
./run.sh /kaist/case5/ case5.html >& case5.log
./run.sh /kaist/case6/ case6.html >& case6.log
./run.sh /kaist/case7/ case7.html >& case7.log
./run.sh /kaist/case8/ case8.html >& case8.log


