#!/bin/bash
#set -x

Home=/home/jack/

Code_Path=/home/jack/Dropbox/VT/JavaScript/codes/jsoup-master/

Dir=$1/

File=$2

#echo $Home
#echo $Dir
#echo $File


Test_File=$Dir$File
#echo step 0 original 
#time firefox $Home/$Test_File & 
#sleep 10
#wmctrl -a Mozilla Firefox; xdotool key Ctrl+w;


echo $Test_File
#--------------------------------------------------------------------------------
#step 1: run jsoupInit0, instrument xxx.html to init0.html, which add unique ids, and add the script for detecting event listnerer to the head, executing the init0.html will generate a download file, which is the auto firing script for the next step.
echo step 1 instrument

time ./step1_jsoupInit0.sh $Test_File
echo step 2  race dection
Init0="_init0.html"
#Test_File_Init0=$Dir/Init0.html
Test_File_Init0=$Test_File$Init0
#echo $Test_File_Init0

#--------------------------------------------------------------
cd /home/jack/src/webkit
time ./scripts/auto_explore_site.sh file:///$Test_File_Init0  >& log
cd $Code_Path





cd /home/jack/src/EventRacer
rm -rf races
mkdir races
time ./run.sh exitWhenGetRaces >& $File.log

Races="_races"
Test_File_Races=$Test_File$Races/
rm -rf $Test_File_Races
mkdir $Test_File_Races

cp /home/jack/src/EventRacer/races/*.txt $Test_File_Races

exit
#---------------------------------------------------------------------

#--------------------------------------------------------------------------------
#step 2: remove aotufiring in dowload, executing the webpage init0.html in <firefox>

rm $Home/download/autoFiring.txt
time ./step2_firefox_init0.sh file:///$Test_File_Init0
#run firefox


#--------------------------------------------------------------------------------
#step 3: run jsoupInit1: instrument inst1.html, which will move the download/autoFiring.txt, and attach to the head, so when loading the page, the event listeners will automatically fire
echo step 3 race dection
mv $Home/download/autoFiring.txt $Dir/autoFiring.txt
time ./step3_jsoupInit1.sh $Test_File
exit

#--------------------------------------------------------------------------------
#step 4: execute the init1.html in webkit
echo step 4 race dection
cd /home/jack/src/webkit
time ./scripts/auto_explore_site.sh file:///$Home/$Dir/Init1.html 0 >& log

#--------------------------------------------------------------------------------
#step 5: execute eventRacer to genreate race files in eventRacer/races
echo step 5 race dection
cd /home/jack/src/EventRacer
rm -rf races
mkdir races
time ./run.sh exitWhenGetRaces >& $File.log

#--------------------------------------------------------------------------------
#step 6: move the race files into the test case folder
echo step 6 race dection
cd $Home/$Dir
rm -rf races
mkdir races
cp /home/jack/src/EventRacer/races/*.txt $Home/$Dir/races




#--------------------------------------------------------------------------------
#step 7: run jsoup, instrument to .html2
echo step 7 instrument
cd $Code_Path
rm $Home/$Dir/*html2
time ./run_part1_java.sh $Dir/$File


#--------------------------------------------------------------------------------
#step 8: run all the race_html2, show diff of each pair
echo step 8 classify
time ./run_part2_firefox.sh $Home/$Dir





#./run_part1_java.sh

#./run_part2_firefox.sh

