<script>
function isDocument(obj) {
	if (obj instanceof HTMLDocument) {
		return true;
	}
	if (Object.prototype.toString.call(obj) == "[object HTMLDocument]") {
		return true;
	}
	return false;
}

function isWindow(obj) {
	if (obj == window) {
		return true;
	}
	if (Object.prototype.toString.call(obj) == "[object Window]") {
		return true;
	}
	return false;
}

function getID(obj) {
	if (obj == null)
		return null;
	if (isWindow(obj))
		return "Lu_window";
	if (isDocument(obj))
		return "Lu_DOM";
	if (obj.id)
		return obj.id;
	else
		return null;
}
 
var addEventListener_Lu = EventTarget.prototype.addEventListener;
// store original
EventTarget.prototype.addEventListener = function(type, fn, capture) {
	this.addEventListener_Lu = addEventListener_Lu;
	// step 1. check
	var on_type = "on" + type;
	var newfn;

	console.log("-->addEventListener: " + getID(this) + " ->  " + type
			+ "----------------------------------------\n");

}
</script>