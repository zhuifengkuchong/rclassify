function autoFireEventListenerById(id, event) {
	var element = document.getElementById(id);
	var event1; // The custom event that will be created
	if (document.createEvent) {
		event1 = document.createEvent("HTMLEvents");
		event1.initEvent(event, true, true);
	} else {
		event1 = document.createEventObject();
		event1.eventType = event;
	}

	event1.eventName = event;

	if (document.createEvent) {
		element.dispatchEvent(event1);
	} else {
		element.fireEvent("on" + event1.eventType, event);
	}

	var onEvent = "on" + event1.eventType;
	if ((element[onEvent] || false) && typeof element[onEvent] == 'function') {
		element[onEvent](element);
	}
	
	console.warn("Fired "+ event +" on " + id);
}