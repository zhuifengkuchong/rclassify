var fireEventListnerAfterRegisteration = false;

if (!myVars) {
	var myVars = {};
}
myVars.count_check_event_handler_change = 0;

function insertEventFiring(element, type) {
	var s = document.createElement('script');
	s.type = 'text/javascript';
	var code = "" + element.id + "[\"" + type + "\"]();";
	try {
		s.appendChild(document.createTextNode(code));
		// insertAfter(s, element);
	} catch (e) {
		s.text = code;
		// insertAfter(s, element);
	}
	console.log(s);
	if (document.getElementsByTagName('body').length >= 1) {
		document.getElementsByTagName('body')[0].appendChild(s);
		console.log(document.getElementsByTagName('body')[0].outerHTML);
	}
}

function autoFireEventListener(element, event) {
	var event1; // The custom event that will be created
	if (document.createEvent) {
		event1 = document.createEvent("HTMLEvents");
		event1.initEvent(event, true, true);
	} else {
		event1 = document.createEventObject();
		event1.eventType = event;
	}

	event1.eventName = event;

	if (document.createEvent) {
		element.dispatchEvent(event1);
	} else {
		element.fireEvent("on" + event1.eventType, event);
	}
}

function autoFireEventListenerById(id, event) {
	var element = document.getElementById(id);
	var event1; // The custom event that will be created
	if (document.createEvent) {
		event1 = document.createEvent("HTMLEvents");
		event1.initEvent(event, true, true);
	} else {
		event1 = document.createEventObject();
		event1.eventType = event;
	}

	event1.eventName = event;

	if (document.createEvent) {
		element.dispatchEvent(event1);
	} else {
		element.fireEvent("on" + event1.eventType, event);
	}

	var onEvent = "on" + event1.eventType;
	if ((element[onEvent] || false) && typeof element[onEvent] == 'function') {
		element[onEvent](element);
	}
}

function insertElementIntoDom(element) {
	if (document.getElementsByTagName('body').length >= 1) {
		document.getElementsByTagName('body')[0].appendChild(element);
		// console.log(document.getElementsByTagName('body')[0].outerHTML);
	} else {
		document.getElementsByTagName('head')[0].appendChild(element);
	}
}
myVars.event_handler_types = [ "animationend", "animationiteration",
		"animationstart", "onabort", "onafterprint", "onbeforeprint",
		"onbeforeunload", "onblur", "oncanplay", "oncanplaythrough",
		"onchange", "onclick", "oncontextmenu", "oncopy", "oncut",
		"ondblclick", "ondrag", "ondragend", "ondragenter", "ondragleave",
		"ondragover", "ondragstart", "ondrop", "ondurationchange", "onemptied",
		"onended", "onerror", "onfocus", "onfocusin", "onfocusout",
		"onhashchange", "oninput", "oninvalid", "onkeydown", "onkeypress",
		"onkeyup",
		// "onload",
		"onloadeddata", "onloadedmetadata", "onloadstart", "onmessage",
		"onmousedown", "onmouseenter", "onmouseleave", "onmousemove",
		"onmouseout", "onmouseover", "onmouseup", "onmousewheel", "onoffline",
		"ononline", "onopen", "onpagehide", "onpageshow", "onpaste", "onpause",
		"onplay", "onplaying", "onpopstate", "onprogress", "onratechange",
		"onreset", "onresize", "onscroll", "onsearch", "onseeked", "onseeking",
		"onselect", "onshow", "onstalled", "onstorage", "onsubmit",
		"onsuspend", "ontimeupdate", "ontoggle", "ontouchcancel", "ontouchend",
		"ontouchmove", "ontouchstart", "onunload", "onvolumechange",
		"onwaiting", "onwheel" ];

var autoFiringElementTemplete = "<script class = \"Lu_Inst\"> setTimeout(function(){ autoFireEventListenerById(\"elementId\",\"eventType\"); }, 6000); </script>";
var autoFiringElements = "";
function check_event_handler_change_run0() {
	myVars.count_check_event_handler_change++;

	var items = document.querySelectorAll("*");
	var element, i, len = items.length;
	for (i = 0; i < len; i++) {
		element = items[i];
		if (element.classList.contains("Lu_Inst")) {
			continue;
		}
		if (!element.hasAttribute("id")) {
			continue;
		}
		var curr_eh;
		for ( var eh_type_index in myVars.event_handler_types) {

			// if(myVars.event_handler_types[eh_type_index] == "onclick" &&
			// element.hasAttribute("href")){
			//				
			// console.log("Ignored onclick on :"+ element.id);
			// continue;
			// }

			// if(myVars.event_handler_types[eh_type_index] == "onclick"){
			//				
			// console.log("Ignored onclick on :"+ element.id);
			// continue;
			// }

			if (element[myVars.event_handler_types[eh_type_index]]) {

				if (myVars.event_handler_types[eh_type_index] == "onclick") {
					// console
					// .log(element.id
					// + ": has : onclick: \n"
					// + element[myVars.event_handler_types[eh_type_index]]);

					if (element[myVars.event_handler_types[eh_type_index]]
							.toString().contains("http://")
							|| element[myVars.event_handler_types[eh_type_index]]
									.toString().contains("www.")) {
						console.log("Ignored onclick on :" + element.id);
						continue;
					}
				}
				console.log("Auto firing eh1: " + element.id + " has "
						+ myVars.event_handler_types[eh_type_index]+"\n"+element[myVars.event_handler_types[eh_type_index]]
						.toString()) ;
				autoFiringElements = autoFiringElements
						+ autoFiringElementTemplete.replace("elementId",
								element.id).replace(
								"eventType",
								myVars.event_handler_types[eh_type_index]
										.substring(2)) + "\n";
				// console.log(temp);

				// autoFireEventListener(element,
				// myVars.event_handler_types[eh_type_index].substring(2));
			}

			if (has_eh2(element, myVars.event_handler_types[eh_type_index])) {
				// if(myVars.event_handler_types[eh_type_index] == "onclick"){
				//					
				// console.log("Ignored onclick on :"+ element.id);
				// continue;
				// }
				// console.log("Auto firing eh2: " + element.id + " has "
				// + myVars.event_handler_types[eh_type_index]);
				autoFiringElements = autoFiringElements
						+ autoFiringElementTemplete.replace("elementId",
								element.id).replace(
								"eventType",
								myVars.event_handler_types[eh_type_index]
										.substring(2)) + "\n";
				// console.log(temp);

				// autoFireEventListener(element,
				// myVars.event_handler_types[eh_type_index].substring(2));
			}

		}
	}
	//dumpHTML();
}

myVars.windowsOnloaded = false;
function windowsOnloadedFn() {
	myVars.windowsOnloaded = true;
}

function has_eh2(element, type) {
	type = type.substring(2);
	if (element.ehs2) {
		if (element.ehs2[type]) {
			if (element.ehs2[type].length > 0) {
				return true;
			}
		}
	}
	return false;
}

var addEventListener_Lu = EventTarget.prototype.addEventListener;
// store original
EventTarget.prototype.addEventListener = function(type, fn, capture) {
	this.addEventListener_Lu = addEventListener_Lu;
	// step 1. check
	var on_type = "on" + type;
	var newfn;
	// print("--Id : " + this.id);
	// print("--Type: " + on_type);
	// print("--Fn :" + fn.toString());

	if (this.id) {
//		if (on_type == "onclick") {
//			console.log(this.id + ": has : onclick: \n" + fn.toString());
//		}

		if ((on_type == "onclick")
				&& (fn.toString().contains("http://") || fn.toString()
						.contains("www."))) {
			console.log(this.id + ": has ignored: onclick: \n" + fn.toString());
		} else {
			console.log(this.id + ": has :"+ on_type+ "\n" + fn.toString());
			if (!this.ehs2) {
				this.ehs2 = [];
			}

			if (!this.ehs2[type]) {
				this.ehs2.push(type);
				this.ehs2[type] = [];
			}
			if (this.ehs2[type].indexOf(fn.toString()) == -1) {
				// print("new eh2 added: " + type + " on : " + this.id);
				this.ehs2[type].push(fn.toString());
			} else {
				// print("old eh2 exist: " + type + " on : " + this.id);
			}
		}
	}
	//
	// print(this.ehs2[type].indexOf("hi"));
	// print(this.ehs2[type].indexOf(fn.toString()));

	this.addEventListener_Lu(type, fn, capture);
}

var removeEventListener_Lu = EventTarget.prototype.removeEventListener;
// store original
EventTarget.prototype.removeEventListener = function(type, fn, capture) {
	this.removeEventListener_Lu = removeEventListener_Lu;
	// step 1. check
	var on_type = "on" + type;
	var newfn;
	// print("Id : " + this.id);
	// print("Type: " + on_type);
	// print("Fn :" + fn.toString());

	if (!this.ehs2) {
		this.ehs2 = [];
	}

	if (!this.ehs2[type]) {
		this.ehs2.push(type);
		this.ehs2[type] = [];
	}
	if (this.ehs2[type].indexOf(fn.toString()) != -1) {
		// print("remove eh: " + type + " From : " + this.id);
		this.ehs2[type].pop(fn.toString());
	} else {
		// print("fail to remove eh" + type + " From : " + this.id);
	}
	//
	// print(this.ehs2[type].indexOf("hi"));
	// print(this.ehs2[type].indexOf(fn.toString()));

	this.removeEventListener_Lu(type, fn, capture);
}

function print(input_string) {
	console.log(input_string);
}
function debug(input_string) {
	print(input_string);
}

function set_attr(id, atr, value) {
	document.getElementById(id).atr = value;
}

function triggerEvent(el, type) {
	if ((el[type] || false) && typeof el[type] == 'function') {
		el[type](el);
	}
}

function dumpHTML() {
	download_dump("autoFiring.txt", autoFiringElements);
}

function download_dump(filename, text) {
	document.getElementById('link').download = filename;
	document.getElementById('link').href = 'data:text/plain;charset=utf-8,'
			+ encodeURIComponent(text);
	document.getElementById('link').click();
}
