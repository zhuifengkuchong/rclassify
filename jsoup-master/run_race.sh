#!/bin/bash
#set -x

Home_Path=/home/jack/Dropbox/VT/JavaScript/codes/

Curr_Path=/home/jack/Dropbox/VT/JavaScript/codes/jsoup-master/

HTML_Path=$1

HTML_File=$2

#echo $Home_Path
#echo $HTML_Path
#echo $HTML_File


Test_File=$HTML_Path$HTML_File



#echo $Test_File
#--------------------------------------------------------------------------------
#step 1: run jsoupInit0, instrument xxx.html to init0.html, which add unique ids, and add the script for detecting event listnerer to the head, executing the init0.html will generate a download file, which is the auto firing script for the next step.
echo step 1 instrument

time ./step1_jsoupInit0.sh $Test_File

#--------------------------------------------------------------------------------
#step 2: remove aotufiring in dowload, executing the webpage init0.html in <firefox>
echo step 2  race dection
Test_File_Init0=$HTML_Path/Init0.html
#echo $Test_File_Init0
rm $Home_Path/download/autoFiring.txt
time ./step2_firefox_init0.sh file:///$Home_Path/$Test_File_Init0

#--------------------------------------------------------------------------------
#step 3: run jsoupInit1: instrument inst1.html, which will move the download/autoFiring.txt, and attach to the head, so when loading the page, the event listeners will automatically fire
echo step 3 race dection
mv $Home_Path/download/autoFiring.txt $HTML_Path/autoFiring.txt
time ./step3_jsoupInit1.sh $Test_File


#--------------------------------------------------------------------------------
#step 4: execute the init1.html in webkit
echo step 4 race dection
cd /home/jack/src/webkit
time ./scripts/auto_explore_site.sh file:///$Home_Path/$HTML_Path/Init1.html 0 >& log

#--------------------------------------------------------------------------------
#step 5: execute eventRacer to genreate race files in eventRacer/races
echo step 5 race dection
cd /home/jack/src/EventRacer
rm -rf races
mkdir races
time ./run.sh exitWhenGetRaces >& $HTML_File.log

#--------------------------------------------------------------------------------
#step 6: move the race files into the test case folder
echo step 6 race dection
cd $Home_Path/$HTML_Path
rm -rf races
mkdir races
cp /home/jack/src/EventRacer/races/*.txt $Home_Path/$HTML_Path/races



