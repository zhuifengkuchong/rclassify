import java.io.Console;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.text.Element;

import org.jsoup.Jsoup;
import org.jsoup.instrumentation.LuInst;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Node;

import javax.script.*;

//import org.mozilla.javascript.*;

public class jsoup_bk {
	static HashMap<String, Integer> Id_LOC = new HashMap<String, Integer>();
	static HashMap<String, String> SRC_Id = new HashMap<String, String>();
	static HashMap<String, String> replaceRaceInfo = new HashMap<String, String>();
	static HashMap<String, String> replaceRaceInfo_prioroty = new HashMap<String, String>();
	static ArrayList<Node> all_Nodes = new ArrayList<Node>();

	public static void main(String[] args) throws ScriptException {
		// TODO Auto-generated method stub

		LuInst.getInstance().FilePath = "/home/jack/Dropbox/VT/JavaScript/codes/";

		String UrlPath = "";
		if (args.length > 0) {
			// UrlPath = LuInst.getInstance().FilePath + args[0];
			UrlPath = "" + args[0];
			System.out.println(UrlPath);
		}
		// File input = new File(LuInst.getInstance().FilePath
		// + "case14/pswlab.kaist.ac.kr/test_addeventlistner.html");

		if (UrlPath == "")
			return;

		File input = new File(UrlPath);// the entry html file
		System.out.println("The html file path: " + input.getAbsolutePath());

		File parentPath = input.getAbsoluteFile().getParentFile();
		System.out.println("The folder path: " + parentPath.getAbsolutePath());

		File racePath = new File(parentPath.getAbsolutePath() + "/races");
		System.out.println("The races path: " + racePath.getAbsolutePath());
		System.out
				.println("***************************************************************************************************");

		File[] raceFiles = racePath.listFiles();
		Arrays.sort(raceFiles);
		for (File race : raceFiles) {

			System.out
					.println("----------------------------------------------------------------------------------------------------------------------");
			Id_LOC = new HashMap<String, Integer>();
			SRC_Id = new HashMap<String, String>();
			LuInst.getInstance().raceText = "";
			LuInst.getInstance().raceInfoFileString.clear();
			String raceFileString = race.getAbsolutePath();
			if (!raceFileString.endsWith(".txt"))
				continue;
			// System.out.println("\n--The race: " + raceFileString);

			// add the current race file, no matter a or b
			LuInst.getInstance().raceInfoFileString.add(raceFileString);

			String raceIdString = raceFileString.substring(0,
					raceFileString.length() - 5);
			// add the other race files, but will only add a, because that's the
			// original order
			for (File race2 : raceFiles) {

				String raceFileString2 = race2.getAbsolutePath();

				if (!raceFileString2.endsWith(".txt"))
					continue;
				if (raceFileString2.endsWith("b.txt"))
					continue;

				String raceIdString2 = raceFileString2.substring(0,
						raceFileString2.length() - 5);
				if (raceIdString.endsWith(raceIdString2))
					continue;

				// System.out.println("----The Other race file: "
				// + raceFileString2);
				LuInst.getInstance().raceInfoFileString.add(raceFileString2);

			}

			try {
				Document doc = Jsoup.parse(input, "UTF-8",
						"http://example.com/");
				System.out
						.println("---------------------------------------------------------");
				// System.out.println(doc.outerHtml());
				try {

					// input.renameTo(new File(input.getAbsolutePath()
					// + "_Lu_BackUp"));
					// Inst_addUniqueId(doc);
					// FileWriter fw0 = new FileWriter(input.getAbsolutePath());
					// fw0.write(doc.outerHtml());
					// fw0.close();

					all_Nodes.clear();
					Get_All_Element_0(doc, doc);
					// Get_All_Element2_0(doc);

					Inst_inHead_1(doc, parentPath.getAbsolutePath());

					Inst_afterEachTag_2(doc);

					Inst_races_3(doc);
					Inst_parsed_tags_4(doc);

					Inst_src_rewriting_5(doc);

					Inst_afterBody_6(doc);

					Inst_Delay_Parsing_7(doc);

					Inst_change_eh_This_8(doc);

					String outputPathString = parentPath.getAbsolutePath()
							+ "/" + race.getName() + ".html2";
					File newTextFile = new File(outputPathString);
					System.out.println("----The output: " + outputPathString);

					FileWriter fw = new FileWriter(newTextFile);
					fw.write(doc.outerHtml());
					fw.close();

				} catch (IOException iox) {
					// do stuff with exception
					iox.printStackTrace();
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

	}

	static Integer getLOC(String id) {
		if (Id_LOC.containsKey(id)) {
			return Id_LOC.get(id);
		} else {
			return -1;
		}
	}

	static void Inst_inHead_1(Document doc, String parentPath) {
		Node headNode = doc.childNode(doc.childNodeSize() - 1).childNode(0);
		Node bodyNode = doc.childNode(doc.childNodeSize() - 1).childNode(2);

		try {
			if (LuInst.getInstance().codeInHead.isEmpty()) {

				LuInst.getInstance().codeInHead = LuInst
						.getInstance()
						.readFile(
								LuInst.getInstance().FilePath
										+ "jsoup-master/Inst/InsertCodeInHead.js");
			}
			copyFileUsingFileChannels(new File(LuInst.getInstance().FilePath
					+ "jsoup-master/Inst/codeInHead.js"), new File(parentPath
					+ "/codeInHead.js"));

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (headNode.childNodeSize() > 0)
			headNode.childNode(0).after(LuInst.getInstance().codeInHead);
		else {
			headNode.after(LuInst.getInstance().codeInHead);
		}

		// ordered
		for (int i = 0; i < all_Nodes.size(); ++i) {
			Node node = all_Nodes.get(i);

			if (node instanceof org.jsoup.nodes.Element) {

				Node node1 = node;

				// if (!node1.attr("id").isEmpty()) {
				// if (node1.attr("class").equals("Lu_Inst")
				// || node1.attr("class").equals("Lu_Inst_In_Head")) {
				// node1.remove();
				// }
				// }

				if (!node1.attr("id").isEmpty()) {
					if (!node1.attr("class").equals("Lu_Inst")) {

						// System.out.println(node1.attr("id"));

						if (!node1.attr("id").isEmpty()) {

							Id_LOC.put(node1.attr("id"), Id_LOC.size() + 1);
						}

						if (!node1.attr("src").isEmpty()
								&& !node1.attr("id").isEmpty()) {
							SRC_Id.put(node1.attr("src").replace('\\', '/')
									.replace("../", ""), node1.attr("id"));
						}

					}
				}

			}

		}

		//
		// for (String key : Id_LOC.keySet()) {
		// System.out.println(key + " : " + Id_LOC.get(key));
		// }
		//
		// for (String key : SRC_Id.keySet()) {
		// System.out.println(SRC_Id.get(key) + " : " + key);
		// }

	}

	public static void copyFileUsingFileChannels(File source, File dest)
			throws IOException {
		FileChannel inputChannel = null;
		FileChannel outputChannel = null;
		try {
			inputChannel = new FileInputStream(source).getChannel();
			outputChannel = new FileOutputStream(dest).getChannel();
			outputChannel.transferFrom(inputChannel, 0, inputChannel.size());
		} finally {
			inputChannel.close();
			outputChannel.close();
		}
	}

	static void Inst_afterEachTag_2(Document doc) {
		// Node headNode = doc.childNode(doc.childNodeSize() - 1).childNode(0);
		// Node bodyNode = doc.childNode(doc.childNodeSize() - 1).childNode(2);

		try {
			if (LuInst.getInstance().codeAfterEachTag.isEmpty()) {

				LuInst.getInstance().codeAfterEachTag = LuInst.getInstance()
						.readFile(
								LuInst.getInstance().FilePath
										+ "jsoup-master/Inst/afterEachTag.js");
			}

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// System.out.println(LuInst.getInstance().codeAfterEachTag);

		// reverse
		for (int i = all_Nodes.size() - 1; i >= 1; i--) {
			Node node = all_Nodes.get(i);

			if (node instanceof org.jsoup.nodes.Element) {

				if (!node.attr("id").isEmpty()) {
					if (!node.attr("class").equals("Lu_Inst")) {

						node.after(LuInst.getInstance().codeAfterEachTag);

					}
				}
			}

		}

	}

	static void Inst_afterBody_6(Document doc) {
		// Node headNode = doc.childNode(doc.childNodeSize() - 1).childNode(0);
		// Node bodyNode = doc.childNode(doc.childNodeSize() - 1).childNode(2);

		try {
			if (LuInst.getInstance().codeAfterBody.isEmpty()) {
				LuInst.getInstance().codeAfterBody = LuInst.getInstance()
						.readFile(
								LuInst.getInstance().FilePath
										+ "jsoup-master/Inst/codeAfterbody.js");
			}

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// reverse
		for (int i = all_Nodes.size() - 1; i >= 0; i--) {
			Node node = all_Nodes.get(i);

			if (node instanceof org.jsoup.nodes.Element) {
				if (!node.outerHtml().equals(" ")) {
					node.after(LuInst.getInstance().codeAfterBody);

					break;
				}
			}

		}

	}

	static void Inst_parsed_tags_4(Document doc) {

		// System.out.println(doc.outerHtml());
		// Node headNode = doc.childNode(doc.childNodeSize() - 1).childNode(0);
		// Node bodyNode = doc.childNode(doc.childNodeSize() - 1).childNode(2);

		// System.out.println("-->Head: \n"+headNode.outerHtml());
		// System.out.println("-->Body: \n"+bodyNode.outerHtml());
		String codeForTagParse = "";
		try {
			if (LuInst.getInstance().CodeForTagParsed.isEmpty()) {
				LuInst.getInstance().CodeForTagParsed = LuInst
						.getInstance()
						.readFile(
								LuInst.getInstance().FilePath
										+ "jsoup-master/Inst/CodeForTagParsed.js");
			}
			codeForTagParse = LuInst.getInstance().CodeForTagParsed;

		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		// reverse
		for (int i = all_Nodes.size() - 1; i >= 0; i--) {
			Node node = all_Nodes.get(i);

			if (node instanceof org.jsoup.nodes.Element) {
				Node node1 = node;

				// System.out.println(node1.outerHtml());
				if (!node1.attr("id").isEmpty()) {
					if (!node1.attr("class").equals("Lu_Inst")
							&& !node1.attr("class").equals("Lu_Inst_Src")) {
						String ID_Parsed = node1.attr("id") + "__parsed";

						// System.out.println(node1.outerHtml());

						if (LuInst.getInstance().raceText.contains(ID_Parsed)) {

							String toReplaceString = codeForTagParse;

							toReplaceString = toReplaceString.replace(
									"scriptId", "script_for_tag_parsed_:"
											+ node1.attr("id"));

							toReplaceString = toReplaceString.replace(
									"elementId", node1.attr("id"));

							node1.after(toReplaceString);
						}
					}
				}
			}

		}

	}

	static void Inst_Delay_Parsing_7(Document doc) {

		// System.out.println(doc.outerHtml());
		// Node headNode = doc.childNode(doc.childNodeSize() - 1).childNode(0);
		// Node bodyNode = doc.childNode(doc.childNodeSize() - 1).childNode(2);

		String lastId = "End_Of_Document";

		// reverse
		for (int i = all_Nodes.size() - 1; i >= 0; i--) {
			Node node = all_Nodes.get(i);

			if (node instanceof org.jsoup.nodes.Element) {
				Node node1 = node;

				// System.out.println(node1.outerHtml());
				if (!node1.attr("id").isEmpty()) {
					if (!node1.attr("class").equals("Lu_Inst")
							&& !node1.attr("class").equals("Lu_Inst_Src")) {
						node1.attr("Lu_Curr_LOC",
								"" + Id_LOC.get(node1.attr("id")));
						node1.attr("Lu_Next_Tag_Id", lastId);
						lastId = node1.attr("id");
					}
				}
			}

		}

		// ************************************************busy delay
		// parsing*****************************************//
		String busyDelayParsing = "";
		try {
			if (LuInst.getInstance().busyDelayParsing.isEmpty()) {
				LuInst.getInstance().busyDelayParsing = LuInst
						.getInstance()
						.readFile(
								LuInst.getInstance().FilePath
										+ "jsoup-master/Inst/busyDelayParsing.js");
			}
			busyDelayParsing = LuInst.getInstance().busyDelayParsing;

		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		// ordered
		for (int i = 0; i < all_Nodes.size(); ++i) {
			Node node = all_Nodes.get(i);

			if (node instanceof org.jsoup.nodes.Element) {
				Node node1 = node;

				// System.out.println(node1.outerHtml());
				if (!node1.attr("id").isEmpty()) {
					if (!node1.attr("class").equals("Lu_Inst")
							&& !node1.attr("class").equals("Lu_Inst_Src")) {
						String nextId__Parsed_event2 = ".event2.type = \""
								+ node1.attr("Lu_Next_Tag_Id") + "__parsed\";";

						String nextId__Parsed = node1.attr("Lu_Next_Tag_Id")
								+ "__" + node1.attr("Lu_Next_Tag_Id")
								+ "__parsed";

						if (LuInst.getInstance().raceText
								.contains(nextId__Parsed_event2)) {

							System.out.println(nextId__Parsed
									+ " needs postponing");

							busyDelayParsing = busyDelayParsing.replace(
									"EventString", nextId__Parsed);
							node1.after(busyDelayParsing);

						}

					}
				}
			}

		}

	}

	static void Inst_change_eh_This_8(Document doc) {

		String[] event_handler_types = { "animationend", "animationiteration",
				"animationstart", "onabort", "onafterprint", "onbeforeprint",
				"onbeforeunload", "onblur", "oncanplay", "oncanplaythrough",
				"onchange", "onclick", "oncontextmenu", "oncopy", "oncut",
				"ondblclick", "ondrag", "ondragend", "ondragenter",
				"ondragleave", "ondragover", "ondragstart", "ondrop",
				"ondurationchange", "onemptied", "onended", "onerror",
				"onfocus", "onfocusin", "onfocusout", "onhashchange",
				"oninput", "oninvalid", "onkeydown", "onkeypress", "onkeyup",
				"onload", "onloadeddata", "onloadedmetadata", "onloadstart",
				"onmessage", "onmousedown", "onmouseenter", "onmouseleave",
				"onmousemove", "onmouseout", "onmouseover", "onmouseup",
				"onmousewheel", "onoffline", "ononline", "onopen",
				"onpagehide", "onpageshow", "onpaste", "onpause", "onplay",
				"onplaying", "onpopstate", "onprogress", "onratechange",
				"onreset", "onresize", "onscroll", "onsearch", "onseeked",
				"onseeking", "onselect", "onshow", "onstalled", "onstorage",
				"onsubmit", "onsuspend", "ontimeupdate", "ontoggle",
				"ontouchcancel", "ontouchend", "ontouchmove", "ontouchstart",
				"onunload", "onvolumechange", "onwaiting", "onwheel" };
		//
		// for (String s : event_handler_types) {
		// System.out.println(s);
		// }

		// reverse
		for (int i = all_Nodes.size() - 1; i >= 0; i--) {
			Node node = all_Nodes.get(i);

			if (node instanceof org.jsoup.nodes.Element) {
				Node node1 = node;

				// System.out.println(node1.outerHtml());
				if (!node1.attr("id").isEmpty()) {
					if (!node1.attr("class").equals("Lu_Inst")
							&& !node1.attr("class").equals("Lu_Inst_Src")) {

						// System.out.println(node1.attr("id"));

						for (String s : event_handler_types) {
							// System.out.println(s);

							if (!node1.attr(s).isEmpty()) {

								System.out.println(node1.attr("id") + " has : "
										+ s);

								if (node1.attr(s).contains("(this)")) {
									node1.attr(
											s,
											node1.attr(s).replace(
													"(this)",
													"(document.getElementById('"
															+ node1.attr("id")
															+ "'))"));
								}

							}
						}

					}
				}
			}

		}

	}

	static void Inst_races_3(Document doc) throws ScriptException {
		Node headNode = doc.childNode(doc.childNodeSize() - 1).childNode(0);
		// Node bodyNode = doc.childNode(doc.childNodeSize() - 1).childNode(2);

		if (LuInst.getInstance().raceText.isEmpty()) {

			for (String raceInfo : LuInst.getInstance().raceInfoFileString) {
				// System.out.println(raceInfo);

				String event1_id = "";
				String event1_type = "";
				String event1_loc = "";

				String event2_id = "";
				String event2_type = "";
				String event2_loc = "";

				try {

					String raceContengString = LuInst.getInstance().readFile(
							raceInfo);

					String[] lines = raceContengString.split(System
							.getProperty("line.separator"));

					for (String s : lines) {
						if (s.startsWith("<script") || s.startsWith("</script")) {
							continue;
						}
						if (s.contains("event1.id = ")) {
							event1_id = s.split("event1.id = ")[1];
							event1_id = event1_id.substring(1,
									event1_id.length() - 3);
							// System.out.println("event1_id   : " + event1_id);
						}

						if (s.contains("event1.type = ")) {
							event1_type = s.split("event1.type = ")[1];
							event1_type = event1_type.substring(1,
									event1_type.length() - 3);
							// System.out
							// .println("event1_type   : " + event1_type);
						}

						if (s.contains("event1.loc = ")) {
							event1_loc = s.split("event1.loc = ")[1];
							event1_loc = event1_loc.substring(1,
									event1_loc.length() - 3);
							// System.out.println("event1_loc   : " +
							// event1_loc);
						}

						if (s.contains("event2.id = ")) {
							event2_id = s.split("event2.id = ")[1];
							event2_id = event2_id.substring(1,
									event2_id.length() - 3);
							// System.out.println("event2_id   : " + event2_id);
						}

						if (s.contains("event2.type = ")) {
							event2_type = s.split("event2.type = ")[1];
							event2_type = event2_type.substring(1,
									event2_type.length() - 3);
							// System.out
							// .println("event2_type   : " + event2_type);
						}

						if (s.contains("event2.loc = ")) {
							event2_loc = s.split("event2.loc = ")[1];
							event2_loc = event2_loc.substring(1,
									event2_loc.length() - 3);
							// System.out.println("event2_loc   : " +
							// event2_loc);
						}

						// System.out.println(s);

					}

					checkRace(event1_id, event1_type, event1_loc);
					checkRace(event2_id, event2_type, event2_loc);

					LuInst.getInstance().raceText = LuInst.getInstance().raceText
							+ raceContengString;

				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
		}

		for (String key : replaceRaceInfo_prioroty.keySet()) {

			// System.out.println(replaceRaceInfo.get(key) +
			// " will replace : \n"
			// + key);

			LuInst.getInstance().raceText = LuInst.getInstance().raceText
					.replace(key, replaceRaceInfo_prioroty.get(key));
		}

		for (String key : replaceRaceInfo.keySet()) {

			// System.out.println(replaceRaceInfo.get(key) +
			// " will replace : \n"
			// + key);

			LuInst.getInstance().raceText = LuInst.getInstance().raceText
					.replace(key, replaceRaceInfo.get(key));
		}

		// System.out.println(LuInst.getInstance().raceText);

		if (headNode.childNodeSize() > 0)
			headNode.childNode(0).after(LuInst.getInstance().raceText);
		else {
			headNode.after(LuInst.getInstance().raceText);
		}

	}

	static void checkRace(String id, String type, String loc) {
		// 1. check if it's iframe src:

		// if(true) return ;

		if (id.startsWith("Src:::")) {
			if (replaceRaceInfo.containsKey(id)) {
				// System.out.println("Already handld : " + id);
			}
			// System.out.println("New Iframe src race detected : " + id);

			// System.out.println(id);
			// System.out.println(type);
			// System.out.println(loc);

			for (String key : SRC_Id.keySet()) {
				// System.out.println(SRC_Id.get(key) + " : " + key);

				if (id.contains(key)) {
					// System.out.println("Match the element: " +
					// SRC_Id.get(key)
					// + " :> " + key);
					// replace ID:

					replaceRaceInfo.put(id, "_src_" + SRC_Id.get(key));

					// Replace Type;

					replaceRaceInfo.put(type, "onclick");

					// Replace LOC;

					// System.out.println("-------------------");
					// System.out.println(loc);
					// System.out.println(SRC_Id.get(key));
					// System.out.println(Id_LOC.get(SRC_Id.get(key)));
					// System.out.println("_src_" + SRC_Id.get(key) + "_LOC");

					// replaceRaceInfo.put(loc,
					// Id_LOC.get(SRC_Id.get(key)).toString());
					replaceRaceInfo_prioroty.put(loc,
							Id_LOC.get(SRC_Id.get(key)).toString());
					break;
				}

			}

			// if(SRC_Id.containsKey(key))

		}

		if (!replaceRaceInfo.containsKey(loc)) {
			replaceRaceInfo.put(loc, Id_LOC.get(id) + "");
		}

	}

	static String[] parseRace(String s) {

		// ScriptEngineManager factory = new ScriptEngineManager();
		// create JavaScript engine
		// ScriptEngine engine = factory.getEngineByName("JavaScript");

		// engine.eval("if(!myVars) {var myVars = {};}");

		// System.out.println("------------------------------\n"+engine.get("myVars"));

		s = s.substring(s.indexOf('\n') + 1);
		s.substring(0, s.lastIndexOf('\n'));
		String[] lines = s.split(System.getProperty("line.separator"));
		// System.out.println(s);
		return lines;
	}

	static void Inst_addUniqueId(Document doc) {
		Node headNode = doc.childNode(doc.childNodeSize() - 1).childNode(0);
		Node bodyNode = doc.childNode(doc.childNodeSize() - 1).childNode(2);

		HashMap<String, Boolean> existingIdHashMap = new HashMap<String, Boolean>();
		List<Node> nodes = headNode.childNodes();
		for (int i = nodes.size() - 1; i >= 0; i--) {
			Node node1 = nodes.get(i);
			if (!node1.attr("id").isEmpty()) {
				if (!node1.attr("class").equals("Lu_Inst")) {
					// System.out.println(node1.attr("id"));
					existingIdHashMap.put(node1.attr("id"), true);
				}
			}
		}

		nodes = bodyNode.childNodes();
		for (int i = nodes.size() - 1; i >= 0; i--) {
			Node node1 = nodes.get(i);
			if (!node1.attr("id").isEmpty()) {
				if (!node1.attr("class").equals("Lu_Inst")) {
					// System.out.println(node1.attr("id"));
					existingIdHashMap.put(node1.attr("id"), true);
				}
			}
		}

		// ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		HashMap<String, Integer> ID_generator = new HashMap<String, Integer>();

		nodes = headNode.childNodes();
		for (int i = 0; i <= nodes.size() - 1; i++) {
			Node node1 = nodes.get(i);
			if (!node1.attr("id").isEmpty()) {
				if (!node1.attr("class").equals("Lu_Inst")) {
					if (node1.attr("id").isEmpty()) {
						if (!ID_generator.containsKey(node1.nodeName())) {
							ID_generator.put(node1.nodeName(), 1);
						}
						String Lu_ID = "Lu_Id_" + node1.nodeName() + "_"
								+ ID_generator.get(node1.nodeName());
						ID_generator.put(node1.nodeName(),
								ID_generator.get(node1.nodeName()) + 1);
						node1.attr("id", Lu_ID);
						existingIdHashMap.put(node1.attr("id"), true);
					}
				}
			}
		}
		nodes = bodyNode.childNodes();
		for (int i = 0; i <= nodes.size() - 1; i++) {
			Node node1 = nodes.get(i);
			if (!node1.attr("id").isEmpty()) {
				if (!node1.attr("class").equals("Lu_Inst")) {
					if (node1.attr("id").isEmpty()) {
						if (!ID_generator.containsKey(node1.nodeName())) {
							ID_generator.put(node1.nodeName(), 1);
						}
						String Lu_ID = "Lu_Id_" + node1.nodeName() + "_"
								+ ID_generator.get(node1.nodeName());
						ID_generator.put(node1.nodeName(),
								ID_generator.get(node1.nodeName()) + 1);
						node1.attr("id", Lu_ID);
						existingIdHashMap.put(node1.attr("id"), true);
					}
				}
			}
		}
	}

	static void Get_All_Element_0(Node root, Document doc) {
		// System.out.println(root.outerHtml());

		// System.out.println(root.getClass());

		if (root != doc.head() && root != doc.body() && root != doc) {
			all_Nodes.add(root);
		}

		if (root.childNodes().size() > 0) {
			for (int i = 0; i < root.childNodes().size(); ++i) {
				if (root.childNode(i) instanceof org.jsoup.nodes.Element) {
					Get_All_Element_0(root.childNode(i), doc);
				}
			}
		}

	}

	static void Get_All_Element2_0(Node root) {
		// ordered
		for (int i = 0; i < all_Nodes.size(); ++i) {
			Node node = all_Nodes.get(i);

			if (node instanceof org.jsoup.nodes.Element) {
				if (!node.outerHtml().equals(" ")) {
					if (!node.attr("class").equals("Lu_Inst")) {

						System.out.println("Traverse: " + node.outerHtml());

					}
				}
			}

		}

		// // reverse
		// for (int i = all_Nodes.size() - 1; i >= 0; i--) {
		// Node node = all_Nodes.get(i);
		//
		// if (node instanceof org.jsoup.nodes.Element) {
		// if (!node.outerHtml().equals(" ")) {
		// if (!node.attr("class").equals("Lu_Inst")) {
		//
		// System.out.println("Traverse: " + node.outerHtml());
		//
		// }
		// }
		// }
		//
		// }
	}

	static void Inst_src_rewriting_5(Document doc) {
		// Node headNode = doc.childNode(doc.childNodeSize() - 1).childNode(0);
		// Node bodyNode = doc.childNode(doc.childNodeSize() - 1).childNode(2);
		String CodeForReplacingSrc = "";
		try {

			if (LuInst.getInstance().CodeForReplacingSrc.isEmpty()) {
				LuInst.getInstance().CodeForReplacingSrc = LuInst
						.getInstance()
						.readFile(
								LuInst.getInstance().FilePath
										+ "jsoup-master/Inst/CodeForReplacingSrc.js");
			}
			CodeForReplacingSrc = LuInst.getInstance().CodeForReplacingSrc;

			// System.out.println(CodeForReplacingSrc);

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// reverse
		for (int i = all_Nodes.size() - 1; i >= 0; i--) {
			Node node = all_Nodes.get(i);

			if (node instanceof org.jsoup.nodes.Element) {
				Node node1 = node;

				if (!node1.outerHtml().equals(" ")
						&& !node1.attr("class").equals("Lu_Inst")
						&& !node1.attr("src").isEmpty()) {

					// System.out.println("-- Node: " + node1.outerHtml());
					// System.out.println("++ Src:  " + node1.attr("src"));

					String src_value = node1.attr("src");
					String src_Id = node1.attr("id");

					String newCodeForReplacingSrc = CodeForReplacingSrc;

					if (!LuInst.getInstance().raceText.contains("_src_"
							+ src_Id)) {
						continue;
					}
					// System.out.println("To replace src for " + src_Id);

					if (true) {// condition can be set later
						node1.removeAttr("src");
						newCodeForReplacingSrc = newCodeForReplacingSrc
								.replace("scriptId", "_src_" + src_Id);
						newCodeForReplacingSrc = newCodeForReplacingSrc
								.replace("functionName",
										"function_name_set_src_to_" + src_Id);
						newCodeForReplacingSrc = newCodeForReplacingSrc
								.replace("srcElementId", src_Id);
						newCodeForReplacingSrc = newCodeForReplacingSrc
								.replace("srcPath", src_value);

						node1.after(newCodeForReplacingSrc);

						// System.out.println(newCodeForReplacingSrc);

					}

				}
			}

		}

	}
}

// //LuInst.getInstance().processingNewTagBegin();
// // System.out
// // .println("\n-------------------------------------------\nToken: "
// // + token);
//
// // System.out.println("-> \n"+String.valueOf(tokeniser.reader.input));

//
// //System.out.println("Token>---------------------------------------");
// //System.out.println("-------"+token.toString());
// //System.out.println("--------------------------------------------");
// //System.out.println("");
//
// Path currentRelativePath = Paths.get("");
// String s = currentRelativePath.toAbsolutePath().toString();
// // System.out.println("Current relative path is: " + s);
//
// // System.out.println(token.tokenType());
//
// // // this is an self ending tag, should insert call after that
// if (LuInst.getInstance().getEnableInstEndTagFlag())
// // enter function enabled
// if (LuInst.getInstance().scriptInHeadInserted) {
// // enter if head script if inserted
// if (LuInst.getInstance().getInstAfterSeflClosingTag()
// || token.isEndTag()) {
// // enter if this is an ending tag
// if (!LuInst.getInstance().needToStopRecurseInstrumentation) {
// LuInst.getInstance().needToStopRecurseInstrumentation = true;
//
// // System.out.println(LuInst.getInstance().insertAfteEachTag());
// // System.out.println("This is after each tag");
//
//
// // System.out.println("Ending of a tag: \n"+token.toString());
// //
// // System.out.println("");
//
//
// try {
// if (token.isEndTag()) {
// if (token.asEndTag().name().equals("head")
// || token.asEndTag().name()
// .equals("html")) {
// // System.out
// // .println("Skip </head>, </body>, </html>");
// } else if (token.asEndTag().name()
// .equals("body")) {
//
// if (LuInst.getInstance().codeAfterBody
// .isEmpty()) {
//
// LuInst.getInstance().codeAfterBody = LuInst
// .getInstance()
// .readFile(
// LuInst.getInstance().FilePath
// + "jsoup-master/Inst/codeAfterbody.js");
// }
// List<Node> insertedList = Parser
// .parseFragment(
// LuInst.getInstance().codeAfterBody,
// doc, baseUri);
// doc.body().appendChild(
// insertedList.get(0));
//
// } else {
// System.out.println("Ending tag: \n"+token.toString());
// System.out.println("");
// if (LuInst.getInstance().codeAfterEachTag
// .isEmpty()) {
//
// LuInst.getInstance().codeAfterEachTag = LuInst
// .getInstance()
// .readFile(
// LuInst.getInstance().FilePath
// + "jsoup-master/Inst/afterEachTag.js");
// }
// List<Node> insertedList = Parser
// .parseFragment(
// LuInst.getInstance().codeAfterEachTag,
// doc, baseUri);
// // System.out.println(this.stack);
// Element tmpElement = this.stack
// .get(this.stack.size() - 1);
// tmpElement.appendChild(insertedList
// .get(0));
//
// }
// }
// else {
//
// System.out.println("Self-ending tag: \n"+token.toString());
// System.out.println("");
// System.out.println("Last element: "+this.stack.get(this.stack.size() -
// 1).childNodes().get(this.stack.get(this.stack.size() -
// 1).childNodes().size()-1));
//
// if (LuInst.getInstance().codeAfterEachTag
// .isEmpty()) {
//
// LuInst.getInstance().codeAfterEachTag = LuInst
// .getInstance()
// .readFile(
// LuInst.getInstance().FilePath
// + "jsoup-master/Inst/afterEachTag.js");
// }
// List<Node> insertedList = Parser
// .parseFragment(
// LuInst.getInstance().codeAfterEachTag,
// doc, baseUri);
// // System.out.println(this.stack);
// Element tmpElement = this.stack
// .get(this.stack.size() - 1);
// tmpElement.appendChild(insertedList
// .get(0));
// // System.out.println("Last element: "+tmpElement.outerHtml());
// }
// } catch (IOException e) {
// // TODO Auto-generated catch block
// e.printStackTrace();
// }
// LuInst.getInstance().needToStopRecurseInstrumentation = false;
// }
// }
// }
//
// // this is <head> tag, should insert the functions after it
// if (LuInst.getInstance().getEnableInstHeadFlag())
// if (token.isStartTag()
// && token.asStartTag().name().equals("head")) {
// if (!LuInst.getInstance().needToStopRecurseInstrumentation) {
// LuInst.getInstance().needToStopRecurseInstrumentation = true;
// // System.out.println("This is <head>");
// LuInst.getInstance().scriptInHeadInserted = true;
// try {
// if (LuInst.getInstance().codeInHead.isEmpty()) {
//
// for (String raceInfo : LuInst.getInstance().raceInfoFileString) {
// LuInst.getInstance().codeInHead = LuInst
// .getInstance().readFile(raceInfo);
//
// List<Node> insertedList = Parser
// .parseFragment(
// LuInst.getInstance().codeInHead,
// doc, baseUri);
// // for(int i= insertedList.size();i<
// // insertedList.size(); i++){
// // doc.head().appendChild(insertedList.get(i));
// // }
// for (int i = 0; i < insertedList.size(); i++) {
// if (insertedList.get(i).outerHtml() != " ") {
// // System.out.println(insertedList.get(i).outerHtml());
// doc.head().appendChild(
// insertedList.get(i));
// }
// }
// }
//
// }
//
// } catch (IOException e) {
// // TODO Auto-generated catch block
// e.printStackTrace();
// }
//
// LuInst.getInstance().codeInHead = "";
//
// try {
// if (LuInst.getInstance().codeInHead.isEmpty()) {
//
// LuInst.getInstance().codeInHead = LuInst
// .getInstance()
// .readFile(
// LuInst.getInstance().FilePath
// + "jsoup-master/Inst/codeInHead.js");
// }
// List<Node> insertedList = Parser.parseFragment(
// LuInst.getInstance().codeInHead, doc,
// baseUri);
//
// for (int i = 0; i < insertedList.size(); i++) {
// if (insertedList.get(i).outerHtml() != " ") {
// // System.out.println(insertedList.get(i).outerHtml());
// doc.head().appendChild(insertedList.get(i));
// }
//
// }
//
// // doc.head().appendChild(insertedList.get(0));
//
// } catch (IOException e) {
// // TODO Auto-generated catch block
// e.printStackTrace();
// }
// LuInst.getInstance().codeInHead = "";
//
// LuInst.getInstance().needToStopRecurseInstrumentation = false;
// }
// }
// //
// LuInst.getInstance().processingNewTagEnd();