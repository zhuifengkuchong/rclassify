package org.jsoup.instrumentation;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Vector;

import org.jsoup.helper.DataUtil;
import org.jsoup.helper.Validate;

public class LuInst {
	private static LuInst instance = null;

	protected LuInst() {
		// Exists only to defeat instantiation.
	}

	public static LuInst getInstance() {
		if (instance == null) {
			instance = new LuInst();
		}
		return instance;
	}

	Boolean instAfterSeflClosingTag = false;// whether this is a self-closing
											// tag

	public String FilePath="";
	
	public Boolean enableInstHeadFlag = true;
	public Boolean enableInstEndTagFlag = true;

	public Boolean scriptInHeadInserted = false;

	public Boolean needToStopRecurseInstrumentation = false;
	public String codeInHead = "";
	public String codeAfterEachTag = "";
	public String afterEachTag_element = "";
	
	public String codeAfterBody = "";
	public String raceText="";
	public String raceTextFull="";
	
	public String busyDelayParsing="";
	public String CodeForReplacingSrc="";
	public String CodeForTagParsed= "";
	
	public HashMap<String, String> allRaces=new HashMap<String, String>();
	
	Boolean processingNewTag = false;// whether now is in process of a token
	
	public Vector<String> raceInfoFileString=new Vector<String>();

	public Boolean getInstAfterSeflClosingTag() {
		return instAfterSeflClosingTag;
	}

	public String insertAfterHeadStartTag() {
		String retVal = "to be insert after <head> tag";
		return retVal;
	}

	public String insertAfteEachTag() {
		String retVal = "to be insert after each tag";
		return retVal;
	}

	public void setInstAfterSeflClosingTag(Boolean instAfterSeflClosingTag) {
		this.instAfterSeflClosingTag = instAfterSeflClosingTag;
	}

	public void processingNewTagBegin() {
		processingNewTag = true;
	}

	public void processingNewTagEnd() {
		processingNewTag = false;
		setInstAfterSeflClosingTag(false);
	}

	public Boolean getEnableInstHeadFlag() {
		return enableInstHeadFlag;
	}

	public void setEnableInstHeadFlag(Boolean enableInstHeadFlag) {
		this.enableInstHeadFlag = enableInstHeadFlag;
	}

	public Boolean getEnableInstEndTagFlag() {
		return enableInstEndTagFlag;
	}

	public void setEnableInstEndTagFlag(Boolean enableInstEndTagFlag) {
		this.enableInstEndTagFlag = enableInstEndTagFlag;
	}


	public String readFile(String fileName) throws IOException {
		File input = new File(fileName);
		ByteBuffer byteData = DataUtil.readFileToByteBuffer(input);

		String charsetName = "UTF-8";
		Validate.notEmpty(
				charsetName,
				"Must set charset arg to character set of file to parse. Set to null to attempt to detect from HTML");
		String docData = Charset.forName(charsetName).decode(byteData)
				.toString();
		
	
		return docData;
		// BufferedReader br = new BufferedReader(new FileReader(fileName));
		// try {
		// StringBuilder sb = new StringBuilder();
		// String line = br.readLine();
		//
		// while (line != null) {
		// sb.append(line);
		// sb.append("\n");
		// line = br.readLine();
		// }
		// return sb.toString();
		// } finally {
		// br.close();
		// }
		//

	}

}
