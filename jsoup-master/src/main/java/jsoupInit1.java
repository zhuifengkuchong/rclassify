import java.io.Console;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.text.Element;

import org.jsoup.Jsoup;
import org.jsoup.instrumentation.LuInst;
import org.jsoup.nodes.Attribute;
import org.jsoup.nodes.Attributes;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Node;

public class jsoupInit1 {
	static ArrayList<Node> all_Nodes = new ArrayList<Node>();
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		LuInst.getInstance().FilePath = "/home/jack/Dropbox/VT/JavaScript/codes/";
		String UrlPath = "";
		if (args.length > 0) {
//			UrlPath = LuInst.getInstance().FilePath + args[0];
			UrlPath = ""+args[0];
			System.out.println(UrlPath);
		}

		if (UrlPath == "")
			return;

		File input = new File(UrlPath);// the entry html file
		System.out.println("The html file path: " + input.getAbsolutePath());

		File parentPath = input.getAbsoluteFile().getParentFile();
		System.out.println("The folder path: " + parentPath.getAbsolutePath());

		try {
			Document doc = Jsoup.parse(input, "UTF-8", "http://example.com/");

			try {

//				input.renameTo(new File(input.getAbsolutePath() + "_Lu_BackUp"));
//				Inst_addUniqueId(doc);
//				FileWriter fw0 = new FileWriter(input.getAbsolutePath());
//				fw0.write(doc.outerHtml());
//				fw0.close();

				// Inst_src_rewriting(doc);
				
				
				Get_All_Element(doc);
				
				Inst_inHead(doc, parentPath.getAbsolutePath());
				
				
				
				
//				Inst_replaceTppabs(doc);
				// Inst_afterEachTag(doc);
				// Inst_parsed_tags(doc);
//				Inst_afterBody(doc);
				// Inst_races(doc);

//				String outputPathString = parentPath.getAbsolutePath() + "/"
//						+ "Init1.html";
				String outputPathString=input.getAbsolutePath()+"_init1.html";
				File newTextFile = new File(outputPathString);
				System.out.println("----The output: " + outputPathString);

				FileWriter fw = new FileWriter(newTextFile);
				fw.write(doc.outerHtml());
				fw.close();

			} catch (IOException iox) {
				// do stuff with exception
				iox.printStackTrace();
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	// }
	
	
	
	
	static void Get_All_Element(Node root) {
		// System.out.println(root.outerHtml());

		// System.out.println(root.getClass());

		all_Nodes.add(root);
		if (root.childNodes().size() > 0) {
			for (int i = 0; i < root.childNodes().size(); ++i) {

				Get_All_Element(root.childNode(i));

			}
		}
	}
//	
//	static void remove_href(Node root) {
//
//		
//	}
	

	static void Inst_inHead(Document doc, String parentPath) {
		Node headNode = doc.childNode(doc.childNodeSize()-1).childNode(0);
//		Node bodyNode = doc.childNode(doc.childNodeSize()-1).childNode(2);

		try {
			if (LuInst.getInstance().codeInHead.isEmpty()) {

				LuInst.getInstance().codeInHead = LuInst
						.getInstance()
						.readFile(
								LuInst.getInstance().FilePath
										+ "jsoup-master/Inst/InsertCodeInHeadInit1.js");
			}
			copyFileUsingFileChannels(new File(LuInst.getInstance().FilePath
					+ "jsoup-master/Inst/codeInHeadInit1.js"), new File(
					parentPath + "/codeInHeadInit1.js"));

			copyFileUsingFileChannels(new File(LuInst.getInstance().FilePath
					+ "download/autoFiring.txt"), new File(
					parentPath + "/autoFiring.txt"));
			LuInst.getInstance().codeInHead += LuInst
					.getInstance()
					.readFile(parentPath + "/autoFiring.txt");
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (headNode.childNodeSize() > 0)
			headNode.childNode(0).after(LuInst.getInstance().codeInHead);
		else {
			headNode.after(LuInst.getInstance().codeInHead);
		}
	}

	public static void deleteFile(File file){
		try {
			file.delete();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static void copyFileUsingFileChannels(File source, File dest)
			throws IOException {
		FileChannel inputChannel = null;
		FileChannel outputChannel = null;
		try {
			inputChannel = new FileInputStream(source).getChannel();
			outputChannel = new FileOutputStream(dest).getChannel();
			outputChannel.transferFrom(inputChannel, 0, inputChannel.size());
		} finally {
			inputChannel.close();
			outputChannel.close();
		}
	}

	static void Inst_afterEachTag(Document doc) {
		Node headNode = doc.childNode(doc.childNodeSize()-1).childNode(0);
		Node bodyNode = doc.childNode(doc.childNodeSize()-1).childNode(2);

		try {
			if (LuInst.getInstance().codeAfterEachTag.isEmpty()) {

				LuInst.getInstance().codeAfterEachTag = LuInst.getInstance()
						.readFile(
								LuInst.getInstance().FilePath
										+ "jsoup-master/Inst/afterEachTag.js");
			}

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		List<Node> nodes = headNode.childNodes();
		for (int i = nodes.size() - 1; i >= 0; i--) {
			Node node1 = nodes.get(i);
			if (!node1.outerHtml().equals(" ")) {
				if (!node1.attr("class").equals("Lu_Inst")) {
					// System.out.println("--" + node1.outerHtml());
					// System.out.println("Node : --" + node1.outerHtml());
					// System.out.println("Class: ==" + node1.attr("class"));
					node1.after(LuInst.getInstance().codeAfterEachTag);
				}
			}
		}

		nodes = bodyNode.childNodes();
		for (int i = nodes.size() - 1; i >= 0; i--) {
			Node node1 = nodes.get(i);
			if (!node1.outerHtml().equals(" ")) {
				if (!node1.attr("class").equals("Lu_Inst")) {
					// System.out.println("--" + node1.outerHtml());
					// System.out.println("Node : --" + node1.outerHtml());
					node1.after(LuInst.getInstance().codeAfterEachTag);
				}
			}
		}

	}

	static void Inst_replaceTppabs(Document doc) {
		Node headNode = doc.childNode(doc.childNodeSize()-1).childNode(0);
		Node bodyNode = doc.childNode(doc.childNodeSize()-1).childNode(2);

		Inst_replaceTppabs_Recursive(headNode);
		Inst_replaceTppabs_Recursive(bodyNode);

		// List<Node> nodes = headNode.childNodes();
		// for (int i = nodes.size() - 1; i >= 0; i--) {
		// Node node1 = nodes.get(i);
		// if (!node1.outerHtml().equals(" ")) {
		// Attributes attrs = node1.attributes();
		// // System.out.println("Node:" + node1.toString());
		// for (int j = attrs.size() - 1; j >= 0; j--) {
		// String keyString = attrs.asList().get(j).getKey();
		// String valueString = attrs.asList().get(j).getValue();
		// // System.out.println(keyString);
		// // System.out.println(valueString);
		// if (keyString.equals("tppabs")) {
		// // System.out.println("From:"
		// // + node1.toString());
		// j--;
		// attrs.asList().get(j).setValue(valueString);
		// // attrs.asList().remove(j + 1);
		// // System.out.println("To  :"
		// // + node1.toString());
		// }
		// }
		// attrs.remove("tppabs");
		// // System.out.println("End :"
		// // + node1.toString());
		// }
		// }
		//
		// nodes = bodyNode.childNodes();
		// for (int i = nodes.size() - 1; i >= 0; i--) {
		// Node node1 = nodes.get(i);
		// if (!node1.outerHtml().equals(" ")) {
		// Attributes attrs = node1.attributes();
		// System.out.println("Node:" + node1.toString());
		// System.out
		// .println("-------------------------------------------------------------");
		// for (int j = attrs.size() - 1; j >= 0; j--) {
		// String keyString = attrs.asList().get(j).getKey();
		// String valueString = attrs.asList().get(j).getValue();
		// // System.out.println(keyString);
		// // System.out.println(valueString);
		// if (keyString.equals("tppabs")) {
		// // System.out.println("From:"
		// // + node1.toString());
		// j--;
		// attrs.asList().get(j).setValue(valueString);
		// // attrs.asList().remove(j + 1);
		// // System.out.println("To  :"
		// // + node1.toString());
		// }
		// }
		// attrs.remove("tppabs");
		// System.out.println("End :" + node1.toString());
		// System.out
		// .println("--------------------------------------------------------------");
		// }
		// }

	}

	static void Inst_replaceTppabs_Recursive(Node node) {
		List<Node> nodes = node.childNodes();
		for (int i = nodes.size() - 1; i >= 0; i--) {
			Node node1 = nodes.get(i);
			if (!node1.outerHtml().equals(" ")) {
				Attributes attrs = node1.attributes();
				System.out.println("Node:" + node1.toString());
				System.out
						.println("-------------------------------------------------------------");
				boolean removeTppabs=false;
				for (int j = attrs.size() - 1; j >= 0; j--) {
					String keyString = attrs.asList().get(j).getKey();
					String valueString = attrs.asList().get(j).getValue();
					// System.out.println(keyString);
					// System.out.println(valueString);
//					keyString2 = "";
					if (keyString.equals("tppabs")) {

						// System.out.println("From:"
						// + node1.toString());
						j--;
						String keyString2 = attrs.asList().get(j).getKey();
						if (keyString2.equals("src")) {
							attrs.asList().get(j).setValue(valueString);
							removeTppabs=true;
						}
						// attrs.asList().remove(j + 1);
						// System.out.println("To  :"
						// + node1.toString());
					}
				}
				if (removeTppabs) {
					attrs.remove("tppabs");
				}
				System.out.println("End :" + node1.toString());
				System.out
						.println("--------------------------------------------------------------");
			}
			Inst_replaceTppabs_Recursive(node1);
		}
	}

	static void Inst_afterBody(Document doc) {
		Node headNode = doc.childNode(doc.childNodeSize()-1).childNode(0);
		Node bodyNode = doc.childNode(doc.childNodeSize()-1).childNode(2);

		try {

			LuInst.getInstance().codeAfterBody = LuInst.getInstance().readFile(
					LuInst.getInstance().FilePath
							+ "jsoup-master/Inst/codeAfterbodyInit.js");

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		List<Node> nodes = bodyNode.childNodes();
		for (int i = nodes.size() - 1; i >= 0; i--) {
			Node node1 = nodes.get(i);
			if (!node1.outerHtml().equals(" ")) {

				// System.out.println("Node : --" + node1.outerHtml());
				node1.after(LuInst.getInstance().codeAfterBody);

				break;

			}
		}
	}

	static void Inst_parsed_tags(Document doc) {
		Node headNode = doc.childNode(doc.childNodeSize()-1).childNode(0);
		Node bodyNode = doc.childNode(doc.childNodeSize()-1).childNode(2);

		String raceText = "";

		try {
			String codeForTagParse = LuInst.getInstance().codeAfterBody = LuInst
					.getInstance().readFile(
							LuInst.getInstance().FilePath
									+ "jsoup-master/Inst/CodeForTagParsed.js");

			for (String raceInfo : LuInst.getInstance().raceInfoFileString) {
				System.out.println(raceInfo);

				try {
					raceText = raceText
							+ LuInst.getInstance().readFile(raceInfo);

				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}

			List<Node> nodes = headNode.childNodes();
			for (int i = nodes.size() - 1; i >= 0; i--) {
				Node node1 = nodes.get(i);
				if (!node1.outerHtml().equals(" ")) {
					if (!node1.attr("class").equals("Lu_Inst")) {

						String ID_Parsed = node1.attr("id") + "__parsed";

						if (raceText.contains(ID_Parsed)) {
							String toReplaceString = codeForTagParse;

							toReplaceString = toReplaceString.replace(
									"scriptId", "script_for_tag_parsed_:"
											+ node1.attr("id"));

							toReplaceString = toReplaceString.replace(
									"elementId", node1.attr("id"));

							node1.after(toReplaceString);
						}

					}
				}
			}

			nodes = bodyNode.childNodes();
			for (int i = nodes.size() - 1; i >= 0; i--) {
				Node node1 = nodes.get(i);
				if (!node1.outerHtml().equals(" ")) {
					if (!node1.attr("class").equals("Lu_Inst")) {
						String ID_Parsed = node1.attr("id") + "__parsed";

						if (raceText.contains(ID_Parsed)) {
							String toReplaceString = codeForTagParse;

							toReplaceString = toReplaceString.replace(
									"scriptId", "script_for_tag_parsed_:"
											+ node1.attr("id"));

							toReplaceString = toReplaceString.replace(
									"elementId", node1.attr("id"));

							node1.after(toReplaceString);
						}
					}
				}
			}
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

	}

	static void Inst_races(Document doc) {
		Node headNode = doc.childNode(doc.childNodeSize()-1).childNode(0);
		Node bodyNode = doc.childNode(doc.childNodeSize()-1).childNode(2);

		String raceText = "";
		for (String raceInfo : LuInst.getInstance().raceInfoFileString) {
			System.out.println(raceInfo);

			try {
				raceText = raceText + LuInst.getInstance().readFile(raceInfo);

			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
		headNode.childNode(0).after(raceText);

	}

	static void Inst_addUniqueId(Document doc) {
		Node headNode = doc.childNode(doc.childNodeSize()-1).childNode(0);
		Node bodyNode = doc.childNode(doc.childNodeSize()-1).childNode(2);

		HashMap<String, Boolean> existingIdHashMap = new HashMap<String, Boolean>();
		List<Node> nodes = headNode.childNodes();
		for (int i = nodes.size() - 1; i >= 0; i--) {
			Node node1 = nodes.get(i);
			if (!node1.outerHtml().equals(" ")) {
				if (!node1.attr("class").equals("Lu_Inst")) {
					// System.out.println(node1.attr("id"));
					existingIdHashMap.put(node1.attr("id"), true);
				}
			}
		}

		nodes = bodyNode.childNodes();
		for (int i = nodes.size() - 1; i >= 0; i--) {
			Node node1 = nodes.get(i);
			if (!node1.outerHtml().equals(" ")) {
				if (!node1.attr("class").equals("Lu_Inst")) {
					// System.out.println(node1.attr("id"));
					existingIdHashMap.put(node1.attr("id"), true);
				}
			}
		}

		// ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		HashMap<String, Integer> ID_generator = new HashMap<String, Integer>();

		nodes = headNode.childNodes();
		for (int i = 0; i <= nodes.size() - 1; i++) {
			Node node1 = nodes.get(i);
			if (!node1.outerHtml().equals(" ")) {

				if (node1.attr("id").isEmpty()) {
					if (!ID_generator.containsKey(node1.nodeName())) {
						ID_generator.put(node1.nodeName(), 1);
					}
					String Lu_ID = "Lu_Id_" + node1.nodeName() + "_"
							+ ID_generator.get(node1.nodeName());
					ID_generator.put(node1.nodeName(),
							ID_generator.get(node1.nodeName()) + 1);
					node1.attr("id", Lu_ID);
					existingIdHashMap.put(node1.attr("id"), true);
				}

			}
		}

		nodes = bodyNode.childNodes();
		for (int i = 0; i <= nodes.size() - 1; i++) {
			Node node1 = nodes.get(i);
			if (!node1.outerHtml().equals(" ")) {

				if (node1.attr("id").isEmpty()) {
					if (!ID_generator.containsKey(node1.nodeName())) {
						ID_generator.put(node1.nodeName(), 1);
					}
					String Lu_ID = "Lu_Id_" + node1.nodeName() + "_"
							+ ID_generator.get(node1.nodeName());
					ID_generator.put(node1.nodeName(),
							ID_generator.get(node1.nodeName()) + 1);
					node1.attr("id", Lu_ID);
					existingIdHashMap.put(node1.attr("id"), true);
				}
			}
		}
	}

	static void Inst_src_rewriting(Document doc) {
		Node headNode = doc.childNode(doc.childNodeSize()-1).childNode(0);
		Node bodyNode = doc.childNode(doc.childNodeSize()-1).childNode(2);
		String CodeForReplacingSrc;
		try {

			CodeForReplacingSrc = LuInst.getInstance().codeAfterBody = LuInst
					.getInstance()
					.readFile(
							LuInst.getInstance().FilePath
									+ "jsoup-master/Inst/CodeForReplacingSrc.js");

			// System.out.println(CodeForReplacingSrc);

			List<Node> nodes = headNode.childNodes();
			for (int i = 0; i <= nodes.size() - 1; i++) {
				Node node1 = nodes.get(i);
				if (!node1.outerHtml().equals(" ")) {

					if (!node1.attr("src").isEmpty()) {
						// System.out.println("-- Node: " + node1.outerHtml());
						// System.out.println("++ Src:  " + node1.attr("src"));

						String src_value = node1.attr("src");
						String src_Id = node1.attr("id");

						String newCodeForReplacingSrc = CodeForReplacingSrc;

						if (true) {// condition can be set later
							node1.removeAttr("src");
							newCodeForReplacingSrc = newCodeForReplacingSrc
									.replace("scriptId", "src_script_for_"
											+ src_Id);
							newCodeForReplacingSrc = newCodeForReplacingSrc
									.replace("functionName",
											"function_name_set_src_to_"
													+ src_Id);
							newCodeForReplacingSrc = newCodeForReplacingSrc
									.replace("srcElementId", src_Id);
							newCodeForReplacingSrc = newCodeForReplacingSrc
									.replace("srcPath", src_value);

							node1.after(newCodeForReplacingSrc);

							// System.out.println(newCodeForReplacingSrc);

						}

					}
				}
			}
			nodes = bodyNode.childNodes();
			for (int i = 0; i <= nodes.size() - 1; i++) {
				Node node1 = nodes.get(i);
				if (!node1.outerHtml().equals(" ")) {
					if (!node1.attr("src").isEmpty()) {

						// System.out.println("-- Node: " + node1.outerHtml());
						// System.out.println("++ Src:  " + node1.attr("src"));

						String src_value = node1.attr("src");
						String src_Id = node1.attr("id");

						String newCodeForReplacingSrc = CodeForReplacingSrc;

						if (true) {// condition can be set later
							node1.removeAttr("src");
							newCodeForReplacingSrc = newCodeForReplacingSrc
									.replace("scriptId", "src_script_for_"
											+ src_Id);
							newCodeForReplacingSrc = newCodeForReplacingSrc
									.replace("functionName",
											"function_name_set_src_to_"
													+ src_Id);
							newCodeForReplacingSrc = newCodeForReplacingSrc
									.replace("srcElementId", src_Id);
							newCodeForReplacingSrc = newCodeForReplacingSrc
									.replace("srcPath", src_value);

							node1.after(newCodeForReplacingSrc);

							// System.out.println(newCodeForReplacingSrc);

						}

					}
				}
			}

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}

// //LuInst.getInstance().processingNewTagBegin();
// // System.out
// // .println("\n-------------------------------------------\nToken: "
// // + token);
//
// // System.out.println("-> \n"+String.valueOf(tokeniser.reader.input));

//
// //System.out.println("Token>---------------------------------------");
// //System.out.println("-------"+token.toString());
// //System.out.println("--------------------------------------------");
// //System.out.println("");
//
// Path currentRelativePath = Paths.get("");
// String s = currentRelativePath.toAbsolutePath().toString();
// // System.out.println("Current relative path is: " + s);
//
// // System.out.println(token.tokenType());
//
// // // this is an self ending tag, should insert call after that
// if (LuInst.getInstance().getEnableInstEndTagFlag())
// // enter function enabled
// if (LuInst.getInstance().scriptInHeadInserted) {
// // enter if head script if inserted
// if (LuInst.getInstance().getInstAfterSeflClosingTag()
// || token.isEndTag()) {
// // enter if this is an ending tag
// if (!LuInst.getInstance().needToStopRecurseInstrumentation) {
// LuInst.getInstance().needToStopRecurseInstrumentation = true;
//
// // System.out.println(LuInst.getInstance().insertAfteEachTag());
// // System.out.println("This is after each tag");
//
//
// // System.out.println("Ending of a tag: \n"+token.toString());
// //
// // System.out.println("");
//
//
// try {
// if (token.isEndTag()) {
// if (token.asEndTag().name().equals("head")
// || token.asEndTag().name()
// .equals("html")) {
// // System.out
// // .println("Skip </head>, </body>, </html>");
// } else if (token.asEndTag().name()
// .equals("body")) {
//
// if (LuInst.getInstance().codeAfterBody
// .isEmpty()) {
//
// LuInst.getInstance().codeAfterBody = LuInst
// .getInstance()
// .readFile(
// LuInst.getInstance().FilePath
// + "jsoup-master/Inst/codeAfterbody.js");
// }
// List<Node> insertedList = Parser
// .parseFragment(
// LuInst.getInstance().codeAfterBody,
// doc, baseUri);
// doc.body().appendChild(
// insertedList.get(0));
//
// } else {
// System.out.println("Ending tag: \n"+token.toString());
// System.out.println("");
// if (LuInst.getInstance().codeAfterEachTag
// .isEmpty()) {
//
// LuInst.getInstance().codeAfterEachTag = LuInst
// .getInstance()
// .readFile(
// LuInst.getInstance().FilePath
// + "jsoup-master/Inst/afterEachTag.js");
// }
// List<Node> insertedList = Parser
// .parseFragment(
// LuInst.getInstance().codeAfterEachTag,
// doc, baseUri);
// // System.out.println(this.stack);
// Element tmpElement = this.stack
// .get(this.stack.size() - 1);
// tmpElement.appendChild(insertedList
// .get(0));
//
// }
// }
// else {
//
// System.out.println("Self-ending tag: \n"+token.toString());
// System.out.println("");
// System.out.println("Last element: "+this.stack.get(this.stack.size() -
// 1).childNodes().get(this.stack.get(this.stack.size() -
// 1).childNodes().size()-1));
//
// if (LuInst.getInstance().codeAfterEachTag
// .isEmpty()) {
//
// LuInst.getInstance().codeAfterEachTag = LuInst
// .getInstance()
// .readFile(
// LuInst.getInstance().FilePath
// + "jsoup-master/Inst/afterEachTag.js");
// }
// List<Node> insertedList = Parser
// .parseFragment(
// LuInst.getInstance().codeAfterEachTag,
// doc, baseUri);
// // System.out.println(this.stack);
// Element tmpElement = this.stack
// .get(this.stack.size() - 1);
// tmpElement.appendChild(insertedList
// .get(0));
// // System.out.println("Last element: "+tmpElement.outerHtml());
// }
// } catch (IOException e) {
// // TODO Auto-generated catch block
// e.printStackTrace();
// }
// LuInst.getInstance().needToStopRecurseInstrumentation = false;
// }
// }
// }
//
// // this is <head> tag, should insert the functions after it
// if (LuInst.getInstance().getEnableInstHeadFlag())
// if (token.isStartTag()
// && token.asStartTag().name().equals("head")) {
// if (!LuInst.getInstance().needToStopRecurseInstrumentation) {
// LuInst.getInstance().needToStopRecurseInstrumentation = true;
// // System.out.println("This is <head>");
// LuInst.getInstance().scriptInHeadInserted = true;
// try {
// if (LuInst.getInstance().codeInHead.isEmpty()) {
//
// for (String raceInfo : LuInst.getInstance().raceInfoFileString) {
// LuInst.getInstance().codeInHead = LuInst
// .getInstance().readFile(raceInfo);
//
// List<Node> insertedList = Parser
// .parseFragment(
// LuInst.getInstance().codeInHead,
// doc, baseUri);
// // for(int i= insertedList.size();i<
// // insertedList.size(); i++){
// // doc.head().appendChild(insertedList.get(i));
// // }
// for (int i = 0; i < insertedList.size(); i++) {
// if (insertedList.get(i).outerHtml() != " ") {
// // System.out.println(insertedList.get(i).outerHtml());
// doc.head().appendChild(
// insertedList.get(i));
// }
// }
// }
//
// }
//
// } catch (IOException e) {
// // TODO Auto-generated catch block
// e.printStackTrace();
// }
//
// LuInst.getInstance().codeInHead = "";
//
// try {
// if (LuInst.getInstance().codeInHead.isEmpty()) {
//
// LuInst.getInstance().codeInHead = LuInst
// .getInstance()
// .readFile(
// LuInst.getInstance().FilePath
// + "jsoup-master/Inst/codeInHead.js");
// }
// List<Node> insertedList = Parser.parseFragment(
// LuInst.getInstance().codeInHead, doc,
// baseUri);
//
// for (int i = 0; i < insertedList.size(); i++) {
// if (insertedList.get(i).outerHtml() != " ") {
// // System.out.println(insertedList.get(i).outerHtml());
// doc.head().appendChild(insertedList.get(i));
// }
//
// }
//
// // doc.head().appendChild(insertedList.get(0));
//
// } catch (IOException e) {
// // TODO Auto-generated catch block
// e.printStackTrace();
// }
// LuInst.getInstance().codeInHead = "";
//
// LuInst.getInstance().needToStopRecurseInstrumentation = false;
// }
// }
// //
// LuInst.getInstance().processingNewTagEnd();