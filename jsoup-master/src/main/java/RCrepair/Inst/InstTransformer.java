package RCrepair.Inst;

import java.io.IOException;

import RCrepair.Utilities;

public class InstTransformer {
	public static String getTemplete(String name) throws IOException{
		return Utilities.readFromFile("/home/jack/Dropbox/VT/JavaScript/codes/jsoup-master/src/main/java/RCrepair/Inst/"+name);
	}
	///////////////////////////////////////////////////////////////////////////////////////////////////////
	public static String get_Repair1B() throws IOException{
		return getTemplete("Repair1B-ReattachingEh.js");
	}
	public static String repair1B(String node2Id, String eh_type, String eh_content, String src_content) throws IOException{
		String templete  = get_Repair1B();
		templete=templete.replace("node2.id", node2Id);
		templete=templete.replace("eh_type", eh_type);
		templete=templete.replace("eh_content", eh_content);		
		templete=templete.replace("src_content", src_content);		
		return templete;
	}
	///////////////////////////////////////////////////////////////////////////////////////////////////////
	public static String get_Repair1C() throws IOException{
		return getTemplete("Repair1C-ReattachingEh.js");
	}	
	public static String repair1C(String node2Id, String eh_type, String eh_content) throws IOException{
		String templete  = get_Repair1C();
		templete=templete.replace("node2.id", node2Id);
		templete=templete.replace("eh_type", eh_type);
		templete=templete.replace("eh_content", eh_content);		
		return templete;
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////
	public static String get_Repair1D() throws IOException{
		return getTemplete("Repair1D-invisibleButton.js");
	}
	public static String repair1D(String node2Id) throws IOException{
		String templete  = get_Repair1D();
		templete=templete.replace("node2.id", node2Id);	
		return templete;
	}
	
	///////////////////////////////////////////////////////////////////////////////////////////////////////
	public static String get_Repair2A() throws IOException{
		return getTemplete("Repair2A-RetryOnload.js");
	}
	public static String repair2A(String node2Id, String eh_type, String varName) throws IOException{
		String templete  = get_Repair2A();
		templete=templete.replace("node2.id", node2Id);	
		templete=templete.replace("eh_type", eh_type);	
		templete=templete.replace("varName", varName);			
		return templete;
	}
	
	///////////////////////////////////////////////////////////////////////////////////////////////////////
	public static String get_Repair2B() throws IOException{
		return getTemplete("Repair2B-TryOnce.js");
	}
	public static String repair2B(String node2Id, String eh_type, String varName) throws IOException{
		String templete  = get_Repair2B();
		templete=templete.replace("node2.id", node2Id);	
		templete=templete.replace("eh_type", eh_type);	
		templete=templete.replace("varName", varName);			
		return templete;
	}
	
}
