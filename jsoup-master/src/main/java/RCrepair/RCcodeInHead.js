if (!myVars) {
	var myVars = {};
}

var fireEventListnerAfterRegisteration = true;

var LU_document_onDOMContentLoaded = false;
var LU_window_onload = false;

var timerInterval = 100000;
var MAX_Postpone_count = 100;
// var pseudo_window = new Object();

var Lu_alerts = [];
var Lu_errors = [];
var Lu_console_log = [];
var Lu_console_warn = [];
var Lu_console_error = [];
var Lu_visited = [];
var Lu_to_visit = [];

var Lu_Fire_EventListner = true;
var Lu_Attach_EventListner = true;
var Lu_Attach_Timer = false;
var Lu_Ajax = true;
var Lu_src = true;

var windowAlert_Lu = window.alert;

window.alert = function(message) {
	Lu_alerts.push(message);
	consoleWarn_Lu("Alert message logged: " + message);
}

var consoleLog_Lu = console.log;
console.log = function(message) {
	consoleLog_Lu("console.log   : " + message);
	// consoleWarn_Lu("Console.log message logged: " + getJSON(message));
	Lu_console_log.push(getJSON(message));
}

var consoleWarn_Lu = console.warn;
console.warn = function(message) {
	consoleWarn_Lu("console.warn  : " + message);
	// consoleWarn_Lu("Console.log message logged: " + getJSON(message));
	Lu_console_warn.push(getJSON(message));
}

var consoleError_Lu = console.error;
console.error = function(message) {
	consoleError_Lu("console.error : " + message.toString());
	// consoleWarn_Lu("Console.log message logged: " + getJSON(message));
	Lu_console_error.push(getJSON(message));
}

function isDocument(obj) {
	if (obj instanceof HTMLDocument) {
		return true;
	}
	if (Object.prototype.toString.call(obj) == "[object HTMLDocument]") {
		return true;
	}
	return false;
}

function isWindow(obj) {
	if (obj == window) {
		return true;
	}
	if (Object.prototype.toString.call(obj) == "[object Window]") {
		return true;
	}
	return false;
}

Element.prototype.getId = function getID() {
	if (this == null)
		return null;
	if (isWindow(this))
		return "Lu_window";
	if (isDocument(this))
		return "Lu_DOM";
	if (this.id)
		return this.id;
	else
		return null;
}
function getID(obj) {
	if (obj == null)
		return null;
	if (isWindow(obj))
		return "Lu_window";
	if (isDocument(obj))
		return "Lu_DOM";
	if (obj.id)
		return obj.id;
	else
		return null;
}

function isAsynScript(string) {
	if (string.startsWith("_script_"))
		return true;
	else
		return false;
}

function isParsing(string) {
	if (string.endsWith("_parsed"))
		return true;
	else
		return false;
}
function isSrc(string) {
	if (string.startsWith("_src_"))
		return true;
	else
		return false;
}

function combRaces() {
	if (!myVars.notExecutedEvent) {

		myVars.notExecutedEvent = {};
	}

	// if (!myVars.parsingTagLOC) {
	// myVars.parsingTagLOC = {};
	// }

	if (!myVars.DoNotFireEvent) {
		myVars.DoNotFireEvent = [];
	}

	myVars.postponedEvent = [];

	if (!myVars.toWaitLists) {
		myVars.toWaitLists = {};
	}

	myVars.parsingEvent = {};

	var firstRace = "";
	for ( var raceInfo in myVars.races) {
		if (firstRace == "") {
			firstRace = raceInfo;
		}

		var curr_race = myVars.races[raceInfo];

		// ///For checking bogus(conflict) race

		var event1String = "" + curr_race.event1.id + "__"
				+ curr_race.event1.type;
		curr_race.event1.eventString = event1String;
		var event2String = "" + curr_race.event2.id + "__"
				+ curr_race.event2.type;
		curr_race.event2.eventString = event2String;

		if (isParsing(event2String)) {

			if (isParsing(event2String) || isAsynScript(event2String)
					|| isSrc(event2String)) {

				if (parseInt(curr_race.event2.loc) < parseInt(curr_race.event1.loc)) {
					console
							.error("Bogus Race detected! This race can't be replayed!!!");
					continue;
				}
			}

		}

		myVars.notExecutedEvent[event1String] = true;
		myVars.notExecutedEvent[event2String] = true;

		// //for checking event handler races.
		consoleWarn_Lu(raceInfo + ": " + event1String + "   --->>>   "
				+ event2String)
		if (raceInfo == firstRace) {
			if (curr_race.varName == curr_race.event1.eventString) {
				consoleWarn_Lu(""
						+ curr_race.varName
						+ ":>>> Event handler type race detected, simply ignore executing the event handler firing!!\n");
				// continue;
				myVars.notExecutedEvent[event1String] = false;
				myVars.DoNotFireEvent.push(event1String);
				break;
			}

			if (curr_race.varName == curr_race.event2.eventString) {
				consoleWarn_Lu(""
						+ curr_race.varName
						+ ":>>> Event handler type race detected, simply ignore executing the event handler firing in the next run!!\n");
				// continue;
				// break;
			}

		} else {
			if (myVars.notExecutedEvent[event1String] == false
					|| myVars.notExecutedEvent[event2String] == false) {
				continue;
			}
			if (event1String == myVars.races[firstRace].varName
					|| event2String == myVars.races[firstRace].varName) {

			} else {
				continue;
			}
		}

		// / for checking parsing, src, async script after window.onload
		if (curr_race.event1.eventString == "Lu_window__onload") {
			if (isAsynScript(curr_race.event2.eventString)
					|| isParsing(curr_race.event2.eventString)
					|| isSrc(curr_race.event2.eventString)) {
				console
						.error(""
								+ curr_race.varName
								+ ":>>> Window.onload conflict race detected ! ignore race!!\n");
				myVars.notExecutedEvent[event1String] = false;
				myVars.notExecutedEvent[event2String] = false;

			}
		}

		if (curr_race.event1.eventString == "Lu_DOM__onDOMContentLoaded") {
			if (isParsing(curr_race.event2.eventString)) {
				console
						.error(""
								+ curr_race.varName
								+ ":>>> document.onDOMContentLoaded conflict race detected ! ignore race!!\n");
				myVars.notExecutedEvent[event1String] = false;
				myVars.notExecutedEvent[event2String] = false;

			}
		}

		// if(!myVars.races[raceInfo].event1.loc.endsWith("_LOC")){
		// myVars.parsingTagLOC[event1String]=myVars.races[raceInfo].event1.loc;
		// }
		// if(!myVars.races[raceInfo].event2.loc.endsWith("_LOC")){
		// myVars.parsingTagLOC[event2String]=myVars.races[raceInfo].event2.loc;
		// }

		if (!myVars.toWaitLists[event2String]) {
			myVars.toWaitLists[event2String] = [];
		}

		// myVars.parsingEvent

		if (event1String.endsWith("__parsed")) {
			if (parseInt(curr_race.event1.loc)) {
				myVars.parsingEvent[parseInt(curr_race.event1.loc)] = event1String;
			}
		}

		if (event2String.endsWith("__parsed")) {
			if (parseInt(curr_race.event2.loc)) {
				myVars.parsingEvent[parseInt(curr_race.event2.loc)] = event2String;
			}
		}

		// //to check conflict races.
		// if the second event of the not race1 race is race1.event1
		if (event2String == myVars.races[firstRace].event1.eventString) {

			// if the current race is parse-> race1.event1
			if (curr_race.event1.type.endsWith("__parsed")) {

				// if race1 is race1.event1-> parsing
				if (myVars.races[firstRace].event2.type.endsWith("__parsed")) {

					// assert they are all valid loc
					if (!curr_race.event1.loc.endsWith("_LOC")
							&& !myVars.races[firstRace].event2.loc
									.endsWith("_LOC")) {
						// consoleWarn_Lu("Cyclic event detected, ignored!!"
						// + curr_race.event1.loc + ", "
						// + myVars.races["race1"].event2.loc);
						if (parseInt(curr_race.event1.loc) > parseInt(myVars.races[firstRace].event2.loc)) {
							console.warn("Cyclic event between " + raceInfo
									+ " and " + firstRace
									+ " detected, ignored!!");
							continue;
						}
					}
				}
			}
		}

		if (check_cycle(event1String, event2String,
				myVars.races[firstRace].event1.eventString)) {
			console.warn("Cyclic event between " + raceInfo + " and "
					+ firstRace + " detected, ignored!!");
			continue;
		}
		// else {

		myVars.toWaitLists[event2String].push(event1String);
		// }

	}

	// var Lu_lastParsing = null;
	// for ( var parsing in myVars.parsingEvent) {
	//
	// console.log("Parsing: " + myVars.parsingEvent[parsing]);
	//
	// if (Lu_lastParsing == null) {
	// Lu_lastParsing = myVars.parsingEvent[parsing];
	// continue;
	// }
	// if (!myVars.toWaitLists[myVars.parsingEvent[parsing]]) {
	// myVars.toWaitLists[myVars.parsingEvent[parsing]] = [];
	// }
	// myVars.toWaitLists[myVars.parsingEvent[parsing]].push(Lu_lastParsing);
	//
	// Lu_lastParsing = myVars.parsingEvent[parsing];
	// }
	//
	// for ( var raceInfo in myVars.races) {
	// var curr_race = myVars.races[raceInfo];
	//
	// // if (check_cycle(event1String, event2String,
	// // myVars.races[firstRace].event1.eventString)) {
	// // console.warn("Cyclic event between " + raceInfo + " and "
	// // + firstRace + " detected, ignored!!");
	// // }
	//
	// }

	consoleWarn_Lu(myVars.notExecutedEvent);
	consoleWarn_Lu(myVars.toWaitLists);
}
combRaces();

function check_cycle(EventString1, EventString2, Prio_Event) {
	// race2: image3__onload(Prio_Event) --->>> image1__onload
	// race1: image1__onload --->>> image2__onload
	// race3: image2__onload(EventString1) --->>> image3__onload(EventString2)
	if (EventString2 != Prio_Event)
		return false;

	Lu_visited = [];
	Lu_to_visit = [];
	Lu_to_visit.push(EventString1);
	Lu_visited.push(EventString2);

	if (check_cycle_2(EventString1, Prio_Event)) {
		return true;
	} else {
		return false;
	}

}

function check_cycle_2(EventString, Prio_Event) {

	Lu_visited.push(EventString);

	// console.log(EventString);

	if (myVars.toWaitLists[EventString]) {

		if (myVars.toWaitLists[EventString].indexOf(Prio_Event) != -1) {
			// console.log("found!");
			return true;
		}

		else {
			// console.log("not found!");

			// console.log(myVars.toWaitLists[EventString]);

			for ( var toWait in myVars.toWaitLists[EventString]) {
				// console.log("to wait: "
				// +myVars.toWaitLists[EventString][toWait]);
				if (check_cycle_2(myVars.toWaitLists[EventString][toWait],
						Prio_Event)) {
					return true;
				}
			}
		}
	}
}

myVars.count_check_event_handler_change = 0;

function insertScript(text) {
	var s = document.createElement('script');
	// s.type = 'text/javascript';
	var code = text;
	try {
		s.appendChild(document.createTextNode(code));
		// insertAfter(s, element);
	} catch (e) {
		s.text = code;
		// insertAfter(s, element);
	}
	s["class"] = "Lu_Inst";
	consoleLog_Lu("---------------------------------------Insert script----------------------------------------\n"
			+ s.outerHTML);
	if (document.getElementsByTagName('body').length >= 1) {
		document.getElementsByTagName('body')[0].appendChild(s);
		// document.body.lastChild.setAttribute("class", "Lu_Inst")
		// consoleLog_Lu(document.getElementsByTagName('body')[0].outerHTML);
	}
}

function insertScript_src(text, function_name, id) {
	var s = document.createElement('script');
	// s.type = 'text/javascript';
	var code = text;
	try {
		s.appendChild(document.createTextNode(code));
		// insertAfter(s, element);
	} catch (e) {
		s.text = code;
		// insertAfter(s, element);
	}

	if (id) {
		s.setAttribute("id", id);
	}
	s.setAttribute("class", "Lu_Inst");
	s.setAttribute("finish_when_loaded", true);

	if (function_name) {
		s.setAttribute("onclick", function_name + "()");
	}
	consoleLog_Lu("---------------------------------------Insert script----------------------------------------\n"
			+ s.outerHTML);
	if (document.getElementsByTagName('body').length >= 1) {
		document.getElementsByTagName('body')[0].appendChild(s);
		// document.body.lastChild.setAttribute("class", "Lu_Inst")
		// consoleLog_Lu(document.getElementsByTagName('body')[0].outerHTML);
	}
}

function insertPostponingTag(keyword) {

}

function handlePostponedEvent() {
	// use a temp container because we don't want infinite loop when an event is
	// not ready
	myVars.postponedEventTemp = [];

	// move all the postponed event into the temp container
	while (myVars.postponedEvent.length > 0) {
		myVars.postponedEventTemp.push(myVars.postponedEvent.pop());
	}

	// fire everyting in the temp container, if the event still needs to be
	// postpone, then it will go back to myVars.postponedEvent
	while (myVars.postponedEventTemp.length > 0) {
		myVars.postponedEventTemp.pop().call();
	}

	// after firing the temp container, if there is still something in the
	// myVars.postponedEvent, we need to setTimeout this function, because the
	// html might finished parsing, but some event are not fired, we can't only
	// rely on script after tag
	if (myVars.postponedEvent.length > 0) {
		var interval1 = setTimeout_Lu(function(event) {
			handlePostponedEvent();
		}, timerInterval);
	}
}

function insertEventFiring(element, type) {
	var s = document.createElement('script');
	s.type = 'text/javascript';
	var code = "" + getID(element) + "[\"" + type + "\"]();";
	try {
		s.appendChild(document.createTextNode(code));
		// insertAfter(s, element);
	} catch (e) {
		s.text = code;
		// insertAfter(s, element);
	}
	s.class = "Lu_Inst";
	consoleLog_Lu(s);
	if (document.getElementsByTagName('body').length >= 1) {
		document.getElementsByTagName('body')[0].appendChild(s);
		// consoleLog_Lu(document.getElementsByTagName('body')[0].outerHTML);
	}
}

function autoFireEventListener(element, event) {
	var event1; // The custom event that will be created
	if (document.createEvent) {
		event1 = document.createEvent("HTMLEvents");
		event1.initEvent(event, true, true);
	} else {
		event1 = document.createEventObject();
		event1.eventType = event;
	}

	event1.eventName = event;

	if (document.createEvent) {
		element.dispatchEvent(event1);
	} else {
		element.fireEvent("on" + event1.eventType, event);
	}
}

function autoFireEventListenerById(id, event) {
	var element = document.getElementById(id);
	var event1; // The custom event that will be created
	if (document.createEvent) {
		event1 = document.createEvent("HTMLEvents");
		event1.initEvent(event, true, true);
	} else {
		event1 = document.createEventObject();
		event1.eventType = event;
	}

	event1.eventName = event;

	if (document.createEvent) {
		element.dispatchEvent(event1);
	} else {
		element.fireEvent("on" + event1.eventType, event);
	}

	var onEvent = "on" + event1.eventType;
	if ((element[onEvent] || false) && typeof element[onEvent] == 'function') {
		element[onEvent](element);
	}
}

function insertElementIntoDom(element) {
	if (document.getElementsByTagName('body').length >= 1) {
		document.getElementsByTagName('body')[0].appendChild(element);
		// consoleLog_Lu(document.getElementsByTagName('body')[0].outerHTML);
	} else {
		document.getElementsByTagName('head')[0].appendChild(element);
	}
}

function printRaceStatus() {
	for ( var raceInfo in myVars.races) {
		consoleLog_Lu("--------------------------------------------------------");
		consoleLog_Lu("raceInfo: " + raceInfo);
		consoleLog_Lu("Event 1: ");
		consoleLog_Lu("id      : " + myVars.races[raceInfo].event1.id);
		consoleLog_Lu("type    : " + myVars.races[raceInfo].event1.type);
		consoleLog_Lu("executed: " + myVars.races[raceInfo].event1.executed);
		consoleLog_Lu("Event 2: ");
		consoleLog_Lu("id      : " + myVars.races[raceInfo].event2.id);
		consoleLog_Lu("type    : " + myVars.races[raceInfo].event2.type);
		consoleLog_Lu("executed: " + myVars.races[raceInfo].event2.executed);
	}
}
myVars.event_handler_types = [ "animationend", "animationiteration",
		"animationstart", "onabort", "onafterprint", "onbeforeprint",
		"onbeforeunload", "onblur", "oncanplay", "oncanplaythrough",
		"onchange", "onclick", "oncontextmenu", "oncopy", "oncut",
		"ondblclick", "ondrag", "ondragend", "ondragenter", "ondragleave",
		"ondragover", "ondragstart", "ondrop", "ondurationchange", "onemptied",
		"onended", "onerror", "onfocus", "onfocusin", "onfocusout",
		"onhashchange", "oninput", "oninvalid", "onkeydown", "onkeypress",
		"onkeyup", "onload", "onloadeddata", "onloadedmetadata", "onloadstart",
		"onmessage", "onmousedown", "onmouseenter", "onmouseleave",
		"onmousemove", "onmouseout", "onmouseover", "onmouseup",
		"onmousewheel", "onoffline", "ononline", "onopen", "onpagehide",
		"onpageshow", "onpaste", "onpause", "onplay", "onplaying",
		"onpopstate", "onprogress", "onratechange", "onreset", "onresize",
		"onscroll", "onsearch", "onseeked", "onseeking", "onselect", "onshow",
		"onstalled", "onstorage", "onsubmit", "onsuspend", "ontimeupdate",
		"ontoggle", "ontouchcancel", "ontouchend", "ontouchmove",
		"ontouchstart", "onunload", "onvolumechange", "onwaiting", "onwheel" ];
function check_event_handler_change(element_id) {
	if (true)
		return;
	myVars.count_check_event_handler_change++;
	// myVars.event_handler_types = [ "onclick", "onload", "onabort", "onblur",
	// "onchange", "ondblclick", "onerror", "onfocus", "onkeydown",
	// "onkeypress", "onkeyup", "onmousedown", "onmousemove",
	// "onmouseout", "onmouseover", "onmouseup", "onreset", "onresize",
	// "onselect", "onsubmit", "onunload", "onscroll" ];

	if (element_id != null) {
		if (document.getElementById(element_id) != null) {

			var this_element = document.getElementById(element_id);

			// console.log("hahaha");

			var curr_eh;
			for ( var eh_type_index in myVars.event_handler_types) {

				curr_eh = this_element[myVars.event_handler_types[eh_type_index]];
				if (curr_eh) {
					step1_classify_ehs(this_element, curr_eh,
							myVars.event_handler_types[eh_type_index]);
				}
			}

		}
	} else {
		// console.log("nonono");

		var items = document.querySelectorAll("*");
		var element, i, len = items.length;
		for (i = 0; i < len; i++) {
			element = items[i];
			if (element.classList.contains("Lu_Inst"))
				continue;

			if (element == document.body)
				continue;
			if (element == document.head)
				continue;

			if (!element.ehs) {
				element.ehs = new Object();
			}
			if (!element.ehs_original) {
				element.ehs_original = new Object();
			}

			if (!element.ehs2) {
				element.ehs2 = new Object();
			}
			if (!element.ehs2_original) {
				element.ehs2_original = new Object();
			}

			var curr_eh;
			for ( var eh_type_index in myVars.event_handler_types) {

				curr_eh = element[myVars.event_handler_types[eh_type_index]];
				if (curr_eh) {
					step1_classify_ehs(element, curr_eh,
							myVars.event_handler_types[eh_type_index]);
				}
			}
		}
		if (window["onload"]) {
			step1_classify_ehs(window, window["onload"], "onload");
		}
	}
}
function step1_classify_ehs(element, curr_eh, eh_type) {
	// if the object that store the event handlers is null, create one
	if (!element.ehs) {
		element.ehs = new Object();
	}
	if (!element.ehs_original) {
		element.ehs_original = new Object();
	}

	if (!element.ehs2) {
		element.ehs2 = {};
	}
	if (!element.ehs2_original) {
		element.ehs2_original = {};
	}

	if (element.ehs[eh_type]) {
		var old_eh = element.ehs[eh_type];// old event handler
		if (old_eh == curr_eh.toString()) {
		} else {
			// means the event handler is changed,need to instrument
			element.ehs_original[eh_type] = curr_eh.toString();
			// print("*****************************" + getID(element)
			// + " has ::---> " + eh_type
			// + "*****************************");
			// print("-->Different old event handler: " + eh_type);
			// print(old_eh + "\n--- VS ---\n::" + curr_eh.toString());
			step2_instrument_event_handlers(element, curr_eh, eh_type);
			// print("------------------------End of the
			// EH--------------------------------------\n\n");
		}
	} else {// if the old event handler doesn't exist
		// need to instrument
		element.ehs_original[eh_type] = curr_eh.toString();
		// print("*****************************" + getID(element) + " has ::--->
		// "
		// + eh_type + "*****************************");
		// print("-->Empty old event handler: " + eh_type);
		// print(old_eh + "\n--- VS ---\n::" + curr_eh.toString());
		step2_instrument_event_handlers(element, curr_eh, eh_type);
		// print("------------------------End of the
		// EH--------------------------------------\n\n");
	}
}
function step2_instrument_event_handlers(element, curr_eh, eh_type) {

	// consoleLog_Lu("For id: " + getID(element) + " , type: " + eh_type);
	var EventString = "" + getID(element) + "__" + eh_type;
	var this_done = false;
	var whetherAttach = true;
	var waitForParse = false;

	if (typeof myVars.notExecutedEvent[EventString] != "undefined") {
		print("2-!!Racing event detected: " + element + " : " + getID(element));

		if (typeof myVars.toWaitLists[EventString] != "undefined") {
			for (toWait in myVars.toWaitLists[EventString]) {
				if (myVars.notExecutedEvent[myVars.toWaitLists[EventString][toWait]]) {
					consoleWarn_Lu("Pre scan: " + EventString
							+ " , need to wait for");
					consoleWarn_Lu("--->>: "
							+ myVars.toWaitLists[EventString][toWait]);
					consoleWarn_Lu(" ");

					if (myVars.toWaitLists[EventString][toWait]
							.endsWith("_parsed")) {
						waitForParse = true;
					}

				}
			}
		}

		element[eh_type] = function replace_e_Lu(event) {
			if (myVars.notExecutedEvent[EventString] == false)
				return;
			consoleLog_Lu("EventString Trying to execute: " + EventString);
			var Postpone_count = 1;

			innerJob();

			function innerJob() {

				if (this_done == true) {
					return;
				}

				if (myVars.notExecutedEvent[EventString] == false)
					return;
				var finishedWaiting = true;
				// consoleLog_Lu(EventString);
				if (typeof myVars.toWaitLists[EventString] != "undefined") {
					for (toWait in myVars.toWaitLists[EventString]) {
						if (myVars.notExecutedEvent[myVars.toWaitLists[EventString][toWait]]) {

							consoleWarn_Lu("Attemp : " + Postpone_count
									+ ", Runtime:" + EventString
									+ " , need to wait for");

							consoleWarn_Lu("--->>: "
									+ myVars.toWaitLists[EventString][toWait]);
							consoleWarn_Lu(" ");

							Postpone_count++;
							finishedWaiting = false;
						}
					}
				}
				if (Postpone_count > MAX_Postpone_count || finishedWaiting) {
					consoleLog_Lu("--EventString is firing: " + EventString);
					if (!myVars.toWaitLists[EventString]) {
						consoleWarn_Lu(EventString
								+ " , doesn't need to wait, Executed!!!");
					} else {
						consoleWarn_Lu(EventString
								+ " , finished waiting, executed!!!");
					}
					// clearInterval(interval1);
					if (!isWindow(element)) {
						if (!element.hasAttribute("finish_when_loaded")) {

							// consoleWarn_Lu(
							// "From : "
							// + JSON
							// .stringify(myVars.notExecutedEvent),
							// null, 2);

							RacingEventDone(EventString);
							// myVars.notExecutedEvent[EventString] = false;
							//
							// consoleWarn_Lu(
							// "To : "
							// + JSON
							// .stringify(myVars.notExecutedEvent),
							// null, 2);
							// consoleWarn_Lu(" ");

							consoleWarn_Lu(getID(element) + " set finished!");
						} else {
							consoleWarn_Lu(getID(element)
									+ " has attribute: finish_when_loaded, not set finished!");
						}
					} else {
						// consoleWarn_Lu("From : "
						// + JSON.stringify(myVars.notExecutedEvent),
						// null, 2);

						RacingEventDone(EventString);
						// myVars.notExecutedEvent[EventString] = false;

						// consoleWarn_Lu("To : "
						// + JSON.stringify(myVars.notExecutedEvent),
						// null, 2);
						// consoleWarn_Lu(" ");
						//
						// consoleWarn_Lu(getID(element) + " set
						// finished!");

					}
					// consoleLog_Lu("curr eh: " + curr_eh.toString());

					this_done = true;

					try {
						curr_eh.call(arguments);
					} catch (err) {
						console.error(err.message);
					}

				}
				// return finishedWaiting;

				if (finishedWaiting == false) {
					// var interval1 = setTimeout(function(event) {
					// consoleLog_Lu('Postpone time: ' + Postpone_count);
					// Postpone_count++;
					// if (innerJob() == true) {
					// clearInterval(interval1);
					// }
					// }, 0);
					myVars.postponedEvent.push(innerJob);
					var interval1 = setTimeout_Lu(function(event) {
						handlePostponedEvent();
					}, timerInterval);
				}

			}

		}
		element.ehs[eh_type] = element[eh_type].toString();
		print("The event handler for has been replaced as racing event!");
		// print(element.ehs[eh_type].toString());

		if (eh_type == "onload" || eh_type == "onDOMContentLoaded") {

			if (!isWindow(element)) {
				fireEventListnerAfterRegisteration = true;
				whetherAttach = false;
			} else {
				fireEventListnerAfterRegisteration = false;
				whetherAttach = true;
			}

		} else {// other type of eh will try-fire immediately
			fireEventListnerAfterRegisteration = true;
			whetherAttach = true;
		}

		if (myVars.DoNotFireEvent.indexOf(EventString) != -1) {
			consoleWarn_Lu("Event handler Race! do not attach! Do not fire!!");
			fireEventListnerAfterRegisteration = false;
			whetherAttach = false;
		}

		if (fireEventListnerAfterRegisteration) {
			consoleWarn_Lu("2--Automatically Firing EH: " + EventString);

			// var Lu_Fire_EventListner = true;
			// var Lu_Attach_EventListner = true;
			// var Lu_Attach_Timer = true;
			// var Lu_Ajax = true;
			// var Lu_src = true;

			if (Lu_Fire_EventListner) {
				consoleLog_Lu("****racing event fire : " + EventString);
				if (EventString == "Lu_DOM__onDOMContentLoaded") {
					document.addEventListener_Lu("DOMContentLoaded",
							element[eh_type], false);
				} else {
					element[eh_type].call();
				}
			}
			// this.addEventListener_Lu(type, new_fn, capture);
		} else {
			consoleWarn_Lu("--Do Not Automatically Firing EH: " + EventString);
		}

		if (!whetherAttach) {
			// this.addEventListener_Lu(type, new_fn, capture);
			consoleWarn_Lu("--Do not attach : " + EventString);
			try {
				element.removeAttribute(eh_type);
				element[eh_type] = null;
			} catch (err) {

			}
		} else {
			consoleWarn_Lu("--Attach: " + EventString);
		}

		fireEventListnerAfterRegisteration = true;

	} else {
		// consoleLog_Lu("Not found, go default!");
		element[eh_type] = function() {
			// consoleLog_Lu("Non-racing event fire : " + EventString);
			curr_eh.call(arguments);
			check_event_handler_change();

		}
		element.ehs[eh_type] = element[eh_type].toString();
		// print("The event handler for has been replaced as default other
		// event!");
		// print(element.ehs[eh_type].toString());
	}
}

function print(input_string) {
	consoleLog_Lu(input_string);
}
function debug(input_string) {
	// print(input_string);
}

function set_attr(id, atr, value) {
	document.getElementById(id).atr = value;
}

function busy_delay(milliseconds) {// to wait for certain milliseconds(1/1000
	// second)
	// console.warn("Start to wait for : " + milliseconds + " milliseconds");
	var start = Date.now();
	while (true) {
		if ((Date.now() - start) > milliseconds) {

			break;
		}
	}
	consoleWarn_Lu("Finished waited : " + milliseconds + " milliseconds");
}

function triggerEvent(el, type) {
	if ((el[type] || false) && typeof el[type] == 'function') {
		el[type](el);
	}
}

function string_replace_all(string, old_str, new_str) {
	var re = new RegExp(old_str, "g");
	return string.replace(re, new_str);
}

function isRacingEventFinishedWaiting(EventString) {
	var finishedWaiting = true;

	if (typeof myVars.toWaitLists[EventString] != "undefined") {
		for (toWait in myVars.toWaitLists[EventString]) {
			// console.log(myVars.toWaitLists[EventString][toWait]+":
			// "+myVars.notExecutedEvent[myVars.toWaitLists[EventString][toWait]]);
			if (myVars.notExecutedEvent[myVars.toWaitLists[EventString][toWait]] == true) {
				// console.log(EventString+ " waiting not finished");
				return false;
			}
		}
	}
	return finishedWaiting;
}

var RacingEventDoneSequence = [];
function RacingEventDone(EventString) {

	consoleWarn_Lu("Important: **RacingEventDone: " + EventString);

	if (isRacingEventFinishedWaiting(EventString) == false) {
		consoleError_Lu("Replay fail to enforce the order of the race, the Event is: "
				+ EventString);
	}

	// if()

	myVars.notExecutedEvent[EventString] = false;
	RacingEventDoneSequence.push(EventString);
	consoleLog_Lu(EventString + " is executed!");

}

function printRacingEventDoneSequence() {
	consoleLog_Lu("The event finish sequence is :");
	for ( var event in RacingEventDoneSequence) {
		consoleLog_Lu(RacingEventDoneSequence[event]);
	}
}

var addEventListener_Lu = EventTarget.prototype.addEventListener;
// store original
EventTarget.prototype.addEventListener = function(type, fn, capture) {
	this.addEventListener_Lu = addEventListener_Lu;
	// step 1. check
	var on_type = "on" + type;
	var newfn;

	print("-->addEventListener: " + getID(this) + " ->  " + type
			+ "----------------------------------------\n");

	var EventString;
	EventString = getID(this);

	EventString = EventString + "__" + on_type;

	// consoleLog_Lu(eventString + ": \n" + fn.toString());

	if (!this.ehs) {
		this.ehs = new Object();
	}
	if (!this.ehs_original) {
		this.ehs_original = new Object();
	}

	if (!this.ehs2) {
		this.ehs2 = {};
	}
	if (!this.ehs2_original) {
		this.ehs2_original = {};
	}

	if (!this.ehs2[type]) {
		this.ehs2[type] = [];
	}

	if (!this.ehs2_original[type]) {
		this.ehs2_original[type] = [];
	}

	var fnString = "";
	// fnString = fn.toString();
	if (this.ehs2_original[type].indexOf(fn.toString()) == -1) {
		// consoleLog_Lu("New eh2 added: " + type + " on : " + EventString);
		this.ehs2_original[type].push(fn.toString());
	} else {
		// consoleLog_Lu("Old eh2 exist: " + type + " on : " + EventString);
	}

	var whetherAttach = true;
	var waitForParse = false;
	var this_done = false;
	if (typeof myVars.notExecutedEvent[EventString] != "undefined") {
		print("1-!!Racing event detected: " + EventString);

		if (typeof myVars.toWaitLists[EventString] != "undefined") {
			for (toWait in myVars.toWaitLists[EventString]) {
				if (myVars.notExecutedEvent[myVars.toWaitLists[EventString][toWait]]) {
					consoleWarn_Lu("Pre scan: " + EventString
							+ " , need to wait for");
					consoleWarn_Lu("--->>: "
							+ myVars.toWaitLists[EventString][toWait]);
					consoleWarn_Lu(" ");

					if (myVars.toWaitLists[EventString][toWait]
							.endsWith("_parsed")) {
						waitForParse = true;
					}

				}
			}
		}

		// /////////////////////////////////////////////////////////////////////////

		function replace_e_Lu() {
			// if (myVars.notExecutedEvent[EventString] == false)
			// return;
			consoleLog_Lu("EventString Trying to execute: " + EventString);
			var Postpone_count = 1;

			innerJob();

			var waitForParse = false;

			function innerJob() {

				if (this_done == true) {
					return;
				}

				var finishedWaiting = true;
				// consoleLog_Lu(EventString);
				if (typeof myVars.toWaitLists[EventString] != "undefined") {
					for (toWait in myVars.toWaitLists[EventString]) {
						if (myVars.notExecutedEvent[myVars.toWaitLists[EventString][toWait]]) {
							consoleWarn_Lu("Attemp : " + Postpone_count
									+ ", Runtime:" + EventString
									+ " , need to wait for");
							consoleWarn_Lu("--->>: "
									+ myVars.toWaitLists[EventString][toWait]);
							consoleWarn_Lu(" ");

							Postpone_count++;
							finishedWaiting = false;
						}
					}
				}
				if (Postpone_count > MAX_Postpone_count || finishedWaiting) {
					consoleLog_Lu("--EventString is firing: " + EventString);
					if (!myVars.toWaitLists[EventString]) {
						consoleWarn_Lu(EventString
								+ " , doesn't need to wait, Executed!!!");
					} else {
						consoleWarn_Lu(EventString
								+ " , finished waiting, executed!!!");
					}
					// clearInterval(interval1);

					consoleWarn_Lu("1-From : "
							+ JSON.stringify(myVars.notExecutedEvent), null, 2);

					RacingEventDone(EventString);
					// myVars.notExecutedEvent[EventString] = false;

					consoleWarn_Lu("To   : "
							+ JSON.stringify(myVars.notExecutedEvent), null, 2);
					consoleWarn_Lu(" ");
					this_done = true;

					try {
						fn.call(arguments);
					} catch (err) {
						console.error(err.message);
					}

				}

				if (finishedWaiting == false) {

					myVars.postponedEvent.push(innerJob);
					var interval1 = setTimeout_Lu(function(event) {
						handlePostponedEvent();
					}, timerInterval);
				}

			}

		}

		this.ehs2[type].push(replace_e_Lu.toString());
		print("The event handler for has been replaced as racing event!");

		if (on_type == "onload" || on_type == "onDOMContentLoaded") {

			if (!isWindow(this)) {
				fireEventListnerAfterRegisteration = true;
				whetherAttach = false;
			} else {
				fireEventListnerAfterRegisteration = false;
				whetherAttach = true;
			}

		} else {// other type of eh will try-fire immediately
			fireEventListnerAfterRegisteration = true;
			whetherAttach = true;
		}

		if (myVars.DoNotFireEvent.indexOf(EventString) != -1) {
			consoleWarn_Lu("Event handler Race! do not attach! Do not fire!!");
			fireEventListnerAfterRegisteration = false;
			whetherAttach = false;
		}

		// if(onPassive.indexOf(type)!=-1 ){
		// if(EventString.startsWith("_script_extn.js__")){
		// fireEventListnerAfterRegisteration=true;
		// }
		// else{
		// fireEventListnerAfterRegisteration=false;
		// }
		// }

		if (fireEventListnerAfterRegisteration) {
			consoleWarn_Lu("1--Automatically Firing EH: " + EventString);

			// var Lu_Fire_EventListner = true;
			// var Lu_Attach_EventListner = true;
			// var Lu_Attach_Timer = true;
			// var Lu_Ajax = true;
			// var Lu_src = true;

			if (Lu_Fire_EventListner) {
				consoleLog_Lu("****racing event fire : " + EventString);

				if (EventString == "Lu_DOM__onDOMContentLoaded") {
					document.addEventListener_Lu("DOMContentLoaded",
							replace_e_Lu, false);
				} else {
					replace_e_Lu.call(arguments);
				}

			}
			// this.addEventListener_Lu(type, new_fn, capture);
		} else {
			consoleWarn_Lu("--Do Not Automatically Firing EH: " + EventString);
		}

		if (whetherAttach) {
			consoleWarn_Lu("--Attach: " + EventString);
			this.addEventListener_Lu(type, replace_e_Lu, capture);
		} else {
			consoleWarn_Lu("--Do not attach : " + EventString);
		}

		fireEventListnerAfterRegisteration = true;

	}

	else {
		// consoleLog_Lu("Not found, go default!");
		function replace_e_Lu() {
			// consoleLog_Lu("Non-racing event fire : " + EventString);
			fn.call(arguments);
			check_event_handler_change();
		}
		this.ehs2[type].push(replace_e_Lu.toString());
		// print(EventString + " : has been replaced as default other event!");
		this.addEventListener_Lu(type, replace_e_Lu, capture);
		// print(element.ehs[eh_type].toString());
	}

	// ///////////////////////////////////////////////////////////////////////////////////

}

var removeEventListener_Lu = EventTarget.prototype.removeEventListener;
// store original
EventTarget.prototype.removeEventListener = function(type, fn, capture) {
	print("-------------------------removeEventListener: " + type
			+ "----------------------------------------\n");
	this.removeEventListener_Lu = removeEventListener_Lu;
	// step 1. check
	var on_type = "on" + type;
	var newfn;
	// print("Id : " + this.id);
	// print("Type: " + on_type);
	// print("Fn :" + fn.toString());

	if (!this.ehs2) {
		this.ehs2 = {};
	}
	if (!this.ehs2[type]) {
		// this.ehs2.push(type);
		this.ehs2[type] = [];
	}

	if (!this.ehs2_original) {
		this.ehs2_original = {};
	}
	if (!this.ehs2_original[type]) {
		this.ehs2_original[type] = [];
	}

	if (this.ehs2_original[type].indexOf(fn.toString()) == -1) {
		// print("remove eh: " + type + " From : " + this.id);
		this.ehs2_original[type].pop(fn.toString());

	} else {
		// print("fail to remove eh" + type + " From : " + this.id);
	}
	//
	// print(this.ehs2[type].indexOf("hi"));
	// print(this.ehs2[type].indexOf(fn.toString()));

	this.removeEventListener_Lu(type, fn, capture);
}

var appendChild_Lu = Node.prototype.appendChild;
// store original]
Node.prototype.appendChild = function(newElement) {
	this.appendChild_Lu = appendChild_Lu;
	var oldElement = this;
	if (newElement.classList && !newElement.classList.contains("Lu_Inst")) {
		if (newElement.tagName.toLowerCase() == 'script') {// this is for
			if (newElement.src) {
				consoleLog_Lu("appendChild: Node is: \n" + newElement.src);

				if (newElement.src.indexOf("Unknown_83_filename") != -1) {
					consoleLog_Lu("Node is abandoned");
				}

				var original_src = newElement.src;// store the original src
				var new_id = "";// give an id if there isn't

				var src_split = original_src.split("/");
				new_id = "_script_" + src_split[src_split.length - 1];

				var RacingEvent = new_id + "__onclick";
				if (myVars.notExecutedEvent[RacingEvent]) {
					consoleWarn_Lu("1-Instrument the script generated script!!!");
					newElement.addEventListener("load", function() {
						consoleWarn_Lu(new_id
								+ "-----------Async Script Loaded");
						RacingEventDone(RacingEvent);
					}, false);

					var try_count = 0;
					window[RacingEvent + "_try_attach"] = function() {
						console.log("Test here");
						if (!isRacingEventFinishedWaiting(RacingEvent)) {
							try_count++;
							console.log("Retry count: " + try_count + "  :  "
									+ RacingEvent);
							setTimeout_Lu(window[RacingEvent + "_try_attach"],
									200);
							return;
						} else {
							console.log(RacingEvent
									+ "_try_attach is sucessfully executed");
							oldElement.appendChild_Lu(newElement);
						}
					}
					window[RacingEvent + "_try_attach"]();
				}

			}
		}

	}

	// this.appendChild_Lu(newElement);
}
var insertBefore_Lu = Node.prototype.insertBefore;
// store original
Node.prototype.insertBefore = function(newElement, referenceElement) {
	this.insertBefore_Lu = insertBefore_Lu;
	var oldElement = this;
	if (newElement.classList && !newElement.classList.contains("Lu_Inst")) {
		if (newElement.tagName.toLowerCase() == 'script') {// this is for
			if (newElement.src) {
				consoleLog_Lu("insertBefore: Node is: \n" + newElement.src);

				if (newElement.src.indexOf("Unknown_83_filename") != -1) {
					consoleLog_Lu("Node is abandoned");
				}

				var original_src = newElement.src;// store the original src
				var new_id = "";// give an id if there isn't

				var src_split = original_src.split("/");
				new_id = "_script_" + src_split[src_split.length - 1];

				var RacingEvent = new_id + "__onclick";
				if (myVars.notExecutedEvent[RacingEvent]) {
					consoleWarn_Lu("1-Instrument the script generated script!!!");
					newElement.addEventListener("load", function() {
						consoleWarn_Lu(new_id
								+ "-----------Async Script Loaded");
						RacingEventDone(RacingEvent);
					}, false);

					var try_count = 0;
					window[RacingEvent + "_try_attach"] = function() {
						console.log("Test here");
						if (!isRacingEventFinishedWaiting(RacingEvent)) {
							try_count++;
							console.log("Retry count: " + try_count + "  :  "
									+ RacingEvent);
							setTimeout_Lu(window[RacingEvent + "_try_attach"],
									200);
							return;
						} else {
							console.log(RacingEvent
									+ "_try_attach is sucessfully executed");
							oldElement.insertBefore_Lu(newElement,
									referenceElement);
						}
					}
					window[RacingEvent + "_try_attach"]();
				}

			}
		}

	}

	// this.insertBefore_Lu(newElement, referenceElement);
}

function myTrim(x) {
	return x.replace(/^\s+|\s+$|\n| /gm, '');
}

var setTimeout_Lu = window.setTimeout;
// store original
window.setTimeout = function(fn, delay) {

	if (Lu_Attach_Timer == true) {
		setTimeout_Lu(fn, delay);
	} else {
		// consoleLog_Lu("Do not setTimeOut");
	}
}

var setInterval_Lu = window.setInterval;
// store original
window.setInterval = function(fn, delay) {

	if (Lu_Attach_Timer == true) {
		setInterval_Lu(fn, delay);
	} else {
		// consoleLog_Lu("Do not setInterval");
	}
}

function getJSON(message) {
	return JSON.stringify(message, null, " ");
}

function Lu_is_array(object) {
	return Array.isArray(object);
}
function Lu_is_object(object) {
	if (typeof object == "object")
		return true;
	else
		return false;
}
