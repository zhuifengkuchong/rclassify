package RCrepair;

import java.io.IOException;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Node;

import RCrepair.Inst.InstTransformer;

public class RaceRepair {
	public static Document repair1A(Document dom, RaceInfo rInfo) {
		System.out
				.println(">>Repair Type 1A");

		Node firstEventNode = dom.getElementById(rInfo.getEvent1Id());
		Node secondEventNode = dom.getElementById(rInfo.getEvent2Id());

		// Utilities.Logging(firstEventNode.outerHtml(), "repair1A");
		// Utilities.Logging(secondEventNode.outerHtml(), "repair1A");

		secondEventNode.before(firstEventNode);
//		Utilities.Logging(dom.outerHtml(), "repair1A");

		return dom;

	}

	public static Document repair1B(Document dom, RaceInfo rInfo)
			throws IOException {
		System.out
				.println(">>Repair Type 1B");
		Node firstEventNode = dom.getElementById(rInfo.getEvent1Id());
		Node secondEventNode = dom.getElementById(rInfo.getEvent2Id());

		String eh_content = secondEventNode.attr(rInfo.getEvent2Type());
		String src_content = secondEventNode.attr("src");
		// Utilities.Logging(eh_content, "repair1B");
		// Utilities.Logging(src_content, "repair1B");

		String toInsertString = InstTransformer.repair1B(rInfo.getEvent2Id(),
				rInfo.getEvent2Type(), eh_content, src_content);

//		secondEventNode.removeAttr(rInfo.getEvent2Type());
		secondEventNode.removeAttr("src");

		firstEventNode.after(toInsertString);

//		Utilities.Logging(dom.outerHtml(), "repair1B");

		return dom;
	}

	public static Document repair1C(Document dom, RaceInfo rInfo)
			throws IOException {
		System.out
				.println(">>Repair Type 1C");
		Node firstEventNode = dom.getElementById(rInfo.getEvent1Id());
		Node secondEventNode = dom.getElementById(rInfo.getEvent2Id());

		// Utilities.Logging(firstEventNode.outerHtml(), "repair1C");
		// Utilities.Logging(secondEventNode.outerHtml(), "repair1C");

		String eh_content = secondEventNode.attr(rInfo.getEvent2Type());

		// Utilities.Logging(eh_content, "repair1C");

		String toInsertString = InstTransformer.repair1C(rInfo.getEvent2Id(),
				rInfo.getEvent2Type(), eh_content);
		// Utilities.Logging(toInsertString, "repair1C");
		secondEventNode.removeAttr(rInfo.getEvent2Type());
		firstEventNode.after(toInsertString);
//		Utilities.Logging(dom.outerHtml(), "repair1C");

		return dom;
	}

	public static Document repair1D(Document dom, RaceInfo rInfo)
			throws IOException {
		System.out
				.println(">>Repair Type 1D");

		Node firstEventNode = dom.getElementById(rInfo.getEvent1Id());
		Node secondEventNode = dom.getElementById(rInfo.getEvent2Id());

		// Utilities.Logging(firstEventNode.outerHtml(), "repair1D");
		// Utilities.Logging(secondEventNode.outerHtml(), "repair1D");

		secondEventNode.attr("disabled", "true");
		secondEventNode.attr("style", "visibility:hidden");

		String toInsertString = InstTransformer.repair1D(rInfo.getEvent2Id());
		firstEventNode.after(toInsertString);
//		Utilities.Logging(dom.outerHtml(), "repair1D");

		return dom;
	}

	public static Document repair2A(Document dom, RaceInfo rInfo)
			throws IOException {
		System.out
				.println("--------------------Repair Type 2A------------------------");

		Node firstEventNode = dom.getElementById(rInfo.getEvent1Id());
		Node secondEventNode = dom.getElementById(rInfo.getEvent2Id());

		String toInsertString = InstTransformer.repair2A(rInfo.getEvent2Id(),
				rInfo.getEvent2Type(), rInfo.getVarName());

		String safe_fn = "safe_node2.id_eh_type_varName()"; // we don't handle
															// the parameters
															// yet: onload =
															// init(x)

		safe_fn = safe_fn.replace("node2.id", rInfo.getEvent2Id());
		safe_fn = safe_fn.replace("eh_type", rInfo.getEvent2Type());
		safe_fn = safe_fn.replace("varName", rInfo.getVarName());

		secondEventNode.attr(rInfo.getEvent2Type(), safe_fn);
		secondEventNode.before(toInsertString);
//		Utilities.Logging(dom.outerHtml(), "repair2A");

		return dom;
	}

	public static Document repair2B(Document dom, RaceInfo rInfo)
			throws IOException {
		System.out
				.println("--------------------Repair Type 2B------------------------");

		Node firstEventNode = dom.getElementById(rInfo.getEvent1Id());
		Node secondEventNode = dom.getElementById(rInfo.getEvent2Id());

		String toInsertString = InstTransformer.repair2B(rInfo.getEvent2Id(),
				rInfo.getEvent2Type(), rInfo.getVarName());

		String safe_fn = "safe_node2.id_eh_type_varName()"; // we don't handle
															// the parameters
															// yet: onload =
															// init(x)

		safe_fn = safe_fn.replace("node2.id", rInfo.getEvent2Id());
		safe_fn = safe_fn.replace("eh_type", rInfo.getEvent2Type());
		safe_fn = safe_fn.replace("varName", rInfo.getVarName());

		/*
		 * System.out.println(toInsertString); System.out.println(safe_fn);
		 */

		secondEventNode.attr(rInfo.getEvent2Type(), safe_fn);
		secondEventNode.before(toInsertString);
//		Utilities.Logging(dom.outerHtml(), "repair2B");

		return dom;
	}

	public static Document repair2C(Document dom, RaceInfo rInfo) {
		System.out
				.println("--------------------Repair Type 2C------------------------");
		return dom;
	}

	public static Document repair3(Document dom, RaceInfo rInfo) {
		return dom;
	}

	public static boolean repairHTML(Document dom, RaceInfo rInfo)
			throws IOException {
		// if (rInfo.getRaceRepairType().isEmpty())
		// return null;
		
//		rInfo.print();
		
		RepairCount.count++;
		
		
		
		DomMeta dMeta = new DomMeta(dom);
		
		rInfo.print();
		
		
		
		if(!dMeta.Transform(rInfo)) {
			
			return false;
		}
//		rInfo.print();
		dMeta.getRepair(rInfo);
		
		if (Config.repairTypes.contains(rInfo.repairType)) {
//			rInfo.print();
		}

		 
		// System.out.println(rInfo.getRaceRepairType());
		if (canRepair1A(dom, rInfo)) {
			repair1A(dom, rInfo);
			rInfo.ev1.done=true;
			rInfo.ev2.done=true;
			rInfo.ev1.repairType="1A";
			rInfo.ev2.repairType="1A";
		}
		else if (canRepair1B(dom, rInfo)) {
			repair1B(dom, rInfo);
			rInfo.ev1.done=true;
			rInfo.ev2.done=true;
			rInfo.ev1.repairType="1B";
			rInfo.ev2.repairType="1B";
		}
		else if (canRepair1C(dom, rInfo)) {
			repair1C(dom, rInfo);
			rInfo.ev1.done=true;
			rInfo.ev2.done=true;
			rInfo.ev1.repairType="1C";
			rInfo.ev2.repairType="1C";
		}
		else if (canRepair1D(dom, rInfo)) {
			repair1D(dom, rInfo);
			rInfo.ev1.done=true;
			rInfo.ev2.done=true;
			rInfo.ev1.repairType="1D";
			rInfo.ev2.repairType="1D";
		}
		else if (canRepair2A(dom, rInfo)) {
			repair2A(dom, rInfo);
			rInfo.ev1.done=true;
			rInfo.ev2.done=true;
		}
		else if (canRepair2B(dom, rInfo)) {
			repair2B(dom, rInfo);
			rInfo.ev1.done=true;
			rInfo.ev2.done=true;
		}

		else if (canRepairOther(dom, rInfo)) {
			// repair2B(dom, rInfo);

			// Utilities.Logging(dom.outerHtml(), "Other");
			
			if(rInfo.ev1.done==true && rInfo.ev2.done==true){
				Utilities.Logging("Race can be repaired as \"Other\"", "Other");
			}

		}

		if (Config.repairTypes.contains(rInfo.repairType))
			return true;
		else
			return false;

		// return dom;
	}

	public static boolean canRepair1A(Document dom, RaceInfo rInfo) {
		if (rInfo.getRaceRepairType().equals("1A"))
			return true;
		return false;
	}

	public static boolean canRepair1B(Document dom, RaceInfo rInfo) {
		if (rInfo.getRaceRepairType().equals("1B"))
			return true;
		return false;
	}

	public static boolean canRepair1C(Document dom, RaceInfo rInfo) {
		if (rInfo.getRaceRepairType().equals("1C"))
			return true;
		return false;
	}

	public static boolean canRepair1D(Document dom, RaceInfo rInfo) {
		if (rInfo.getRaceRepairType().equals("1D"))
			return true;
		return false;
	}

	public static boolean canRepair2A(Document dom, RaceInfo rInfo) {
		if (rInfo.getRaceRepairType().equals("2A"))
			return true;
		return false;
	}

	public static boolean canRepair2B(Document dom, RaceInfo rInfo) {
		if (rInfo.getRaceRepairType().equals("2B"))
			return true;
		return false;
	}

	public static boolean canRepairOther(Document dom, RaceInfo rInfo) {
		if (rInfo.getRaceRepairType().equals("Other"))
			return true;
		return false;
	}

}
