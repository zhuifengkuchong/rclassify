<script class = "Lu_Inst">
	var @EventString@_try_attach_count=0;
	function @EventString@_try_attach() {		
		if(!isRacingEventFinishedWaiting("@EventString@") && @EventString@_try_attach_count<100){
			@EventString@_try_attach_count++;
			console.log("Retry count: " + @EventString@_try_attach_count + "  :  @EventString@_try_attach");				
			setTimeout_Lu(@EventString@_try_attach, 200);
			return;
		}   	
		else{
			console.log("@EventString@_try_attach sucessfully executed");
			document.getElementById("@id@").src="@src@";
			RacingEventDone("@EventString@");
		}
	}
	@EventString@_try_attach();
</script>