package RCrepair;

import java.awt.List;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;
import java.util.ArrayList;

import org.jsoup.helper.DataUtil;
import org.jsoup.helper.Validate;

public class Utilities {
	public static String readFromFile(String path) throws IOException {
		File input = new File(path);
		ByteBuffer byteData = DataUtil.readFileToByteBuffer(input);

		String charsetName = "UTF-8";
		Validate.notEmpty(
				charsetName,
				"Must set charset arg to character set of file to parse. Set to null to attempt to detect from HTML");
		String docData = Charset.forName(charsetName).decode(byteData)
				.toString();

		return docData;
	}

	public static String[] splitByLines(String s) {
		String[] lines = s.split(System.getProperty("line.separator"));
		return lines;
	}

//	static String[] splitByLine(String s) {
//		s = s.substring(s.indexOf('\n') + 1);
//		s.substring(0, s.lastIndexOf('\n'));
//		String[] lines = s.split(System.getProperty("line.separator"));
//		// System.out.println(s);
//		return lines;
//	}
	
	static String[] splitString(String s, String sep) {
		return s.split(sep);
	}
	
	
	public static void write (String filename, Object[] objects) throws IOException{
		  BufferedWriter outputWriter = null;
		  outputWriter = new BufferedWriter(new FileWriter(filename));
		  for (int i = 0; i < objects.length; i++) {
		    // Maybe:
//		    outputWriter.write(x[i]+"");
		    // Or:
		    outputWriter.write(objects[i].toString());
		    outputWriter.newLine();
		  }
		  outputWriter.flush();  
		  outputWriter.close();  
		}
	

	public static void Logging(String s, String cate) {
		if (Config.DoNotPrint.contains(cate))
			return;

		System.out.println(s);
	}
	
	
	public static void copyFileUsingFileChannels(File source, File dest)
			throws IOException {
		FileChannel inputChannel = null;
		FileChannel outputChannel = null;
		try {
			inputChannel = new FileInputStream(source).getChannel();
			outputChannel = new FileOutputStream(dest).getChannel();
			outputChannel.transferFrom(inputChannel, 0, inputChannel.size());
		} finally {
			inputChannel.close();
			outputChannel.close();
		}
	}
	

	public static ArrayList<String> linuxExec(String cmd) {
		ArrayList<String> ret = new ArrayList<String>();
		String s;
		Process p;
		try {
			// p = Runtime.getRuntime().exec("ls -aF");
			p = Runtime.getRuntime().exec(cmd);
			BufferedReader br = new BufferedReader(new InputStreamReader(
					p.getInputStream()));
			while ((s = br.readLine()) != null){
				System.out.println("line: " + s);
				ret.add(s);
			}
			p.waitFor();
			System.out.println("exit: " + p.exitValue());
			p.destroy();
		} catch (Exception e) {
		}
		
		return ret;
	}

}
