package RCrepair;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.SortedSet;
import java.util.TreeSet;

import org.jsoup.Jsoup;
import org.jsoup.instrumentation.LuInst;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Node;

import RCrepair.Inst.InstTransformer;

public class DOMTransformer {
	private static DOMTransformer instance;

	static File parentPath;

	// static ArrayList<String>

	// private static ArrayList<Node> all_Nodes;

	public DOMTransformer() {
		System.out.println("LOG -: New DOMTransformer instance created\n");
	}

	public static DOMTransformer getInstance() {
		if (instance == null)
			instance = new DOMTransformer();
		return instance;
	}

	public static Document getDomFromFile(String folder, String path)
			throws IOException {
		File input = new File(folder + "/" + path);// the entry html file
		// System.out.println("The html file path: " + input.getAbsolutePath());
		// File parentPath = input.getAbsoluteFile().getParentFile();
		// System.out.println("The folder path: " +
		// parentPath.getAbsolutePath());

		

		Document doc = Jsoup.parse(input, "UTF-8", "http://example.com/");
		return doc;
	}

	public static void saveDOMToFile(Document dom, String path) {

	}

	public static Document addUniqueId(Document dom) {
		return null;
	}

	public static String getRaceFile(String path) {
		return null;
	}

	public static void insertNodeBeforeHead(Document dom, String node) {
		ArrayList<Node> all_Nodes = DOMTransformer.getAllElement(dom, dom);
		all_Nodes.get(0).before(node);
	}

	public static void insertCodeInhead(Document dom, String node) {
		try {

			String codeInHead = LuInst
					.getInstance()
					.readFile(
							"/home/jack/Dropbox/VT/JavaScript/codes/jsoup-master/src/main/java/RCrepair/InsertCodeInHead.js");
			Utilities
					.copyFileUsingFileChannels(
							new File(
									"/home/jack/Dropbox/VT/JavaScript/codes/jsoup-master/src/main/java/RCrepair/RCcodeInHead.js"),
							new File(parentPath.getAbsolutePath()
									+ "/RCcodeInHead.js"));

			// System.out.println(codeInHead);
			ArrayList<Node> all_Nodes = DOMTransformer.getAllElement(dom, dom);
			all_Nodes.get(0).before(codeInHead);

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static void insertNodeAfterBody(Document dom, String node) {

	}

	public static void insertNodeAfterNode(Document dom, String id, String node) {

	}

	public static ArrayList<Node> getAllElement(Document root, Document dom) {
		ArrayList<Node> all_Nodes = new ArrayList<Node>();
		Get_All_Element(dom, dom, all_Nodes);
		return all_Nodes;
	}

	static void Get_All_Element(Document root, Node node,
			ArrayList<Node> all_Nodes) {
		if (node instanceof org.jsoup.nodes.Element
				&& !node.outerHtml().equals(" ") && node != root) {
			// && !node.attr("class").equals("Lu_Inst") && node != root) {
			all_Nodes.add(node);
		}

		if (node.childNodes().size() > 0) {
			for (int i = 0; i < node.childNodes().size(); ++i) {
				Get_All_Element(root, node.childNode(i), all_Nodes);
			}
		}
	}

	static File[] getRaces(String path) {
		File input = new File(path);// the entry html file
		// System.out.println("The html file path: " + input.getAbsolutePath());

		File parentPath = input.getAbsoluteFile().getParentFile();
		// System.out.println("The folder path: " +
		// parentPath.getAbsolutePath());

		File racePath = new File(parentPath.getAbsolutePath() + "/races");
		// System.out.println("The races path: " + racePath.getAbsolutePath());
		// System.out
		// .println("***************************************************************************************************");

		File[] raceFiles = racePath.listFiles();
		Arrays.sort(raceFiles);

		for (File race : raceFiles) {
			// System.out.println(race.getAbsolutePath());
		}

		return raceFiles;
	}

	static String[] splitByLine(String s) {
		s = s.substring(s.indexOf('\n') + 1);
		s.substring(0, s.lastIndexOf('\n'));
		String[] lines = s.split(System.getProperty("line.separator"));
		// System.out.println(s);
		return lines;
	}

	static RaceInfo parseRaceString(String s, String name) {
		String[] ssStrings = DOMTransformer.splitByLine(s);

		RaceInfo rInfo = new RaceInfo(name);

		// System.out.println(ssStrings);
		for (String ss : ssStrings) {
			// System.out.println(ss);
			if (ss.contains(".varName=")) {
				Utilities.Logging(ss, "parseRaceString");
				String valueString = Utilities.splitString(ss, "\"")[1];
				Utilities.Logging(valueString, "parseRaceString");
				rInfo.setVarName(valueString);
			} else if (ss.contains(".varType=")) {
				Utilities.Logging(ss, "parseRaceString");
				String valueString = Utilities.splitString(ss, "\"")[1];
				Utilities.Logging(valueString, "parseRaceString");
				rInfo.setVarType(valueString);
			} else if (ss.contains(".repairType =")) {
				Utilities.Logging(ss, "parseRaceString");
				String valueString = Utilities.splitString(ss, "\"")[1];
				Utilities.Logging(valueString, "parseRaceString");
				rInfo.setRaceRepairType(valueString);
			} else if (ss.contains(".event1.id =")) {
				Utilities.Logging(ss, "parseRaceString");
				String valueString = Utilities.splitString(ss, "\"")[1];
				Utilities.Logging(valueString, "parseRaceString");
				rInfo.setEvent1Id(valueString);
			} else if (ss.contains(".event1.type")) {
				Utilities.Logging(ss, "parseRaceString");
				String valueString = Utilities.splitString(ss, "\"")[1];
				Utilities.Logging(valueString, "parseRaceString");
				rInfo.setEvent1Type(valueString);
			} else if (ss.contains(".event1.loc")) {
				Utilities.Logging(ss, "parseRaceString");
				String valueString = Utilities.splitString(ss, "\"")[1];
				Utilities.Logging(valueString, "parseRaceString");
				rInfo.setEvent1Loc(valueString);
			} else if (ss.contains(".event1.eventType =")) {
				Utilities.Logging(ss, "parseRaceString");
				String valueString = Utilities.splitString(ss, "\"")[1];
				Utilities.Logging(valueString, "parseRaceString");
				rInfo.setEvent1EventType(valueString);
			} else if (ss.contains(".event1.isRead =")) {
				Utilities.Logging(ss, "parseRaceString");
				String valueString = Utilities.splitString(ss, "\"")[1];
				Utilities.Logging(valueString, "parseRaceString");
				rInfo.setEvent1IsRead(valueString);
			} else if (ss.contains(".event2.id =")) {
				Utilities.Logging(ss, "parseRaceString");
				String valueString = Utilities.splitString(ss, "\"")[1];
				Utilities.Logging(valueString, "parseRaceString");
				rInfo.setEvent2Id(valueString);
			} else if (ss.contains(".event2.type")) {
				Utilities.Logging(ss, "parseRaceString");
				String valueString = Utilities.splitString(ss, "\"")[1];
				Utilities.Logging(valueString, "parseRaceString");
				rInfo.setEvent2Type(valueString);
			} else if (ss.contains(".event2.loc")) {
				Utilities.Logging(ss, "parseRaceString");
				String valueString = Utilities.splitString(ss, "\"")[1];
				Utilities.Logging(valueString, "parseRaceString");
				rInfo.setEvent2Loc(valueString);
			} else if (ss.contains(".event2.eventType =")) {
				Utilities.Logging(ss, "parseRaceString");
				String valueString = Utilities.splitString(ss, "\"")[1];
				Utilities.Logging(valueString, "parseRaceString");
				rInfo.setEvent2EventType(valueString);
			} else if (ss.contains(".event2.isRead =")) {
				Utilities.Logging(ss, "parseRaceString");
				String valueString = Utilities.splitString(ss, "\"")[1];
				Utilities.Logging(valueString, "parseRaceString");
				rInfo.setEvent2IsRead(valueString);
			}
		}

		// rInfo.print();

		return rInfo;
	}

	static void Inst_addUniqueId(ArrayList<Node> all_Nodes, Document doc) {

		HashMap<String, Boolean> existingIdHashMap = new HashMap<String, Boolean>();

		for (int i = 0; i < all_Nodes.size(); ++i) {
			Node node = all_Nodes.get(i);

			if (node instanceof org.jsoup.nodes.Element) {
				if (!node.outerHtml().equals(" ")) {
					if (!node.attr("class").equals("Lu_Inst")
							&& !node.attr("id").isEmpty()) {
						// System.out.println(node.attr("id"));
						existingIdHashMap.put(node.attr("id"), true);
					}
				}
			}
			//

		}

		// ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		HashMap<String, Integer> ID_generator = new HashMap<String, Integer>();

		for (int i = 0; i < all_Nodes.size(); ++i) {
			Node node = all_Nodes.get(i);

			if (node instanceof org.jsoup.nodes.Element) {
				if (!node.outerHtml().isEmpty()) {
					if (!node.attr("class").equals("Lu_Inst")) {

						// System.out.println(node.outerHtml());

						if (node.attr("id").isEmpty()) {
							if (!ID_generator.containsKey(node.nodeName())) {
								ID_generator.put(node.nodeName(), 1);
							}

							while (true) {
								String Lu_ID = "Lu_Id_" + node.nodeName() + "_"
										+ ID_generator.get(node.nodeName());
								if (existingIdHashMap.containsKey(Lu_ID)) {
									ID_generator
											.put(node.nodeName(), ID_generator
													.get(node.nodeName()) + 1);
								} else {
									node.attr("id", Lu_ID);
									existingIdHashMap
											.put(node.attr("id"), true);
									ID_generator
											.put(node.nodeName(), ID_generator
													.get(node.nodeName()) + 1);
									break;
								}
							}

							// String Lu_ID = "Lu_Id_" + node.nodeName() + "_"
							// + ID_generator.get(node.nodeName());
							// ID_generator.put(node.nodeName(),
							// ID_generator.get(node.nodeName()) + 1);
							// node.attr("id", Lu_ID);
							// existingIdHashMap.put(node.attr("id"), true);
						}
					}
				}
			}
			// System.out.println("------------------------------------------");

		}

	}

	// **********************************main***********************************//

	public static List<String> analysisRaceType(Document dom, RaceInfo ri) {
		List<String> list = new ArrayList<String>();

		return list;
	}

	public static Document insertTimeMeasure(Document dom) {
		Document dom2 = dom.clone();

		try {
			String codeInHead = LuInst
					.getInstance()
					.readFile(
							"/home/jack/Dropbox/VT/JavaScript/codes/jsoup-master/src/main/java/RCrepair/MeasureTime.js");
			ArrayList<Node> all_Nodes = DOMTransformer
					.getAllElement(dom2, dom2);
			all_Nodes.get(0).before(codeInHead);

		} catch (Exception e) {
			// TODO: handle exception
		}

		return dom2;
	}

	public static String main(String bbString) throws IOException {
		long startTime = System.currentTimeMillis();
		System.out
				.println("\n------------------------------------------------------------------------\n"
						+ bbString);
		File input = new File("" + "/" + bbString);// the entry html file
		parentPath = input.getAbsoluteFile().getParentFile();
		Document dom = DOMTransformer.getDomFromFile("", bbString);
		// System.out.println(dom.outerHtml());
		ArrayList<Node> all_Nodes = DOMTransformer.getAllElement(dom, dom);
		DOMTransformer.Inst_addUniqueId(all_Nodes, dom);

		// System.out.println(parentPath.getAbsolutePath());

		File[] files = parentPath.listFiles();
		if (null != files) {
			for (int i = 0; i < files.length; i++) {
				if (files[i].isDirectory()) {
					continue;
				} else {
					// files[i].delete();
					// System.out.println(files[i].getAbsolutePath());
					if (files[i].getName().endsWith(".html3")) {
						files[i].delete();
					}

					if (files[i].getName().contains("remain_race")) {
						files[i].delete();
					}
				}
			}
		}

		RepairCount.reset();

		File[] raceFiles = DOMTransformer.getRaces("" + "/" + bbString);
		Arrays.sort(raceFiles);
		for (File f : raceFiles) {
			String raceTxt = Utilities.readFromFile(f.getAbsolutePath());
			// System.out.println(raceTxt);
			RaceInfo rInfo = DOMTransformer.parseRaceString(raceTxt,
					f.getName());
			// rInfo.print();
			// rInfo.raceId = f.getName();

			// System.out.println(f.getName().substring(4,
			// f.getName().length()-5));

			// remain_race.add(Integer.parseInt(f.getName().substring(4,
			// f.getName().length()-5)));

			// if(true)continue;

			Document dom2 = dom.clone();

			if (RaceRepair.repairHTML(dom2, rInfo)) {

				int raceIndex = Integer.parseInt(f.getName().substring(4,
						f.getName().length() - 5));

				RepairCount.remain_raceSet.add(raceIndex);
				String typeString = "";
				if (RepairCount.repair_types.containsKey(raceIndex)) {
					typeString = RepairCount.repair_types.get(raceIndex) + ",";
				}
				typeString += rInfo.repairType;

				RepairCount.repair_types.put(raceIndex, typeString);

				 String outputPathString = parentPath.getAbsolutePath() + "/"
				 + f.getName() + ".html3";
				 File newTextFile = new File(outputPathString);
				
				 // System.out.println("----The output: " +
				 // outputPathString);
				
				 FileWriter fw = new FileWriter(newTextFile);
				
				 fw.write(dom2.outerHtml());
				 fw.close();
				//
				// insertTimeMeasure(dom);

			}
		}
		RepairCount.print(bbString);

		System.out.println(RepairCount.print3());

		SortedSet<Integer> keys = new TreeSet<Integer>(
				RepairCount.remain_raceSet);
		Utilities.write(parentPath.getAbsolutePath() + "/remain_race",
				keys.toArray());

		// System.out.println(RepairCount.print2());

		long endTime = System.currentTimeMillis();
		long totalTime = endTime - startTime;
		// System.out.println(totalTime);

		// return RepairCount.print2()+ "	&	" + totalTime+ "ms" + "	&	"
		// +bbString.substring(35);
		return bbString.substring(35) + "	&	" + RepairCount.print2() + "	&	"
				+ totalTime + "	&	" + RepairCount.print3();
		// return RepairCount.print2()+ "	&	" + totalTime ;

	}

	public static String main2(String bbString) throws IOException {
		long startTime = System.currentTimeMillis();
		System.out
				.println("\n------------------------------------------------------------------------\n"
						+ bbString);

		Document dom = DOMTransformer.getDomFromFile("", bbString);
		// System.out.println(dom.outerHtml());
		ArrayList<Node> all_Nodes = DOMTransformer.getAllElement(dom, dom);
		DOMTransformer.Inst_addUniqueId(all_Nodes, dom);

		// System.out.println(parentPath.getAbsolutePath());
		File input = new File("" + "/" + bbString);// the entry html file
		parentPath = input.getAbsoluteFile().getParentFile();
		
		HashSet<String> exsiting = new HashSet<String> ();

		File[] files = parentPath.listFiles();
		if (null != files) {
			for (int i = 0; i < files.length; i++) {
				if (files[i].isDirectory()) {
					continue;
				} else {
					// files[i].delete();
					// System.out.println(files[i].getAbsolutePath());
					if (files[i].getName().endsWith(".html3")) {
						exsiting.add(files[i].getName());
//						files[i].delete();
					}

//					if (files[i].getName().contains("remain_race")) {
//						files[i].delete();
//					}
				}
			}
		}
		

//		String outputPathString = parentPath.getAbsolutePath()
//				+ "/original.html3";
//		File newTextFile = new File(outputPathString);
//
//		FileWriter fw = new FileWriter(newTextFile);
//		fw.write(insertTimeMeasure(dom).outerHtml());
//		fw.close();

		File[] raceFiles = DOMTransformer.getRaces("" + "/" + bbString);
		Arrays.sort(raceFiles);
		for (File f : raceFiles) {

			String outputPathString =  f.getName() + ".html3";
//			if(!exsiting.contains(outputPathString)) continue;
			
//			int raceIndex = Integer.parseInt(f.getName().substring(4,
//					f.getName().length() - 5));
//			if (!RepairCount.remain_raceSet.contains(raceIndex))
//				continue;

			String raceTxt = Utilities.readFromFile(f.getAbsolutePath());
			// System.out.println(raceTxt);
			RaceInfo rInfo = DOMTransformer.parseRaceString(raceTxt,
					f.getName());
			// rInfo.print();
			// rInfo.raceId = f.getName();

			// System.out.println(f.getName().substring(4,
			// f.getName().length()-5));

			// remain_race.add(Integer.parseInt(f.getName().substring(4,
			// f.getName().length()-5)));

			// if(true)continue;

			Document dom2 = dom.clone();

			if (RaceRepair.repairHTML(dom2, rInfo)) {

//				outputPathString = parentPath.getAbsolutePath() + "/"
//						+ f.getName() + ".html3";
				File newTextFile = new File(outputPathString);

				// System.out.println("----The output: " +
				// outputPathString);

				FileWriter fw = new FileWriter(newTextFile);
				fw.write(insertTimeMeasure(dom2).outerHtml());
				fw.close();

			}
		}
		RepairCount.print(bbString);

		System.out.println(RepairCount.print3());

		SortedSet<Integer> keys = new TreeSet<Integer>(
				RepairCount.remain_raceSet);
		Utilities.write(parentPath.getAbsolutePath() + "/remain_race",
				keys.toArray());

		// System.out.println(RepairCount.print2());

		long endTime = System.currentTimeMillis();
		long totalTime = endTime - startTime;
		// System.out.println(totalTime);

		// return RepairCount.print2()+ "	&	" + totalTime+ "ms" + "	&	"
		// +bbString.substring(35);
		return bbString.substring(35) + "	&	" + RepairCount.print2() + "	&	"
				+ totalTime + "	&	" + RepairCount.print3();
		// return RepairCount.print2()+ "	&	" + totalTime ;

	}

}
