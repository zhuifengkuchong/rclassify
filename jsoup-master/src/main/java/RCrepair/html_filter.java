package RCrepair;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

public class html_filter {

	public static ArrayList<String> main(String args) {
		// TODO Auto-generated method stub
		
		ArrayList<String> retArrayList = new ArrayList<String>();
		try {
			String raceTxt = Utilities.readFromFile(args);
			String[] ssStrings = Utilities.splitByLines(raceTxt);
			
			Arrays.sort( ssStrings);
			
			for (int i = 0; i < ssStrings.length; i++) {
				String ss = ssStrings[i];
				
				if(ss.contains("DiffJson.html")) continue;
				if(ss.contains("init0")) continue;
				if(ss.contains("ER_actionlog")) continue;
				if(ss.contains("index_utag_VS_DOMContentLoaded")) continue;
				if(ss.contains("500-2")) continue;
				if(ss.contains("y0xPLMnMz0vMAfGjzO")) continue;
				if(ss.contains("newyorklife")) continue;
				if(ss.contains("honeywell")) continue;
				if(ss.contains("404")) continue;
				
				
				retArrayList.add(ss);
			}
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		for (int i = 0; i < retArrayList.size(); i++) {
			System.out.println("\""+retArrayList.get(i)+"\",");
		}
		return retArrayList;
	}

}
