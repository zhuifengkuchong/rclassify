package RCrepair;

import java.util.HashMap;
import java.util.HashSet;
import java.util.SortedSet;
import java.util.TreeSet;

public class RepairCount {

	public static void reset() {
		count = 0;
		Bogus = 0;
		filter = 0;
		R1A = 0;
		R1B = 0;
		R1C = 0;
		R2ST = 0;
		R2HB = 0;
		R2DY = 0;
		remain_raceSet.clear();
		repair_types.clear();
	}

	public static String print3() {
		String ret = "";
		
		SortedSet<Integer> keys = new TreeSet<Integer>(repair_types.keySet());
		for (Integer key : keys) { 
			ret = ret + key + ":" + repair_types.get(key) + "	";
		   // do something
		}

		return ret;
	}

	public static HashSet<Integer> remain_raceSet = new HashSet<Integer>();
	public static HashMap<Integer, String> repair_types = new HashMap<Integer, String>();

	public static int count = 0;
	public static int Bogus = 0;
	public static int filter = 0;
	public static int R1A = 0;
	public static int R1B = 0;
	public static int R1C = 0;
	public static int R2ST = 0;
	public static int R2HB = 0;
	public static int R2DY = 0;

	public static void print(String name) {
		System.out.println();
		System.out.println("Name    : " + name);
		System.out.println("Count   : " + count);
		System.out.println("Bogus   : " + Bogus);
		System.out.println("filter  : " + filter);
		System.out.println("R1A     : " + R1A);
		System.out.println("R1B     : " + R1B);
		System.out.println("R1C     : " + R1C);
		System.out.println("R2ST     : " + R2ST);
		System.out.println("R2HB     : " + R2HB);
		System.out.println("R2DY     : " + R2DY);

		System.out.println();

		SortedSet<Integer> keys = new TreeSet<Integer>(repair_types.keySet());
		System.out.println("--" + keys.toString());

	}

	public static String print2() {
		int unhandleed = count - Bogus - filter - R1A - R1B - R1C - R2ST - R2HB
				- R2DY;
		String retString = count + "	&	" + Bogus + "	&	" + filter + "	&	"
				+ unhandleed + "	&	" + R1A + "	&	" + R1B + "	&	" + R1C + "	&	"
				+ R2ST + "	&	" + R2HB + "	&	" + R2DY;
		return retString;
	}

}
