package RCrepair;

import static org.junit.Assert.*;

import java.io.IOException;
import java.util.ArrayList;

import javax.script.ScriptException;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Node;
import org.junit.Test;

public class AllBenchmarks {

	
	@Test
	public void MeasureTime() throws IOException{
		int count = 0;
		int countJquery = 0;
		for (String bbString : Config.websites) {
//			for (String bbString : Config.benchmarks_websites) {
//			EvaluateTime.main(bbString);
				count++;
			if(jsCount.main(bbString)){
				countJquery++;
			}

		}
		System.out.println(countJquery);
		System.out.println(count);
	}
	
	@Test
	public void getJquery() throws IOException {

		for (String bbString : Config.websites) {
			Document dom = DOMTransformer.getDomFromFile("", bbString);
			// System.out.println(dom.outerHtml());
			ArrayList<Node> all_Nodes = DOMTransformer.getAllElement(dom, dom);
			System.out.println(bbString);
			for(Node node : all_Nodes){
				if(node.equals(dom)) continue;
				if(node.equals(dom.html())) continue;
				if(node.equals(dom.head())) continue;
				if(node.equals(dom.body())) continue;
				if(!node.nodeName().endsWith("script")) continue;
				if(!node.attr("src").contains("jquery")) continue;
				if(!node.attr("src").contains("1.")) continue;
				if(!node.attr("src").contains(".js")) continue;
				if(node.attr("src").contains("migrate")) continue;

//				System.out.println(node.nodeName());
				System.out.println(node);
			}
			
			
		}


	}
	
	
	@Test
	public void allBenchmarks() throws IOException {

		ArrayList<String> results = new ArrayList<String>();
		for (String bbString : Config.benchmarks) {
			System.out.println(Config.rootpath + bbString);
			results.add(DOMTransformer.main(Config.rootpath + bbString));
			
//			DOMTransformer.main(Config.rootpath + bbString);
//			results.add(DOMTransformer.main2(Config.rootpath + bbString));
			
			
		}
		
		for (int i = 1; i < results.size(); i++) {
			System.out.println(results.get(i));
		}

	}
	
	@Test
	public void performanceTest() throws IOException {
		ArrayList<String> results = new ArrayList<String>();
		for (String bbString : Config.benchmarks) {

		}
		
		for (int i = 1; i < results.size(); i++) {
			System.out.println(results.get(i));
		}
	}

	@Test
	public void allWebsites() throws Exception {
		ArrayList<String> results = new ArrayList<String>();
		for (String bbString : Config.websites) {
//			results.add(DOMTransformer.main(bbString));
			results.add(DOMTransformer.main2(bbString));
//			jsoup.main(bbString);
			
		}
		for (int i = 1; i < results.size(); i++) {
			System.out.println(results.get(i));
		}

	}
	
	

	
	@Test
	public void RClassify() throws IOException, ScriptException {


//		ArrayList<String> results = new ArrayList<String>();
//		for (String bbString : Config.websites) {
////			results.add(DOMTransformer.main(bbString));
//			jsoup.main(bbString);
//			
//		}
//		for (int i = 1; i < results.size(); i++) {
//			System.out.println(results.get(i));
//		}

	}
	
	
	@Test
	public void filter_html() throws IOException {


//		html_filter.main("/home/jack/Dropbox/VT/JavaScript/codes/RCrepair/benchmarks/htms");

	}

	@Test
	public void testJSRunner() throws IOException {
		// String repairs = Utilities
		// .readFromFile("/home/jack/Dropbox/VT/JavaScript/codes/RCrepair/benchmarks/kaist_ex1/repairs/race1a.txt");
		// DOMTransformer.parseRaceString(repairs);
	}

}
