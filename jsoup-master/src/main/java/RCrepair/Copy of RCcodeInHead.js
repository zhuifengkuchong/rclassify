if (!myVars) {
	var myVars = {};
}
if (!myVars.notExecutedEvent) {
	myVars.notExecutedEvent = {};
}
myVars.postponedEvent = [];
if (!myVars.toWaitLists) {
	myVars.toWaitLists = {};
}
for (var raceInfo in myVars.races) {
	var event1String = "" + myVars.races[raceInfo].event1.id + "__"
			+ myVars.races[raceInfo].event1.type;
	var event2String = "" + myVars.races[raceInfo].event2.id + "__"
			+ myVars.races[raceInfo].event2.type;

	myVars.notExecutedEvent[event1String] = true;
	myVars.notExecutedEvent[event2String] = true;

	// //for checking event handler races.
	console.log(raceInfo + ": " + event1String + "   --->>>   "
			+ event2String)


	// / for checking parsing, src, async script after window.onload
//	if (curr_race.event1.eventString == "Lu_window__onload") {
//
//	}

//	if (curr_race.event1.eventString == "Lu_DOM__onDOMContentLoaded") {
//
//	}
	if (!myVars.toWaitLists[event2String]) {
		myVars.toWaitLists[event2String] = [];
	}
	myVars.toWaitLists[event2String].push(event1String);
}







function isRacingEventFinishedWaiting(EventString) {
	var finishedWaiting = true;
	if (typeof myVars.toWaitLists[EventString] != "undefined") {
		for (toWait in myVars.toWaitLists[EventString]) {
			// console.log(myVars.toWaitLists[EventString][toWait]+":
			// "+myVars.notExecutedEvent[myVars.toWaitLists[EventString][toWait]]);
			if (myVars.notExecutedEvent[myVars.toWaitLists[EventString][toWait]] == true) {
				// console.log(EventString+ " waiting not finished");
				return false;
			}
		}
	}
	return finishedWaiting;
}
var RacingEventDoneSequence = [];
function RacingEventDone(EventString) {
	myVars.notExecutedEvent[EventString] = false;
	RacingEventDoneSequence.push(EventString);
	consoleLog_Lu(EventString + " is set to false");
	if (isRacingEventFinishedWaiting(EventString) == false) {
		console
				.error("Replay fail to enforce the order of the race, the Event is: "
						+ EventString);
		// Lu_errors
		// .push("Replay fail to enforce the order of the race, the Event is: "
		// + EventString);
	}
}