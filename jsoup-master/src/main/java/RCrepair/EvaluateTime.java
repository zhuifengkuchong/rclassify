package RCrepair;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Node;

class Pair {
	public Pair(double a, double b) {
		this.first = a;
		this.second = b;
	}

	public String name = "";
	public double first = 0;
	public double second = 0;
}

public class EvaluateTime {

	public static void main(String path) {

		Pair original = null;
		ArrayList<Pair> races = new ArrayList<Pair>();

		File input = new File("" + "/" + path);// the entry html file
		File parentPath = input.getAbsoluteFile().getParentFile();

		File[] files = parentPath.listFiles();
		if (null != files) {
			for (int i = 0; i < files.length; i++) {
				if (files[i].isDirectory()) {
					if (!files[i].getName().contains("Time_"))
						continue;

//					System.out.println(files[i]);
					Pair pair = countTime(files[i]);

					if (files[i].getName().contains("Time_original")) {
						pair.name = "Original";
						original = pair;
					} else {
						pair.name = files[i].getAbsolutePath();
						races.add(pair);
					}

					// System.out.println(pair.first + "   ,   "+ pair.second);

				} else {
					continue;
				}
			}
		}
		if (original != null) {
			for (Pair pair : races) {
				System.out.println(pair.name+ "    "+ pair.first / original.first * 100 + "%"+ "    "+ pair.second / original.second * 100 + "%");
//				System.out.println(pair.first / original.first * 100 + "%");
//				System.out.println(pair.second / original.second * 100 + "%");
			}
		}
	}

	static Pair countTime(File path) {
		Pair pair = new Pair(0, 0);

		double total_A = 0;
		double total_B = 0;
		int count = 0;

		File[] files = path.listFiles();
		if (null != files) {
			for (int i = 0; i < files.length; i++) {
				if (files[i].isDirectory()) {
					continue;
				} else {
					// System.out.println(files[i]);
					try {
						String content = Utilities.readFromFile(files[i]
								.getAbsolutePath());
						String[] ss = Utilities.splitByLines(content);

						int a = Integer.parseInt(ss[0]);
						int b = Integer.parseInt(ss[1]);

						total_A += a;
						total_B += b;
						count++;

						// System.out.println(a);
						// System.out.println(b);
						//

						// System.out.println(content);
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
		}

		if (count != 0) {
			pair.first = total_A / count;
			pair.second = total_B / count;
		}

		return pair;
	}

}
