package RCrepair;

import java.awt.font.TransformAttribute;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;

import javax.swing.text.rtf.RTFEditorKit;

import org.jsoup.instrumentation.LuInst;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Node;

public class DomMeta {
	Document dom;

	public Document getDom() {
		return dom;
	}

	public void setDom(Document dom) {
		this.dom = dom;
	}

	public HashMap<String, Integer> id_loc = new HashMap<String, Integer>();
	public HashMap<String, String> src_id = new HashMap<String, String>();
	public HashMap<String, Node> id_node = new HashMap<String, Node>();
	public HashMap<String, Node> src_node = new HashMap<String, Node>();
	public HashMap<Integer, Node> loc_node = new HashMap<Integer, Node>();
	public HashMap<Integer, Node> loc_script = new HashMap<Integer, Node>();
	public ArrayList<Node> all_Nodes;

	public boolean Transform(RaceInfo ri) {

		ri.setVarType(VarTypeByName(ri.getVarName()));

		if (isBogusRace(ri))
			return false;

		if (isFilteredRace(ri))
			return false;

		if (!transformEvent(ri.ev1)) {
			System.out.println("Ev1 wrong");
			System.out.println("Can't handle now");
			return false;
		}
		if (!transformEvent(ri.ev2)) {
			System.out.println("Ev2 wrong");
			System.out.println("Can't handle now");
			return false;
		}

		return true;

	}

	public boolean isBogusRace(RaceInfo ri) {
		boolean bogus = false;

		if (ri.ev1.eType.startsWith("Parse") && ri.ev2.isStatic == true) {
			if (Integer.parseInt(ri.ev1.loc) < Integer.parseInt(ri.ev2.loc))
				bogus = true;

		}
		if (ri.ev2.eType.startsWith("Parse") && ri.ev1.isStatic == true) {
			if (Integer.parseInt(ri.ev2.loc) < Integer.parseInt(ri.ev1.loc))
				bogus = true;

		}

		if (ri.ev1.id.equals("Lu_window") && ri.ev1.type.equals("onload")) {
			if (ri.ev2.eType.startsWith("Parse"))
				bogus = true;
			if (ri.ev2.eType.equals("Iframe"))
				bogus = true;
			if (ri.ev2.eType.equals("AsynScript"))
				bogus = true;
			if (ri.ev2.eType.equals("onActive"))
				bogus = true;
		}

		if (ri.ev2.id.equals("Lu_window") && ri.ev2.type.equals("onload")) {
			if (ri.ev1.eType.startsWith("Parse"))
				bogus = true;
			if (ri.ev1.eType.equals("Iframe"))
				bogus = true;
			if (ri.ev1.eType.equals("AsynScript"))
				bogus = true;
			if (ri.ev1.eType.equals("onActive"))
				bogus = true;
		}

		if (bogus == true) {
			System.out.println("Bogus race, Shouldn't repair");
			ri.repairType = "None";
			RepairCount.Bogus++;
			return true;
		} else {
			return false;
		}
	}

	public boolean isFilteredRace(RaceInfo ri) {

		if (ri.ev2.eType.startsWith("Parse") && ri.ev2.isStatic == true) {
			// ri.repairType = "Delay parsing attemped, Shouldn't repair";
			System.out.println("Delay parsing attemped, Shouldn't repair");
			ri.repairType = "None";
			RepairCount.filter++;
			return true;
		}

		if (ri.ev1.eType.equals("onPassive")) {
			// if(ri.ev2.eType.equals(anObject))
			// ri.repairType = "Early onPassive attemped, Shouldn't repair";
			System.out.println("Early onPassive attemped, Shouldn't repair");
			ri.repairType = "None";
			RepairCount.filter++;
			return true;
		}

		// if (ri.ev1.eType.equals("onPassive")
		// && ri.ev2.eType.equals("onPassive")) {
		if (Config.onPassive.contains(ri.ev1.type)
				&& Config.onPassive.contains(ri.ev2.type)) {
			if (ri.ev1.id.startsWith("_script_")
					|| ri.ev2.id.startsWith("_script_")
					|| ri.ev1.id.startsWith("Src:::")
					|| ri.ev2.id.startsWith("Src:::")
					|| ri.ev1.id.startsWith("_src_")
					|| ri.ev2.id.startsWith("_src_")) {
			}
			else{
				ri.repairType = "Both onPassive, Shouldn't repair";
				System.out.println("Both onPassive, Shouldn't repair");
				ri.repairType = "None";
				RepairCount.filter++;
				return true;
			}
		}

		String EventString1 = ri.ev1.id + "__" + ri.ev1.type;
		String EventString2 = ri.ev2.id + "__" + ri.ev2.type;
		if (ri.ev2.eType.equals("onPassive") && ri.varName.equals(EventString2)) {
			// Race var : b1__onclick
			// ri.repairType =
			// "Late attaching onPassive event handler, not need to repair";
			System.out
					.println("Late attaching onPassive event handler, not need to repair");
			ri.repairType = "None";
			RepairCount.filter++;
			return true;
		}

		if (ri.ev1.eType.equals("onActive") && ri.varName.equals(EventString1)) {
			// Race var : b1__onclick
			// ri.repairType =
			// "Late attaching onPassive event handler, not need to repair";
			System.out.println("Not need to repair");
			ri.repairType = "None";
			RepairCount.filter++;
			return true;
		}

		if (ri.ev1.eType.equals(ri.ev2.eType)) {
			if (ri.ev1.isRead.equals("True") && ri.ev2.isRead.equals("False")) {
				System.out
						.println("Same types of events, the write should be before read");
				ri.repairType = "None";
				RepairCount.filter++;
				return true;
			}
		}

		return false;
	}

	public void getRepair(RaceInfo ri) {

		// ri.print();
		// if (ri.ev1.isRead.equals("True") && ri.ev2.isRead.equals("False")) {
		// return;
		// }

		if (isBogusRace(ri))
			return;

		if (isFilteredRace(ri))
			return;

		String EventString1 = ri.ev1.id + "__" + ri.ev1.type;
		String EventString2 = ri.ev2.id + "__" + ri.ev2.type;

		// heuristic 1: static onclick vs parse script
		if (ri.ev1.eType.equals("Parse-script")
				&& ri.ev2.eType.equals("onPassive") && ri.ev2.isStatic == true) {
			// check if 1A
			System.out.println("heuristic 1");
			Node node1 = id_node.get(ri.ev1.id);
			Node node2 = id_node.get(ri.ev2.id);

			boolean repair1A = true;
			for (Node node : all_Nodes) {
				int loc = id_loc.get(node.attr("id"));
				int ev1_loc = Integer.parseInt(ri.ev1.loc);
				int ev2_loc = Integer.parseInt(ri.ev2.loc);
				if (loc <= Math.min(ev1_loc, ev2_loc))
					continue;
				if (loc >= Math.max(ev1_loc, ev2_loc))
					break;

				if (node.nodeName().equals("script")) {
					repair1A = false;
				}

				// System.out.println(node.outerHtml());
			}
			if (repair1A == true) {
				ri.repairType = "1A";
				RepairCount.R1A++;
				return;
			} else {
				ri.repairType = "1C";
				RepairCount.R1C++;
				return;
			}

			// heuristic 2: static onload vs parse script
		} else if (ri.ev1.eType.equals("Parse-nonScript")
				&& ri.ev2.eType.equals("onPassive") && ri.ev2.isStatic == true) {

			ri.repairType = "1C";
			RepairCount.R1C++;
			return;

		}

		// else if (ri.ev1.eType.equals("Parse-script")
		else if (ri.ev1.eType.startsWith("Parse")
				&& ri.ev2.eType.equals("onActive")) {
			if (ri.ev2.type.equals("onload")) {
				if (ri.ev2.isStatic == true) {
					ri.repairType = "1B";
					RepairCount.R1B++;
				}
				if (ri.varName.equals(EventString2)) {
					ri.repairType = "1B";
					RepairCount.R1B++;
				}
			}

			return;

			// heuristic 3: static onclick vs dynamic async script
		}

		// else ev1.loc < ev2.loc

		else {

			dynamicTransformEvent(ri, ri.ev1);
			dynamicTransformEvent(ri, ri.ev2);

			if (ri.ev1.done == true && ri.ev2.done == true) {
				DOMTransformer.insertCodeInhead(dom, null);
				DOMTransformer.insertNodeBeforeHead(dom, ri.recoverJS());

				ri.repairType = "Other";
			}

			if (ri.ev1.repairType.startsWith("Dynamic")
					&& ri.ev2.repairType.startsWith("Dynamic")) {
				RepairCount.R2DY++;
			} else if (ri.ev1.repairType.startsWith("Static")
					&& ri.ev2.repairType.startsWith("Static")) {
				RepairCount.R2ST++;
			} else {
				RepairCount.R2HB++;
			}

			// System.out.println(dom.outerHtml());

		}

	}

	public void dynamicTransformEvent(RaceInfo ri, RCEvent ev) {
		if (ev.eType.equals("onPassive")) {
			transformonPassiveEvent(ri, ev);
			ev.done = true;
		} else if (ev.eType.equals("Iframe")) {
			transformIframeSrcEvent(ri, ev);
			ev.done = true;
		} else if (ev.eType.equals("AsynScript")) {
			// dynamically enforce
			if (ev.isStatic == false) {
				ri.repairType = "Dynamic_asyncSrc";
			} else {
				ri.repairType = "Static_asyncSrc";
			}
			ev.done = true;
		} else if (ev.eType.equals("onActive")) {
			transformonActiveEvent(ri, ev);
			ev.done = true;
		} else if (ev.eType.equals("Parse-nonScript")) {
			// add notify
			transformonParsingEvent(ri, ev);

			ev.done = true;
		} else if (ev.eType.equals("Parse-script")) {
			// add notify
			transformonParsingEvent(ri, ev);
			ev.done = true;
		} else {

		}

	}

	public void transformonParsingEvent(RaceInfo ri, RCEvent ev) {
		String code = "";
		try {
			code = LuInst
					.getInstance()
					.readFile(
							"/home/jack/Dropbox/VT/JavaScript/codes/jsoup-master/src/main/java/RCrepair/ParsingFinish.js");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Node node = id_node.get(ev.id);
		String eventString = ev.id + "__" + ev.type;
		code = code.replace("@EventString@", eventString);
		node.after(code);
		ri.repairType = "Static_Parsing";

	}

	public void transformonActiveEvent(RaceInfo ri, RCEvent ev) {
		String evType = ev.type;

		if (ev.isStatic == true) {
			Node node = id_node.get(ev.id);
			String type = ev.type;
			if (node != null) {
				if (node.hasAttr(type)) {
					String code = "";
					try {
						code = LuInst
								.getInstance()
								.readFile(
										"/home/jack/Dropbox/VT/JavaScript/codes/jsoup-master/src/main/java/RCrepair/ReAttachOnActiveStatic.js");
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

					String fnString = node.attr(evType);

					if (fnString.endsWith("(this)")) {
						String replaceThis = "(document.getElementById(\""
								+ ev.id + "\"))";
						fnString = fnString.replace("(this)", replaceThis);
					}

					code = code.replace("@id@", ev.id);
					code = code.replace("@type@", evType);
					code = code.replace("@fn@", fnString);
					code = code
							.replace("@EventString@", ev.id + "__" + ev.type);

					node.attr(evType, "fn_" + ev.id + "__" + ev.type + "()");
					node.before(code);
					ri.repairType = "Static_Active";
				}
			} else {
				if (ev.id.equals("Lu_window")) {
					ri.repairType = "Dynamic_windowOnload";
				}
			}

		} else {
			// dynamic instrument
			ri.repairType = "Dynamic_Active";
		}

	}

	public void transformonPassiveEvent(RaceInfo ri, RCEvent ev) {
		String evType = ev.type;
		ev.type += "_attach";

		if (ev.isStatic == true) {
			String code = "";
			try {
				code = LuInst
						.getInstance()
						.readFile(
								"/home/jack/Dropbox/VT/JavaScript/codes/jsoup-master/src/main/java/RCrepair/ReAttachOnPassiveStatic.js");
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			Node node = id_node.get(ev.id);
			code = code.replace("@id@", ev.id);
			code = code.replace("@type@", evType);
			code = code.replace("@fn@", node.attr(evType));
			code = code.replace("@EventString@", ev.id + "__" + ev.type);
			node.removeAttr(evType);
			node.after(code);
			ri.repairType = "Static_Passive";
		} else {
			ri.repairType = "Dynamic_Passive";
		}
	}

	public void transformIframeSrcEvent(RaceInfo ri, RCEvent ev) {
		if (!ev.id.startsWith("_src_"))
			return;
		String id = ev.id.substring(5);
		try {
			String code = LuInst
					.getInstance()
					.readFile(
							"/home/jack/Dropbox/VT/JavaScript/codes/jsoup-master/src/main/java/RCrepair/ReAttachSrcStatic.js");

			Node node2 = id_node.get(id);
			code = code.replace("@id@", id);
			code = code.replace("@type@", ev.type);
			code = code.replace("@src@", node2.attr("src"));
			code = code.replace("@EventString@", ev.id + "__" + ev.type);
			node2.removeAttr("src");
			node2.after(code);

			ev.done = true;
			ri.repairType = "Static_Iframe";

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public boolean transformEvent(RCEvent ev) {
		try {
			if (id_loc.containsKey(ev.id)) {// update the loc
				ev.loc = "" + id_loc.get(ev.id);
			}
			if (ev.type.endsWith("__parsed")) {

				if (src_node.containsKey(Integer.parseInt(ev.loc))) {
					ev.eType = "Parse-script";
				} else {
					ev.eType = "Parse-nonScript";
				}
				ev.isStatic = true;
				return true;

			} else if (ev.type.startsWith("on")) {
				if (ev.type.equals("onclick")) {
					if (ev.id.startsWith("_script_")) {
						ev.eType = "AsynScript";

						String src = ev.id.substring(8);

						for (Node node : loc_script.values()) {
							if (node.outerHtml().contains(src)) {
								String loc = "" + id_loc.get(node.attr("id"));
								ev.loc = loc;
							}
						}
						ev.isStatic = false;
						return true;

					}
				}

				// System.out.println(ev.type);

				if (ev.id.equals("Lu_window")) {
					if (Config.onActive.contains(ev.type)) {
						ev.eType = "onActive";
					} else {
						ev.eType = "onPassive";
					}
					return true;
				}

				if (ev.id.equals("Lu_DOM")) {
					if (Config.onActive.contains(ev.type)) {
						ev.eType = "onActive";
					} else {
						ev.eType = "onPassive";
					}
					return true;
				}

				Node node = id_node.get(ev.id);
				if (node == null)
					return false;
				if (node.hasAttr(ev.type)) {
					ev.isStatic = true;
				}

				if (Config.onActive.contains(ev.type)) {
					ev.eType = "onActive";
				} else {
					ev.eType = "onPassive";
				}
				return true;

			} else if (ev.type.startsWith("Src:::")) {
				ev.eType = "Iframe";
				for (String src : src_node.keySet()) {
					if (ev.type.contains(src)) {
						Node node = src_node.get(src);
						ev.id = "_src_" + node.attr("id");
						ev.type = "onclick";
						ev.loc = "" + id_loc.get(node.attr("id"));
						ev.isStatic = true;
						return true;
					}
				}

			}
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
			return false;
		}
		return false;
	}

	public String VarTypeByName(String name) {
		if (name.startsWith("Tree[")) {
			return "DOM Node";
		}
		if (name.startsWith("DOMNode[")) {
			return "DOM Attribute";
		}
		if (name.startsWith("Array[")) {
			return "JS Array";
		}
		return "JS Variable";
	}

	// public void checkEventType()

	public DomMeta(Document dom) {
		this.dom = dom;
		all_Nodes = DOMTransformer.getAllElement(dom, dom);

		int loc = 1;

		for (int i = 0; i < all_Nodes.size(); ++i) {
			Node node = all_Nodes.get(i);

			if (node instanceof org.jsoup.nodes.Element) {
				if (node.outerHtml().equals(" "))
					continue;
				if (node.attr("class").equals("Lu_Inst"))
					continue;
				if (node == dom)
					continue;
				// if (node == dom.head())
				// continue;
				// if (node == dom.body())
				// continue;

				// System.out.println(node.outerHtml());

				String id = node.attr("id");

				id_loc.put(id, loc);

				id_node.put(id, node);
				if (node.hasAttr("src")) {
					String src = node.attr("src");
					src_id.put(src, id);
					// System.out.println(src);
					src_node.put(src, node);
				}

				if (((org.jsoup.nodes.Element) node).tagName().equals("script")) {
					loc_script.put(loc, node);
				}

				loc_node.put(loc, node);
				loc++;

			}

		}

	}

}
