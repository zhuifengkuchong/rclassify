package RCrepair;

public class RaceInfo {
	public String getVarName() {
		return varName;
	}

	public void setVarName(String varName) {
		this.varName = varName.replace("Window[26].", "");
	}

	public String getRaceRepairType() {
		return repairType;
	}

	public void setRaceRepairType(String race1RepairType) {
		this.repairType = race1RepairType;
	}

	public String getEvent1Id() {
		return this.ev1.id;
	}

	public void setEvent1Id(String event1Id) {
		this.ev1.id = event1Id;
	}

	public String getEvent1Type() {
		return this.ev1.type;
	}

	public void setEvent1Type(String event1Type) {
		this.ev1.type = event1Type;
	}

	public String getEvent1Loc() {
		return this.ev1.loc;
	}

	public void setEvent1Loc(String event1Loc) {
		this.ev1.loc = event1Loc;
	}

	public String getEvent1EventType() {
		return this.ev1.eType;
	}

	public void setEvent1EventType(String event1EventType) {
		this.ev1.eType = event1EventType;
	}

	public String getEvent2Id() {
		return this.ev2.id;
	}

	public void setEvent2Id(String event2Id) {
		this.ev2.id = event2Id;
	}

	public String getEvent2Type() {
		return this.ev2.type;
	}

	public void setEvent2Type(String event2Type) {
		this.ev2.type = event2Type;
	}

	public String getEvent2Loc() {
		return this.ev2.loc;
	}

	public void setEvent2Loc(String event2Loc) {
		this.ev2.loc = event2Loc;
	}

	public String getEvent2EventType() {
		return this.ev2.eType;
	}

	public void setEvent2EventType(String event2EventType) {
		this.ev2.eType = event2EventType;
	}

	// public int getRaceId() {
	// return RaceId;
	// }
	//
	// public void setRaceId(int raceId) {
	// RaceId = raceId;
	// }

	public String getVarType() {
		return varType;
	}

	public void setVarType(String varType) {
		this.varType = varType;
	}

	public String getRepairType() {
		return repairType;
	}

	public void setRepairType(String repairType) {
		this.repairType = repairType;
	}

	public String isEvent1IsRead() {
		return this.ev1.isRead;
	}

	public void setEvent1IsRead(String event1IsRead) {
		this.ev1.isRead = event1IsRead;
	}

	public String isEvent2IsRead() {
		return this.ev2.isRead;
	}

	public void setEvent2IsRead(String event2IsRead) {
		this.ev2.isRead = event2IsRead;
	}

	private String raceId = "";
	// public int RaceId = -1;
	public String varName = "";
	public String varType = "";
	public String repairType = "";

	public RCEvent ev1 = new RCEvent();
	public RCEvent ev2 = new RCEvent();

	public RaceInfo(String name) {
		this.raceId = name.replace(".txt", "");

	}

	// public RaceInfo(int id) {
	// this.RaceId = id;
	// }

	public void print() {
		
		Utilities.Logging("\n>>" + this.raceId
				+ "", "RaceInfo_print");

//		if(true) return;
		Utilities
				.Logging("Race var        : " + this.varName, "RaceInfo_print");
		Utilities
				.Logging("Race varType    : " + this.varType, "RaceInfo_print");
		Utilities.Logging("Race fix        : " + this.repairType+"\n",
				"RaceInfo_print");

		Utilities.Logging("Event1 Id       : " + this.ev1.id, "RaceInfo_print");
		Utilities.Logging("Event1 Type     : " + this.ev1.type,
				"RaceInfo_print");
		Utilities
				.Logging("Event1 LOC      : " + this.ev1.loc, "RaceInfo_print");
		Utilities.Logging("Event1 Etype    : " + this.ev1.eType,
				"RaceInfo_print");
		Utilities.Logging("Event1 isRead   : " + this.ev1.isRead,
				"RaceInfo_print");
		Utilities.Logging("Event1 isStatic : " + this.ev1.isStatic,
				"RaceInfo_print");
		Utilities.Logging("", "RaceInfo_print");

		Utilities.Logging("Event2 Id       : " + this.ev2.id, "RaceInfo_print");
		Utilities.Logging("Event2 Type     : " + this.ev2.type,
				"RaceInfo_print");
		Utilities
				.Logging("Event2 LOC      : " + this.ev2.loc, "RaceInfo_print");
		Utilities.Logging("Event2 Etype    : " + this.ev2.eType,
				"RaceInfo_print");
		Utilities.Logging("Event2 isRead   : " + this.ev2.isRead,
				"RaceInfo_print");
		Utilities.Logging("Event2 isStatic : " + this.ev2.isStatic,
				"RaceInfo_print");

		Utilities.Logging("", "RaceInfo_print");
	}

	String recoverJS() {
		String s = "";
		s += "<script id = \"" + this.raceId + "\" class = \"Lu_Inst\">\n";
		s += "	if(!myVars) {var myVars = {};}\n";
		s += "	if(!myVars.races) {myVars.races= {};}\n";
		s += "	myVars.races."+this.raceId+"={};\n";
		s += "	myVars.races."+this.raceId+".varName=\"" + this.varName + "\";\n";
		s += "	myVars.races."+this.raceId+".varType=\"" + this.varType + "\";\n";
		s += "	myVars.races."+this.raceId+".repairType = \"" + this.repairType + "\";\n";
		s += "	myVars.races."+this.raceId+".event1={};\n";
		s += "	myVars.races."+this.raceId+".event2={};\n";
		s += "	myVars.races."+this.raceId+".event1.id = \"" + this.ev1.id + "\";\n";
		s += "	myVars.races."+this.raceId+".event1.type = \"" + this.ev1.type + "\";\n";
		s += "	myVars.races."+this.raceId+".event1.loc = \"" + this.ev1.loc + "\";\n";
		s += "	myVars.races."+this.raceId+".event1.isRead = \"" + this.ev1.isRead + "\";\n";
		s += "	myVars.races."+this.raceId+".event1.eventType = \"" + this.ev1.eType + "\";\n";
		s += "	myVars.races."+this.raceId+".event2.id = \"" + this.ev2.id + "\";\n";
		s += "	myVars.races."+this.raceId+".event2.type = \"" + this.ev2.type + "\";\n";
		s += "	myVars.races."+this.raceId+".event2.loc = \"" + this.ev2.loc + "\";\n";
		s += "	myVars.races."+this.raceId+".event2.isRead = \"" + this.ev2.isRead + "\";\n";
		s += "	myVars.races."+this.raceId+".event2.eventType = \"" + this.ev2.eType + "\";\n";
		s += "	myVars.races."+this.raceId+".event1.executed= false;// true to disable, false to enable\n";
		s += "	myVars.races."+this.raceId+".event2.executed= false;// true to disable, false to enable\n";
		s += "</script>\n";

		return s;
	}
}
