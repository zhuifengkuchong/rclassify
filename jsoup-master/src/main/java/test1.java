import static org.junit.Assert.*;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class test1 {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

//	@Test
//	public void test1() {
//		String html = "<html><head><title>First parse</title></head>"
//				+ "<body><p>Parsed HTML into a doc.</p></body></html>";
//		Document doc = Jsoup.parse(html);
//		System.out.println(doc.outerHtml());
//	}

	@Test
	public void test2() {
		File input = new File("../case14/pswlab.kaist.ac.kr/case1.html");
		try {
			Document doc = Jsoup.parse(input, "UTF-8", "http://example.com/");
			System.out.println("---------------------------------------------------------");
			System.out.println(doc.outerHtml());
	        try {
	            
	            File newTextFile = new File("../case14/pswlab.kaist.ac.kr/output.html");

	            FileWriter fw = new FileWriter(newTextFile);
	            fw.write(doc.outerHtml());
	            fw.close();

	        } catch (IOException iox) {
	            //do stuff with exception
	            iox.printStackTrace();
	        }
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	@Test
	public void test_addeventlistner() {
		File input = new File("../case14/pswlab.kaist.ac.kr/test_addeventlistner.html");
		try {
			Document doc = Jsoup.parse(input, "UTF-8", "http://example.com/");
			System.out.println("---------------------------------------------------------");
			System.out.println(doc.outerHtml());
	        try {
	            
	            File newTextFile = new File("../case14/pswlab.kaist.ac.kr/output.html");

	            FileWriter fw = new FileWriter(newTextFile);
	            fw.write(doc.outerHtml());
	            fw.close();

	        } catch (IOException iox) {
	            //do stuff with exception
	            iox.printStackTrace();
	        }
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	
	
	
	
}
