#!/bin/bash
set -x
Path=$1
cd $Path

numfiles=(*.html2)
numfiles=${#numfiles[@]}
  
let numRaces=$numfiles/2                  
#echo $numRaces

for i in `seq 1 $numRaces`;
do
	Race=race$i  
        #echo $Race
        
	rm -rf ../../download/download*.txt     	   
        for f in $Race*.html2
	do

 	 echo $f  
	 time firefox $f &
 	 sleep 10
	  wmctrl -a Mozilla Firefox; xdotool key Ctrl+w;  
	done

	rm -rf $Race
	mkdir $Race

	mv ../../download/download.txt $Race/download.txt
	mv ../../download/download\(1\).txt $Race/download2.txt

	cp ../../jsoup-master/DiffJson.html $Race/DiffJson.html 
	cp ../../jsoup-master/master.css $Race/master.css 
	
	echo compare!
	time firefox $Race/DiffJson.html &        
	sleep 2
	wmctrl -a Mozilla Firefox; xdotool key Ctrl+w;  
	
done    


