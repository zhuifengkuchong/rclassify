#!/bin/sh
rm -rf races
mkdir races
FILE=$1
DIR=`dirname $FILE`
NAME=`basename $FILE`

echo DIR: ${DIR}
echo NAME: ${NAME}

rm -rf ../webkit/ER_actionlog
rm -rf ../webkit/DebugInfo/token
rm -rf ../webkit/DebugInfo/id
cp ${DIR}/ER_actionlog_$1 ../webkit/ER_actionlog
cp ${DIR}/DebugInfo/token ../webkit/DebugInfo/token
cp ${DIR}/DebugInfo/id ../webkit/DebugInfo/id
bin/eventracer/webapp/raceanalyzer ../webkit/ER_actionlog $2

#lu
#git config --global alias.updateall '!func(){ git fetch --all && git reset --hard origin/master; }; func'
