/*
 *  Copyright (C) 2001 Peter Kelly (pmk@post.com)
 *  Copyright (C) 2003, 2004, 2005, 2006, 2007, 2008, 2009 Apple Inc. All Rights Reserved.
 *
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public
 *  License as published by the Free Software Foundation; either
 *  version 2 of the License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public
 *  License along with this library; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "config.h"
#include "JSEventListener.h"

#include "Event.h"
#include "Frame.h"
#include "JSEvent.h"
#include "JSEventTarget.h"
#include "JSMainThreadExecState.h"
#include "WorkerContext.h"
#include <runtime/ExceptionHelpers.h>
#include <runtime/JSLock.h>
#include <wtf/RefCountedLeakCounter.h>

#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <cstring>
#include <map>
using namespace std;
#include <QString>
#include <QVariant>
#include <QMetaType>


#include "ScriptSourceCode.h"
#include "ScriptValue.h"


using namespace JSC;

class ScriptSourceCode;

namespace WebCore {

JSEventListener::JSEventListener(JSObject* function, JSObject* wrapper, bool isAttribute, DOMWrapperWorld* isolatedWorld)
    : EventListener(JSEventListenerType)
    //, m_wrapper(*isolatedWorld->globalData(), wrapper)
    , m_wrapper(wrapper)
    , m_isAttribute(isAttribute)
    , m_isolatedWorld(isolatedWorld)
{
    if (wrapper)
        m_jsFunction.setMayBeNull(*m_isolatedWorld->globalData(), wrapper, function);
    else
        ASSERT(!function);

}

JSEventListener::~JSEventListener()
{
}

JSObject* JSEventListener::initializeJSFunction(ScriptExecutionContext*) const
{
    ASSERT_NOT_REACHED();
    return 0;
}

void JSEventListener::visitJSFunction(SlotVisitor& visitor)
{
    if (m_jsFunction)
        visitor.append(&m_jsFunction);
}
#if 0
void printValueforJSValue(JSValue val,ExecState *callFrame) {
	if(val.isEmpty()) {
		cout<<"The value is empty"<<endl;
	}else if(val.isUndefined()) {
		cout<<"Value is undefined"<<endl;
	}else if(val.isNull()) {
		cout<<"The value is null"<<endl;
	}else if(val.isUndefinedOrNull()) {
		cout<<"The value is either undefined or null"<<endl;
	}else if(val.isBoolean()) {
		bool b = val.toBoolean(callFrame);
		cout<<"This is boolean"<<b<<endl;
	}else if(val.isNumber()) {
		cout<<"This is a number"<<val.toNumber(callFrame)<< endl;
	}else if(val.isDouble()) {
		double d = val.toFloat(callFrame);
		cout<<"This is double"<<d<<endl;
	}else if  (val.isString()) {
		UString str = val.toUString(callFrame);
		cout<<"This is a string "<<str.ascii().data()<<endl;
	}else if(val.isPrimitive()){
		cout<<"This is a primitive data type"<<endl;
	}else if(val.isGetterSetter()){
		cout<<"This is a getter setter data type"<<endl;
	}else if(val.isObject()) {
		cout<<"This is object " <<endl;
	}
}
#endif

string getStringForJSValue(JSValue val,ExecState *callFrame) {

	stringstream ss;
	if(val.isEmpty()) {
		return string("\"empty\"");
	}else if(val.isUndefined()) {
		return string("\"undefined\"");
	}else if(val.isNull()) {
		return string("\"null\"");
	}else if(val.isUndefinedOrNull()) {
		return string("\"undefined_or_null\"");
	}else if(val.isBoolean()) {
		bool b = val.toBoolean(callFrame);
		return b?string("true"):string("false");	
	}else if(val.isNumber()) {
		ss<<val.toNumber(callFrame);
		return ss.str();
	}else if(val.isDouble()) {
		ss<<val.toFloat(callFrame);
		return ss.str();
	}else if  (val.isString()) {
		UString str = val.toUString(callFrame);
		string quote = "\"";
		string output = quote+string(str.ascii().data())+quote;
		return output;
	}else if(val.isPrimitive()){
		return string("\"primitive\"");
	}else if(val.isGetterSetter()){
		return string("\"getter_setter\"");
	}else if(val.isObject()) {
		return string("\"object\"");
	}else {
		return string("\"unidentified\"");
	}
}





void myIterate(JSC::JSValue value , ExecState* exec, map<JSObject*,string>* objectMap,string varName, const char *fileName ,int indentLevel = 1) {
	ofstream dumpFile;
 	string valueOrType = getStringForJSValue(value,exec);       
	string outputString;

	if(varName == "window" || varName == "document") 
		return;

	if(valueOrType == "object") {
		map<JSObject*,string>::iterator objMapItr = objectMap->find(value.getObject());
		if(objMapItr != objectMap->end()) {
			return ;
		}
		for(int i =0; i < indentLevel ; ++i) {
			outputString+="\t";
		}
		indentLevel++;
	       // cout<<"Object Name : " <<varName<<" : "<<value.getObject()<<endl;
                
		dumpFile.open(fileName,ios::app);
		outputString = varName +" :  { " ;
		dumpFile<<outputString<<"\n";
		dumpFile.close();
		
		objectMap->insert(pair<JSObject*,string>(value.getObject(),varName));

		JSC::PropertyNameArray propArray(exec);
		JSC::JSObject::getPropertyNames(value.getObject(),exec,propArray,JSC::ExcludeDontEnumProperties);
		if(propArray.size() != 0 ){
			JSC::PropertyNameArrayData::PropertyNameVector::const_iterator propItr = propArray.begin();
			while(propItr != propArray.end()) {
				Identifier ident = *propItr;
				JSValue  propValue = value.getObject()->get(exec,ident);
				myIterate(propValue,exec,objectMap,string(ident.ascii().data()),fileName,indentLevel);
				++propItr;
			}
		
		}

		dumpFile.open(fileName,ios::app);
		outputString = "},";
		dumpFile<<outputString<<"\n";
		dumpFile.close();
	
	}else {

		dumpFile.open(fileName, ios::app);
	        for(int i =0; i < indentLevel ; ++i) {
			outputString+="\t";
	        }
		string outputString = varName +" : "+getStringForJSValue(value,exec) +"  , ";
		dumpFile<<outputString<<"\n";
		dumpFile.close();		
	}

}

void dumpEntireState() {
    //call dumpHtml
    //dump JSValues
    cout << __FUNCTION__ << endl;
    return;
}

void dumpHtml(JSObject *docObj, ExecState *exec, const char *fileName) {        
	ofstream dumpFile;
	dumpFile.open(fileName);

	JSC::Identifier identAll(exec,"all");
	JSC::Identifier identIndex(exec,"0");
	JSC::Identifier identHtml(exec,"outerHTML");
	JSC::JSValue allval = docObj->get(exec,identAll);

	JSC::JSValue outerhtml = allval.getObject()->get(exec,identIndex).getObject()->get(exec,identHtml);
	if(outerhtml.isString()) {
		UString str = outerhtml.toUString(exec);
		dumpFile<<str.ascii().data();
	}
	dumpFile.close();
}

void dumpJS(JSDOMGlobalObject *globalObject, ExecState* exec) { 

	JSDOMWindow* myWindow = static_cast<JSDOMWindow*>(globalObject);
	ScriptController* controller = myWindow->impl()->frame()->script();
        ifstream utilJs;
	string line;
	utilJs.open("util.js");
	stringstream jsCodeBuffer;
	if(utilJs.is_open()) {
		while(getline(utilJs,line)) {
  			jsCodeBuffer<<line<<"\n";
		}
		utilJs.close();
	}

	QString src=QString::fromLocal8Bit(jsCodeBuffer.str().c_str());
	const ScriptSourceCode code(src);
	if(controller) {
		controller->executeScript(code);
		JSC::JSValue v = controller->executeScript(ScriptSourceCode("printAllVariable()")).jsValue(); 
	        string s  = getStringForJSValue(v,globalObject->globalExec());
		stringstream outputStream(s);
		string op;
		while(getline(outputStream,op,'#')) {
			cout<<"Output: "<<op<<endl;
		}
		cout<<"\n***************************************\n";
	}
}

void JSEventListener::handleEvent(ScriptExecutionContext* scriptExecutionContext, Event* event)
{
    ASSERT(scriptExecutionContext);
    if (!scriptExecutionContext || scriptExecutionContext->isJSExecutionForbidden())
        return;

    JSLock lock(SilenceAssertionsOnly);

    JSObject* jsFunction = this->jsFunction(scriptExecutionContext);
    if (!jsFunction)
        return;

    JSDOMGlobalObject* globalObject = toJSDOMGlobalObject(scriptExecutionContext, m_isolatedWorld.get());
    if (!globalObject)
        return;

    /**
     * To convert the event id into string
     */
    std::stringstream ss;
    ss<< event->timeStamp();
    std::string myEventId;
    ss>>myEventId;

    if (scriptExecutionContext->isDocument()) {
        JSDOMWindow* window = static_cast<JSDOMWindow*>(globalObject);
        Frame* frame = window->impl()->frame();
        if (!frame)
            return;

        // The window must still be active in its frame. See <https://bugs.webkit.org/show_bug.cgi?id=21921>.
        // FIXME: A better fix for this may be to change DOMWindow::frame() to not return a frame the detached window used to be in.
        if (frame->domWindow() != window->impl())
            return;
        // FIXME: Is this check needed for other contexts?
	ScriptController* script = frame->script();
	if (!script->canExecuteScripts(AboutToExecuteScript) || script->isPaused())
		return;
	//dumpJS(globalObject,globalObject->globalExec());
    }

    ExecState* exec = globalObject->globalExec();
    JSValue handleEventFunction = jsFunction;

    //   controller->executeScript(ScriptSourceCode(src));

    // controller->evaluate(code);

    //    ofstream beforeState;
//    const char *beforeStateFile = "beforestate.dump";
//    beforeState.open(beforeStateFile);
//    beforeState<<" { "<<"\n";
//    beforeState.close();
//
//    map<JSObject*,string> beforeObjMap;
//
//    JSC::JSGlobalObject* scope = static_cast<JSGlobalObject*>(globalObject);
//    if(scope && scope->isGlobalObject()) {
//	    JSC::SymbolTable& symbolTable  = scope->symbolTable();
//	    JSC::SymbolTable::const_iterator itr = symbolTable.begin();
//
//	    for( ; itr!= symbolTable.end(); ++itr) {
//		std::string varName(UString(itr->first).ascii().data());
//		JSC::JSValue val = scope->registerAt(itr->second.getIndex()).get();
//		myIterate(val,exec,&beforeObjMap,varName,beforeStateFile,0);
//		if(varName == "document" ){
//			dumpHtml(val.getObject(),exec,"beforehtml.dump");
//		}
//
//	    }
//    }
//
//    beforeState.open(beforeStateFile,ios::app);
//    beforeState<<"\n"<<"}"<<"\n";
//    beforeState.close();
//
//    ifstream reader (beforeStateFile);
//    string line;
//    string total="";
//    if(reader.is_open()) {
//	while(getline(reader,line)) {
//		total +=line;	
//	}
//	reader.close();
//    } 
#if 0
    cJSON *json = cJSON_Parse(total.c_str());
    if(json)
	    cout<<"The type is "<<json->type<<endl;
#endif
    CallData callData;
    CallType callType = getCallData(handleEventFunction, callData);
    // If jsFunction is not actually a function, see if it implements the EventListener interface and use that
    if (callType == CallTypeNone) {
        handleEventFunction = jsFunction->get(exec, Identifier(exec, "handleEvent"));
        callType = getCallData(handleEventFunction, callData);
    }

    if (callType != CallTypeNone) {

        RefPtr<JSEventListener> protect(this);

        MarkedArgumentBuffer args;
        args.append(toJS(exec, globalObject, event));

        Event* savedEvent = globalObject->currentEvent();
        globalObject->setCurrentEvent(event);

        JSGlobalData& globalData = globalObject->globalData();
        DynamicGlobalObjectScope globalObjectScope(globalData, globalData.dynamicGlobalObject ? globalData.dynamicGlobalObject : globalObject);

        globalData.timeoutChecker.start();
        JSValue thisValue = handleEventFunction == jsFunction ? toJS(exec, globalObject, event->currentTarget()) : jsFunction;
        JSValue retval = scriptExecutionContext->isDocument()
            ? JSMainThreadExecState::call(exec, handleEventFunction, callType, callData, thisValue, args) //,myEventId)
            : JSC::call(exec, handleEventFunction, callType, callData, thisValue, args);//,myEventId);
        globalData.timeoutChecker.stop();

        globalObject->setCurrentEvent(savedEvent);

#if ENABLE(WORKERS)
        if (scriptExecutionContext->isWorkerContext()) {
            bool terminatorCausedException = (exec->hadException() && isTerminatedExecutionException(exec->exception()));
            if (terminatorCausedException || globalData.terminator.shouldTerminate())
                static_cast<WorkerContext*>(scriptExecutionContext)->script()->forbidExecution();
        }
#endif

        if (exec->hadException()) {
            event->target()->uncaughtExceptionInEventHandler();
            reportCurrentException(exec);
        } else {
            if (!retval.isUndefinedOrNull() && event->storesResultAsString())
                event->storeResult(ustringToString(retval.toUString(exec)));
            if (m_isAttribute) {
                if (retval.isFalse())
                    event->preventDefault();
            }
        }
    }


//    ofstream afterState;
//    const char *afterStateFile = "afterstate.dump";
//    afterState.open(afterStateFile);
//    afterState<<"{"<<"\n";
//    afterState.close();
//
//    map<JSObject *,string> afterObjMap;
//	
//
//    if(scope && scope->isGlobalObject()) {
//	    JSC::SymbolTable& symbolTable  = scope->symbolTable();
//	    JSC::SymbolTable::const_iterator itr = symbolTable.begin();
//
//	    for( ; itr!= symbolTable.end(); ++itr) {
//		std::string varName( UString(itr->first).ascii().data());
//		JSC::JSValue val = scope->registerAt(itr->second.getIndex()).get();
//		string outputString = varName+" : " +getStringForJSValue(val,exec)+"\n";
//		afterState<<outputString;
//		myIterate(val,exec,&afterObjMap,varName,afterStateFile,0);
//		if(varName == "document") {
//			dumpHtml(val.getObject(),exec,"afterhtml.dump");
//		}
//	    }
//    	
//    }
//
//    afterState.open(afterStateFile,ios::app);
//    afterState<<"\n"<<"}"<<"\n";
//    afterState.close();
//

            /*
    
    JSValue myNan = JSValue(std::numeric_limits<double>::quiet_NaN());
    if(beforeMap.size() == afterMap.size()) {
	    map<string,JSValue>::iterator firstItr = beforeMap.begin();
	    map<string,JSValue>::iterator secondItr = afterMap.begin();
	    while(firstItr!= beforeMap.end()) {
		    if(firstItr->first == secondItr->first) {                          
			    JSC::JSValue val1 = firstItr->second;
			    JSC::JSValue val2 = secondItr->second;
			    //if(!JSC::JSValue::equal(exec,firstItr->second,secondItr->second)){
			    cout<<"The first var name "<<firstItr->first<< " Description is "<<val1.description()<< endl;
        		    cout<<"The second var name " <<secondItr->first<<"  Description is "<< val2.description() <<endl;
			    if(val1.isUndefinedOrNull() && val2.isUndefinedOrNull()) {
				    //do nothing
			    }
			    
			    //else if(val1.isObject() && val2.isObject()  && JSC::JSValue::equal(exec,val1,val2) && (firstItr->first.find("document") != std::string::npos || firstItr->first.find("window") != std::string::npos)) {
			    //

			    else if(val1.isObject() && val2.isObject()  && JSC::JSValue::equal(exec,val1,val2) && ( firstItr->first.find("window") != std::string::npos)) {
				    if(!compareObj(exec,val1,val2)){
				        cout<<"Not matching" <<endl;
					break;
				    }
			    }

			    else if( firstItr->first.find("document") != std::string::npos) {
				showAllProps(val1.getObject(),exec);
			    }
			    
			    //else if (val1.isNumber() && val2.isNumber() && JSValue(std::numeric_limits<double>::quite_NaN()) == val1.asNumber() && JSValue(std::numeric_limits<double>::quite_NaN()) == val2.asNumber()){
			    else if (val1.isNumber() && val2.isNumber() && myNan == val1 &&  myNan == val2){
				    //do nothing
			    
			    }else if(!JSC::JSValue::equal(exec,val1,val2)){				
				    cout<<"Not matching base case values re not equal" <<firstItr->first<<endl;
				    printValueforJSValue(firstItr->second,exec);
				    printValueforJSValue(secondItr->second,exec);
				    break;
			    }
		    }
		    ++firstItr;
		    ++secondItr;
	    }

    }else {
	    cout<<"Maps are not matching"<<endl;
    }
    */
}

bool JSEventListener::virtualisAttribute() const
{
    return m_isAttribute;
}

bool JSEventListener::operator==(const EventListener& listener)
{
    if (const JSEventListener* jsEventListener = JSEventListener::cast(&listener))
        return m_jsFunction == jsEventListener->m_jsFunction && m_isAttribute == jsEventListener->m_isAttribute;
    return false;
}

} // namespace WebCore
