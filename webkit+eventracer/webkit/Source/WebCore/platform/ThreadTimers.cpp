/*
 * Copyright (C) 2006, 2008 Apple Inc. All rights reserved.
 * Copyright (C) 2009 Google Inc. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY APPLE COMPUTER, INC. ``AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL APPLE COMPUTER, INC. OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 * OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. 
 */

#include "config.h"
#include "ThreadTimers.h"

#include "SharedTimer.h"
#include "ThreadGlobalData.h"
#include "Timer.h"
#include <wtf/ActionLogReport.h>
#include <wtf/CurrentTime.h>
#include <wtf/MainThread.h>
#include <wtf/text/CString.h>
#include <wtf/text/WTFString.h>
#include <wtf/text/StringBuilder.h>
#include "../../WebKit/qt/Api/qwebframe.h"

#include "../../WTF/wtf/replay/replayTrace.h"
#include <stdio.h>
#include <typeinfo>
#include <vector>
#include <set>
#include <iostream>
#include <fstream>
#include <sstream>
#include <string.h>
using namespace std;

namespace WebCore {

EventActionsHB::EventActionsHB() :
		m_invalidEventAction(true), m_networkResponseRecursion(0), m_uiActionRecursion(
				0), m_inTimerEventAction(false), m_numDisabledInstrumentationRequests(
				0) {
	allocateEventActionId(); // event action with id 0 is unused (as empty value for hashtables).
	m_currentEventActionId = allocateEventActionId();
	m_lastUIEventAction = m_currentEventActionId;
}

EventActionsHB::~EventActionsHB() {
}

EventActionId EventActionsHB::allocateEventActionId() {

//	int nestRoot = replayTrace::Instance()->eventWillBeNested();

//	if (nestRoot > 0) {
//		return nestRoot;
//	}

	EventActionId result = m_timerInfo.size();
	// lu : printf("allocateEventActionId:  %d\n",result);
	m_timerInfo.append(TimerEvent());
	if (result % 8192 == 8191) {
		ActionLogSave();
	}
	return result;
}

//map<int,set<int> > arcs;

void EventActionsHB::addExplicitArc(EventActionId earlier,
		EventActionId later) {
//	if(arcs.find(earlier)!=arcs.end() && arcs[earlier].find(later)!=arcs[earlier].end()){
//		return;
//	}
//	arcs[earlier].insert(later);
	if (earlier <= 0 || later <= 0) {
		CRASH()
		;
	}
	if (earlier == later) {
		return;
	}
	printf("addExplicitArc:  %d  ->  %d\n", earlier, later);
	ActionLogAddArc(earlier, later, -1);
//	replayTrace::addArc(earlier, later, true, "black");
}

void EventActionsHB::addExplicitArcParser(EventActionId earlier,
		EventActionId later) {

	if (earlier <= 0 || later <= 0 || earlier == later) {
		CRASH()
		;
	}
	printf("addExplicitArc:  %d  ->  %d\n", earlier, later);
	ActionLogAddArc(earlier, later, -1);
//	replayTrace::addArc(earlier, later, true, "blue");
}

void EventActionsHB::addTimedArc(EventActionId earlier, EventActionId later,
		double duration) {

	if (earlier <= 0 || later <= 0) {
		CRASH()
		;
	}
	printf("addTimedArc:  %d  ->  %d\n", earlier, later);
	ActionLogAddArc(earlier, later, duration * 1000);
//	replayTrace::addArc(earlier, later, true, "red");
}

void EventActionsHB::setCurrentEventAction(EventActionId newEventActionId) {

//	int nestRoot = replayTrace::Instance()->eventWillBeNested();
//
//	if (nestRoot > 0) {
//		return;
//	}

	m_currentEventActionId = newEventActionId;
	m_timerInfo[newEventActionId].m_wasEntered = true;
	m_invalidEventAction = false;
	if (m_inTimerEventAction) {
		ActionLogEnterOperation(newEventActionId, ActionLog::TIMER);
	} else if (m_uiActionRecursion > 0) {
		ActionLogEnterOperation(newEventActionId, ActionLog::USER_INTERFACE);
	} else if (m_networkResponseRecursion > 0) {
		ActionLogEnterOperation(newEventActionId, ActionLog::NETWORK);
	} else {
		ActionLogEnterOperation(newEventActionId, ActionLog::UNKNOWN);
	}
}

EventActionId EventActionsHB::splitCurrentEventActionIfNotInScope() {
	setCurrentEventAction(allocateEventActionId());
	return currentEventAction();
}

void EventActionsHB::setCurrentEventActionInvalid() {
	ActionLogExitOperation();
	m_invalidEventAction = true;
}

void EventActionsHB::checkInValidEventAction() {
	if (m_invalidEventAction) {
		fprintf(stderr, "Not in a valid event action.\n");
		fflush(stderr);
//		CRASH()
		;
	}
}

void EventActionsHB::userInterfaceModification() {
	// Note(veselin): We've decided not to insert arcs from
	// UI modification to UI event arcs.
}

EventActionId EventActionsHB::startUIAction() {
	if (m_inTimerEventAction || m_networkResponseRecursion != 0) {
		return m_currentEventActionId;
	}
//	printf("ui open %d\n", m_uiActionRecursion);
	if (m_uiActionRecursion++ == 0) {
		EventActionId newId = allocateEventActionId();
		printf("add arc: EventActionsHB::startUIAction() \n");
		addExplicitArc(m_lastUIEventAction, newId);
		setCurrentEventAction(newId);
		m_lastUIEventAction = newId;
	}
	return m_currentEventActionId;
}

void EventActionsHB::endUIAction() {
	if (m_inTimerEventAction || m_networkResponseRecursion != 0) {
		return;
	}
	--m_uiActionRecursion;
	if (m_uiActionRecursion == 0) {
		setCurrentEventActionInvalid();
	}
//	printf("ui close %d\n", m_uiActionRecursion);
}

EventActionId EventActionsHB::startNetworkResponseEventAction() {
	if (m_inTimerEventAction || m_uiActionRecursion != 0) {
		return m_currentEventActionId;
	}
//	printf("open %d\n", m_networkResponseRecursion);
	if (m_networkResponseRecursion++ == 0) {
		setCurrentEventAction(allocateEventActionId());
	}
	return m_currentEventActionId;
}

void EventActionsHB::finishNetworkReponseEventAction() {
	if (m_inTimerEventAction || m_uiActionRecursion != 0) {
		return;
	}
	--m_networkResponseRecursion;
	if (m_networkResponseRecursion == 0) {
		setCurrentEventActionInvalid();
	}
//	printf("close %d\n", m_networkResponseRecursion);
}

void EventActionsHB::setInTimerEventAction(bool inTimer) {
//	printf("in timer %d\n", static_cast<int>(inTimer));
	if (m_uiActionRecursion != 0 || m_networkResponseRecursion != 0) {
		CRASH()
		;
	}
	m_inTimerEventAction = inTimer;
}

// Fire timers for this length of time, and then quit to let the run loop process user input events.
// 100ms is about a perceptable delay in UI, so use a half of that as a threshold.
// This is to prevent UI freeze when there are too many timers or machine performance is low.
static const double maxDurationOfFiringTimers = 0.050;

// Timers are created, started and fired on the same thread, and each thread has its own ThreadTimers
// copy to keep the heap and a set of currently firing timers.

static MainThreadSharedTimer* mainThreadSharedTimer() {
	static MainThreadSharedTimer* timer = new MainThreadSharedTimer;
	return timer;
}

ThreadTimers::ThreadTimers() :
		m_sharedTimer(0), m_firingTimers(false) {
	if (isMainThread())
		setSharedTimer(mainThreadSharedTimer());
}

// A worker thread may initialize SharedTimer after some timers are created.
// Also, SharedTimer can be replaced with 0 before all timers are destroyed.
void ThreadTimers::setSharedTimer(SharedTimer* sharedTimer) {
	if (m_sharedTimer) {
		m_sharedTimer->setFiredFunction(0);
		m_sharedTimer->stop();
	}

	m_sharedTimer = sharedTimer;

	if (sharedTimer) {
		m_sharedTimer->setFiredFunction(ThreadTimers::sharedTimerFired);
		updateSharedTimer();
	}
}

void ThreadTimers::updateSharedTimer() {
	if (!m_sharedTimer)
		return;

	if (m_firingTimers || m_timerHeap.isEmpty())
		m_sharedTimer->stop();
	else
		m_sharedTimer->setFireInterval(
				max(
						m_timerHeap.first()->m_nextFireTime
								- monotonicallyIncreasingTime(), 0.0));
}

void ThreadTimers::sharedTimerFired() {
	// Redirect to non-static method.
	threadGlobalData().threadTimers().sharedTimerFiredInternal();
}

#if 1
static bool e1_visited = false;
static bool e2_visited = false;
static bool shouldDump = false;

bool notMyTurn(string timerType, int index, string type1, int index1,
		string type2, int index2) {
	//if (typeidstr2num.find())
//	int this_tid = typeid(timer);	// can be different
//	int this_eid = num_of_timer[this_id]++;	// same
//	if (this_tid == e1_tid && this_eid == e1_eid) {
//		if (!e2_visited) {
//			return true;
//		}
//	}
//	if (this_tid == e2_tid && this_eid == e2_tid) {
//		e2_visited = true;
//	}
	if (e2_visited)
		return false;	//e2 already fired

	if (timerType.compare(type1) == 0 && index == index1) {	//event=e1, should execute first
		e1_visited = true;
		return false;
	}

	if (timerType.compare(type2) == 0 && index == index2 && !e1_visited) {//event=e2, should postpone if e1 has not been fired
		return true;
	}

	if (timerType.compare(type2) == 0 && index == index2 && e1_visited) {//event=e2, should postpone if e1 has not been fired
		cout << "Should Dump here\n";

//		QWebFrame::dumpState();
		shouldDump = true;

		e2_visited = true;
		return false;
	}

	return false;
}
#endif

void ThreadTimers::sharedTimerFiredInternal() {
//	return;
	// Do a re-entrancy check.
	if (m_firingTimers)
		return;
	m_firingTimers = true;

	double fireTime = monotonicallyIncreasingTime();
	double timeToQuit = fireTime + maxDurationOfFiringTimers;

	while (!m_timerHeap.isEmpty()
			&& m_timerHeap.first()->m_nextFireTime <= fireTime) {

		cout << "m_timerHeap size: " << m_timerHeap.size() << endl;
		set<string> availableTimers;
		Vector<TimerBase*>::iterator itTimer;
		for (itTimer = m_timerHeap.begin(); itTimer != m_timerHeap.end();
				++itTimer) {
			string timerName = (string) typeid(*(*itTimer)).name();
			cout << "\n->Available timer: " << (long) (*itTimer) << "\n->type: "
					<< timerName << "\n->firetime: "
					<< (*itTimer)->m_nextFireTime << endl;
			if (availableTimers.find(timerName) != availableTimers.end()) {
				cout << "Timer with same type!   : " << timerName << endl;
			}
			availableTimers.insert(timerName);
		}

		cout << "**************" << endl;

		TimerBase* timer = m_timerHeap.first();
		timer->m_nextFireTime = 0;
		timer->heapDeleteMin();

		cout << "\n->Selected timer: " << (long) (timer) << "\n->type: "
				<< (string) typeid(*timer).name() << "\n->firetime: "
				<< timer->m_nextFireTime << endl;

		double interval = timer->repeatInterval();

		stringstream timerPty;
		stringstream dumpTimerInfo;
		timerPty << ((long) timer);
		int timerId = replayTrace::uniqueTimerId(timerPty.str());
		cout << "Timer: Before fire or not fire: " << timerId << ", "
				<< (string) typeid(*timer).name() << " ptr(" << timerId << ") "
				<< "\n";

//		replayTrace::logToFile("timerInfo", "ThreadTimers: Default\n");

		printf("\n%s\n", __FUNCTION__);
		timer->setNextFireTime(interval ? fireTime + interval : 0, interval);

#if 0
		if(notMyTurn(my_eventActionsHB, timer, e1_tid, e1_eid, e2_tid, e2_eid)) {
			//we should postpone e1
			timer->setNextFireTime(interval ? fireTime + interval : 0, interval);
		}
#endif

//		TimerBase* timer = m_timerHeap.first();
#if 1

		bool shouldFire = true;
		static map<string, int> timerCount;
		string timerType;
		int index;
		timerType = (string) typeid(*timer).name();
		if (timerCount.find(timerType) == timerCount.end()) {
			timerCount[timerType] = 0;
		}
		index = timerCount[timerType] + 1;//the index about to fire, +1 on existing index

		std::cout << "Timer type:  " << timerType << "  , Index: " << index
				<< std::endl;

		cout << "ReplayMode: " << replayController::Instance()->replayMode
				<< "  ,  mutate race: "
				<< replayController::Instance()->replayRaceId << "\n";

		if (timerType.compare("N7WebCore5TimerINS_19HTMLParserSchedulerEEE")
				== 0) {
			replayController::Instance()->parsingEventTimerId = timerId;
		}

		if (replayController::Instance()->whetherPostponeTimer(timerId)) {
			shouldFire = false;
		}


		if (timerType.compare("N7WebCore5TimerINS_11EventSenderINS_11ImageLoaderEEEEE")
				== 0) {
			cout<<"This is : "<<"N7WebCore5TimerINS_11EventSenderINS_11ImageLoaderEEEEE\n";
			shouldFire=false;
		}


//		}

//		if(true){

		if (!shouldFire) {
			interval = 0.001;
			cout << "isInteresting moment: Need to postpone timer: " << timerId
					<< "  ,type: " << timerType << "  , Index: " << index
					<< "\n";

			printf("\n%s\n", __FUNCTION__);
			timer->setNextFireTime(interval ? fireTime + interval : 1000,
					interval);
		} else {

			if (!m_eventActionsHB.isInstrumentationDisabled()) {
				EventActionId newId = m_eventActionsHB.allocateEventActionId();
				m_eventActionsHB.setCurrentEventAction(newId);
				timer->m_lastFireEventAction = newId;

				replayController::Instance()->timerEvents[m_eventActionsHB.currentEventAction()] =
						timerType;
				if (timer->m_ignoreFireIntervalForHappensBefore) {

					printf(
							"add arc: ThreadTimers::sharedTimerFiredInternal() \n");

					m_eventActionsHB.addExplicitArc(timer->m_starterEventAction,
							newId);
				} else {
					m_eventActionsHB.addTimedArc(timer->m_starterEventAction,
							newId, timer->m_nextFireInterval);
				}
				m_eventActionsHB.setInTimerEventAction(true);
			}
			ActionLogEventTriggered(timer);

			printf(
					"Fire id:   %d,  action fired: inverval:{ %f }, timer:{ %d }\n",
					m_eventActionsHB.currentEventAction(), interval, timerId);
//			if (QWebFrame::getReplayLogMode() > 0) {
			cout << "Should fire event\n";
			timerCount[timerType] = index;
			stringstream ss;
			ss << "Timer Log\n";
			ss << m_eventActionsHB.currentEventAction() << "\n";
			ss << timerType << "\n";
			ss << timerCount[timerType] << "\n";
			ss << "------------------------------\n";

//			}

//			replayTrace::writeNestedEvents(ss.str());
//
			printf("Timer: ***** Fire Start: %d*****\n", timerId);

			dumpTimerInfo.clear();

			dumpTimerInfo << "Current Event: "
					<< m_eventActionsHB.currentEventAction() << "\n";
			dumpTimerInfo << "Timer: > > > Fire Start:  " << timerId << "\n";
//			replayTrace::logToFile("timerInfo", dumpTimerInfo.str());

			stringstream ss3;
			ss3 << m_eventActionsHB.currentEventAction() << "[shape=box]";
			replayTrace::logToFile("dot", ss3.str());

			replayTrace::timerStart(timerId);

			stringstream ss2;
			ss2 << m_eventActionsHB.currentEventAction() << "\n";
			ss2 << timerType << "\n";
			ss2 << timerId;
			replayTrace::logToFile("timer", ss2.str());
			replayTrace::logToFile("timer", "[");

//			ss2.str(std::string());
//			ss2<<"========Fire Start===>>  :"<<timerType;
//			if (timerType.compare("N7WebCore5TimerINS_19HTMLParserSchedulerEEE")
//					== 0) {
//				replayTrace::logToFile("Token", ss2.str());
//			}
			timer->fired();

			ss2.str(std::string());
			ss2 << "--->Timer fire:\nPtr  :" << timerId << "\nType: "
					<< timerType << "\nEvent:"
					<< m_eventActionsHB.currentEventAction();
			replayTrace::logToFile("event", ss2.str());

//			ss2.str(std::string());
//			ss2<<"========Fire Finish==>>  :"<<timerType<<"\n\n";
//			if (timerType.compare("N7WebCore5TimerINS_19HTMLParserSchedulerEEE")
//					== 0) {
//				replayTrace::logToFile("Token", ss2.str());
//			}

			if (timerType.compare("N7WebCore5TimerINS_12GCControllerEEE")
					== 0) {
				replayTrace::beforeClose();
			}

			replayTrace::logToFile("timer", "]\n");

			replayTrace::timerEnd(timerId);

			printf("Timer: ***** Fire Finish: %d *****\n", timerId);
//
			dumpTimerInfo.clear();
////
			dumpTimerInfo << "Timer: < < < Fire Finish: " << timerId << "\n";
			dumpTimerInfo
					<< "---------------------------------------------------\n\n";
//			replayTrace::logToFile("timerInfo", dumpTimerInfo.str());

//			//lu: write timer events to log
//

			if (replayController::Instance()->replayMode > 0) {
				if (shouldDump) {

					QWebFrame::dumpState("AfterEvent2");
					shouldDump = false;
				}
			}
		}

#else

		timer->fired();

#endif

#if 0
		//this is just an option
		if (AllDone(my_eventActionsHB, timer, e1_tid, e1_eid, e2_tid, e2_eid)) {
			//dump the state info into a file
		}
#endif

		if (shouldFire) {
			if (!m_eventActionsHB.isInstrumentationDisabled()) {
				m_eventActionsHB.setInTimerEventAction(false);
				m_eventActionsHB.setCurrentEventActionInvalid();
			}
		}

		cout << "Timer: After fire or not fire : " << timerId << "\n";
		// Catch the case where the timer asked timers to fire in a nested event loop, or we are over time limit.
		if (!m_firingTimers || timeToQuit < monotonicallyIncreasingTime())
			break;

	}

	m_firingTimers = false;

	updateSharedTimer();
}

void ThreadTimers::fireTimersInNestedEventLoop() {
	// Reset the reentrancy guard so the timers can fire again.
	m_firingTimers = false;
	updateSharedTimer();
}

} // namespace WebCore

//
//void ThreadTimers::sharedTimerFiredInternal() {
//	// Do a re-entrancy check.
//	if (m_firingTimers)
//		return;
//	m_firingTimers = true;
//
//	double fireTime = monotonicallyIncreasingTime();
//	double timeToQuit = fireTime + maxDurationOfFiringTimers;
//
//	while (!m_timerHeap.isEmpty()
//			&& m_timerHeap.first()->m_nextFireTime <= fireTime) {
//		TimerBase* timer = m_timerHeap.first();
//		timer->m_nextFireTime = 0;
//		timer->heapDeleteMin();
//
//		double interval = timer->repeatInterval();
//
//		stringstream timerPty;
//		stringstream dumpTimerInfo;
//		timerPty << timer;
//		int timerId = replayTrace::uniqueTimerId(timerPty.str());
//		cout << "Timer: Before fire or not fire: " << timerId << ", "
//				<< (string) typeid(*timer).name() << "\n";
//
////		replayTrace::logToFile("timerInfo", "ThreadTimers: Default\n");
//		timer->setNextFireTime(interval ? fireTime + interval : 0, interval);
//
////        static int counter=1;
//
////        counter++;
//		// Once the timer has been fired, it may be deleted, so do nothing else with it after this point.
//#if 0
//		if(notMyTurn(my_eventActionsHB, timer, e1_tid, e1_eid, e2_tid, e2_eid)) {
//			//we should postpone e1
//			timer->setNextFireTime(interval ? fireTime + interval : 0, interval);
//		}
//#endif
//
////		TimerBase* timer = m_timerHeap.first();
//#if 1
//
//		bool shouldFire = true;
//		static map<string, int> timerCount;
//		string timerType;
//		int index;
//		timerType = (string) typeid(*timer).name();
//		if (timerCount.find(timerType) == timerCount.end()) {
//			timerCount[timerType] = 0;
//		}
//		index = timerCount[timerType] + 1;//the index about to fire, +1 on existing index
//
//		std::cout << "Timer type:  " << timerType << "  , Index: " << index
//				<< std::endl;
//
//		if (QWebFrame::getReplayLogMode() > 0) {
//
//			static bool readReplayLogDone = false;
//			if (!readReplayLogDone) {
//				map<int, map<int, ChildInfo> > &e1e2 =
//						replayTrace::readNextReplayTrace();
//				readReplayLogDone = true;
//
//				for (map<int, map<int, ChildInfo> >::iterator it = e1e2.begin();
//						it != e1e2.end(); ++it) {
//					cout << "Racing event: " << (*it).first << "\n";
//				}
//
////				if (e1e2.size() == 0) {
////					e1_visited = true;
////					e2_visited = true;
////				}
//			}
//
////			if (e1e2.size() == 2) {		//there is a replay trace
//////		if (!e2_visited) {
////
////				if (notMyTurn(timerType, index, e1e2[0].first, e1e2[0].second,
////						e1e2[1].first, e1e2[1].second)) {
////					cout << "Should postpone event\n";
////					cout << e1e2[0].first << "\n" << e1e2[0].second << "\n"
////							<< e1e2[1].first << "\n" << e1e2[1].second << "\n";
////
////					shouldFire = false;
////				}
//////		}
////			}
//		}
//
//		if (replayController::Instance()->whetherPostponeTimer(timerId)) {
//			shouldFire = false;
//		}
//
////		if(timerType.compare("N7WebCore5TimerINS_19HTMLParserSchedulerEEE")==0){
////
////			//two things. postpone this parsing event, or postpone it's succesors, or let go
////
////		}
//
//		if (!shouldFire) {
//			interval = 0.001;
////			replayTrace::logToFile("timerInfo", "ThreadTimers: postpone\n");
//			timer->setNextFireTime(interval ? fireTime + interval : 0,
//					interval);
//		}
//
//		else {
//			replayTrace::addTimerEvent(m_eventActionsHB.currentEventAction(),
//					timerType);
//			if (!m_eventActionsHB.isInstrumentationDisabled()) {
//				EventActionId newId = m_eventActionsHB.allocateEventActionId();
//				m_eventActionsHB.setCurrentEventAction(newId);
//				timer->m_lastFireEventAction = newId;
//				if (timer->m_ignoreFireIntervalForHappensBefore) {
//
//					printf(
//							"add arc: ThreadTimers::sharedTimerFiredInternal() \n");
//
//					m_eventActionsHB.addExplicitArc(timer->m_starterEventAction,
//							newId);
//				} else {
//					m_eventActionsHB.addTimedArc(timer->m_starterEventAction,
//							newId, timer->m_nextFireInterval);
//				}
//				m_eventActionsHB.setInTimerEventAction(true);
//			}
//			ActionLogEventTriggered(timer);
//
//			printf(
//					"Fire id:   %d,  action fired: inverval:{ %f }, timer:{ %d }\n",
//					m_eventActionsHB.currentEventAction(), interval, timerId);
////			if (QWebFrame::getReplayLogMode() > 0) {
//			cout << "Should fire event\n";
//			timerCount[timerType] = index;
//			stringstream ss;
//			ss << "Timer Log\n";
//			ss << m_eventActionsHB.currentEventAction() << "\n";
//			ss << timerType << "\n";
//			ss << timerCount[timerType] << "\n";
//			ss << "------------------------------\n";
//
////			}
//
//			replayTrace::writeNestedEvents(ss.str());
////
//			printf("Timer: ***** Fire Start: %d*****\n", timerId);
//
//			dumpTimerInfo.clear();
//
//			dumpTimerInfo << "Current Event: "
//					<< m_eventActionsHB.currentEventAction() << "\n";
//			dumpTimerInfo << "Timer: > > > Fire Start:  " << timerId << "\n";
////			replayTrace::logToFile("timerInfo", dumpTimerInfo.str());
//
//			replayTrace::timerStart(timerId);
//
//			stringstream ss2;
//			ss2 << m_eventActionsHB.currentEventAction() << "\n";
//			ss2 << timerType << "\n";
//			ss2 << timerId;
//			replayTrace::logToFile("timer", ss2.str());
//			replayTrace::logToFile("timer", "[");
//
//			timer->fired();
//
//			replayTrace::logToFile("timer", "]\n");
//
//			replayTrace::timerEnd(timerId);
//
//			printf("Timer: ***** Fire Finish: %d *****\n", timerId);
////
//			dumpTimerInfo.clear();
//////
//			dumpTimerInfo << "Timer: < < < Fire Finish: " << timerId << "\n";
//			dumpTimerInfo
//					<< "---------------------------------------------------\n\n";
////			replayTrace::logToFile("timerInfo", dumpTimerInfo.str());
//
////			//lu: write timer events to log
////
//
//			if (QWebFrame::getReplayLogMode() > 0) {
//				if (shouldDump) {
//
//					QWebFrame::dumpState("AfterEvent2");
//					shouldDump = false;
//				}
//			}
//		}
//
//#else
//
//		timer->fired();
//
//#endif
//
//#if 0
//		//this is just an option
//		if (AllDone(my_eventActionsHB, timer, e1_tid, e1_eid, e2_tid, e2_eid)) {
//			//dump the state info into a file
//		}
//#endif
//
//		if (shouldFire) {
//			if (!m_eventActionsHB.isInstrumentationDisabled()) {
//				m_eventActionsHB.setInTimerEventAction(false);
//				m_eventActionsHB.setCurrentEventActionInvalid();
//			}
//		}
//
//		cout << "Timer: After fire or not fire : " << timerId << "\n";
//		// Catch the case where the timer asked timers to fire in a nested event loop, or we are over time limit.
//		if (!m_firingTimers || timeToQuit < monotonicallyIncreasingTime())
//			break;
//	}
//
//	m_firingTimers = false;
//
//	updateSharedTimer();
//}
