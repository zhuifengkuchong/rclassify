/*
 * racingEventRecognizing.cpp
 *
 *  Created on: Feb 24, 2014
 *      Author: jack
 */

#include "racingEventRecognizing.h"
#include "replayTrace.h"
#include <string>       // std::string
#include <iostream>     // std::cout
#include <sstream>

using namespace std;

replayController::replayController() {
	// TODO Auto-generated constructor stub

	cout << "replayController::replayController() \n";

	enabled = false;
	replayMode = 0;
	replayRaceId = 0;
//	postponeChildTimers = false;
	totalRaces = 0;
//	lastInterestingEvent=-1;
}

replayController::~replayController() {
	// TODO Auto-generated destructor stub
}

replayController* replayController::Instance() {
	static replayController* m_pInstance;
	if (!m_pInstance)   // Only allow one instance of class to be generated.
		m_pInstance = new replayController();
	return m_pInstance;
}

bool replayController::isInterestingToken(int currEid, int lineNumber,
		int lineIndex) {

	if (!enabled || replayController::Instance()->replayMode == 0)
		return false;
//
//	static bool done = false;
//	if (!done) {
//		map<int, map<int, ChildInfo> > &e1e2 =
//				replayTrace::readNextReplayTrace();
//		cout << "Token value 1: " << tokenInfo[0] << "\n";
//		cout << "Token value 2: " << tokenInfo[1] << "\n";
//		done = true;
//	}
	int currToken = lineNumber * 100 + lineIndex;

	bool retVal = false;
	cout << "Check token\n";
	for (map<int, SingleRaceEnforcer>::iterator it =
			replayController::Instance()->raceEnforcers.begin();
			it != replayController::Instance()->raceEnforcers.end(); ++it) {
		SingleRaceEnforcer& RaceEnforcer = (*it).second;

		if (!RaceEnforcer.active)
			continue;

		for (int i = 0; i < 2; i++) {
			if (currToken == (*it).second.tokenInfo[i]) {
				cout << "isInterestingToken: " << currToken << "\n";

				RaceEnforcer.eventStamp[i] = 0;
				RaceEnforcer.eventStampEvents[i][0] = currEid;
				cout << "Token identified: " << currToken << " , eid: "
						<< currEid << " , In race: " << (*it).first << "\n";

				ancesstorToken[currEid] = currToken;

				//check whether to postpone the next parser

				RaceEnforcer.makeAMove(i, currEid, 0);

				if (i == RaceEnforcer.eventToPostpone) {

					map<int, ChildInfo>& childInfo =
							RaceEnforcer.childInfo[currToken][i];
					cout << "Find the event sequence to postpone! i = " << i
							<< "\n";
					cout << "Children size: " << childInfo.size() << "\n";

				}

				retVal = true;
			}

			cout << "-----------------------------------------------\n";
		}
	}
	return retVal;
}

void replayController::isInterestingAcr(int from, int to) {
//1. check if 'from' it's interesting event

	static set<pair<int, int> > existingArcs;

	pair<int, int> currArc = make_pair(from, to);
	if (existingArcs.find(currArc) == existingArcs.end()) {
		existingArcs.insert(currArc);
	} else {
		return;
	}

	if (!enabled || replayController::Instance()->replayMode == 0)
		return;
	cout << "From : " << from << "  , to : " << to << "\n";
	for (map<int, SingleRaceEnforcer>::iterator it =
			replayController::Instance()->raceEnforcers.begin();
			it != replayController::Instance()->raceEnforcers.end(); ++it) {
		SingleRaceEnforcer& RaceEnforcer = (*it).second;
		if (!RaceEnforcer.active)
			continue;

		for (int i = 0; i < 2; i++) {

			cout << "----------------EventSequence: " << (*it).first << " -> "
					<< i << "-----------------------------------\n";

			if (RaceEnforcer.eventStamp[i] == -1) {
				cout << "EventSequence: " << (*it).first << " -> " << i
						<< "  has not started yet, skip checking\n";
				continue;
			}
			if (RaceEnforcer.sequenceWhetherFinished[i]) {
				cout << "EventSequence: " << (*it).first << " -> " << i
						<< "  has finished, skip checking\n";
				continue;
			}

			if (from
					== RaceEnforcer.eventStampEvents[i][RaceEnforcer.eventStamp[i]]) {

				cout << "i=: " << i << "  , Race id=: " << RaceEnforcer.raceId
						<< "\n";
				cout << "      eventStamp[i]: " << RaceEnforcer.eventStamp[i]
						<< "\n";
				cout << "sequenceFinished[i]: "
						<< RaceEnforcer.sequenceWhetherFinished[i] << "\n";

				cout << "isInterestingAcr: event:  " << to
						<< " is child of event: " << from << "\n";

				//2. check if 'to' is the interesting child
				std::string timerType = "NotTimer";
				if (timerEvents.find(to) != timerEvents.end()) {
					timerType = timerEvents[to];
				}
//				map<int, map<int, map<int, ChildInfo> > > &allE1E2 =
//						replayTrace::readAllReplayTrace();

				int token = ancesstorToken[from];
				ancesstorToken[to] = ancesstorToken[from];

				ChildInfo& childNeeded =
						RaceEnforcer.childInfo[RaceEnforcer.tokenInfo[i]][i][RaceEnforcer.eventStamp[i]];
//
				cout << "Current child type: " << timerType << "\n";
				cout << "Child needed type : " << childNeeded.timerType
						<< "\nChild needed index:" << childNeeded.index << "\n";

				//type is what we need
				if (childNeeded.timerType.compare(timerType) == 0) {

					cout << "Debug match: match type: from : " << from
							<< " to: " << to << "  type: " << timerType << "\n";

					//initialize the exisiting index of children to be 1
					static map<int, int> exisingChildren;
					if (exisingChildren.find(from) == exisingChildren.end()) {
						exisingChildren[from] = 1;
					}

					cout << "Debug match: next children size: "
							<< exisingChildren[from] << "\n";
////
					//index is also what we need
					if (childNeeded.index == exisingChildren[from]) {
						cout << "isInteresting moment: Match child!\n";

						//update the stamps
						RaceEnforcer.eventStamp[i]++;
						RaceEnforcer.eventStampEvents[i][RaceEnforcer.eventStamp[i]] =
								to;

						//check is it's the end of the event sequence
						cout << "eventStamp["<<i<<"]: "
								<< RaceEnforcer.eventStamp[i] << "\n";

						RaceEnforcer.makeAMove(i, to,
								RaceEnforcer.eventStamp[i]);

						if (RaceEnforcer.eventStamp[i]
								== RaceEnforcer.childInfo[token][i].size()) {

							cout << "isInteresting moment: Sequence: " << i << " of race: "
									<< RaceEnforcer.raceId << " Finished!!\n";

							if (i == 1) {
								cout << "isInteresting moment: Task of race : " << RaceEnforcer.raceId
										<< " is finished, clear up\n";
								RaceEnforcer.sequenceWhetherFinished[i] = true;
								RaceEnforcer.clearPostponeTimers();
//								RaceEnforcer.active = false;
							}
						}
					} else {
						cout << "Not Match child!\n";
						exisingChildren[from]++;
						return;
					}
				}

			}
			else{
				cout << "EventSequence: " << (*it).first << " -> " << i
						<< "  not match the From event, skip checking\n";
			}
			cout << "---------------------------------------------------\n";
		}
	}

}

bool replayController::whetherPostponeTimer(int timerId) {
	for (map<int, SingleRaceEnforcer>::iterator it =
			replayController::Instance()->raceEnforcers.begin();
			it != replayController::Instance()->raceEnforcers.end(); ++it) {
		SingleRaceEnforcer& RaceEnforcer = (*it).second;

		if (!RaceEnforcer.active)
			continue;

		if (RaceEnforcer.checkPostponeTimer(timerId))
			return true;

	}

	return false;
}

//void replayController::toPostponeTimer(int race, int timerId) {
//	cout << "Start postponing timer:  " << timerId << "\n";
//	if (timerToPostpone[race].find(timerId) == timerToPostpone[race].end())
//		timerToPostpone[race].insert(timerId);
//}

//void replayController::notToPostponeTimer(int timerId) {
//	if (timerToPostpone.find(timerId) != timerToPostpone.end())
//		timerToPostpone.erase(timerId);
//}

void replayController::setReplayMode(int mode) {
	cout << "Replay Mode: " << mode << "\n";
	replayMode = mode;
//	if (mode == 0) {
//		eventToPostpone = -1;
//	} else if (mode == 1 || mode == 2) {
//		eventToPostpone = 1;
//	} else if (mode == 3 || mode == 4) {
//		eventToPostpone = 0;
//	}
}
int replayController::getReplayMode() {

//mode 0: run normal execution, no replay

//mode 1: mutate the trace log, delete replay log -> we use
//mode 2: mutate the trace log, keep the replay log

//mode 3: replay trace log, delete replay log
//mode 4: replay trace log, keep replay log -> we use

	return replayMode;
}
int replayController::getReplayRaceId() {
	return replayRaceId;
}

void replayController::setReplayRaceId(int replayRaceId) {
	this->replayRaceId = replayRaceId;

	raceEnforcers[replayRaceId].eventToPostpone = 0;

	cout << "Set Mutate race: " << replayRaceId << "\n";

}

std::string ChildInfo::print() {

	std::stringstream ss;

	ss << "Child             :->   \n";

	ss << "ChildIndex        :   " << this->index << "\n";

	ss << "ChildType         :   " << this->timerType << "\n";

	if (controlTimerIndex != -1) {
		ss << "controlTimerIndex :   " << this->controlTimerIndex << "\n";
	}

	return ss.str();

}

ChildInfo::ChildInfo() {

	//int parent;
	//int index;
	//std::string timerType;
	//bool done;
	//
	//int controlTimerIndex;

	done = false;
	parent = -1;
	index = -1;
	timerType = "null";
	controlTimerIndex = -1;

}

//ChildInfo();
//
//int parent;
//int index;
//std::string timerType;
//bool done;
//
//int controlTimerIndex;
//
//string print();

