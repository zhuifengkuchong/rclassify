/*
 * replayTrace.h
 *
 *  Created on: Sep 27, 2013
 *      Author: jack
 */
//#include <iostream>

//#include "racingEventRecognizing.h"
//#include "BasicDS.h"
#include "racingEventRecognizing.h"
using namespace std;
//using namespace WebCore;

class replayTrace {
public:
	static replayTrace* Instance();

	vector<string> readReplayConfigure();

	static map<int, map<int, ChildInfo> >& readNextReplayTrace();
	static map<int, map<int, map<int, map<int, ChildInfo> > > > & readAllReplayTrace();

	static void initReplayRaces();

	static char* getFileFullPath();

	void appendToReplayLog(string line);
	void clearReplayLog();

	vector<pair<string, string> > events;

	void addEvent(string element, string eventType);
	pair<string, string> pullEvent();
	vector<string> split_string(string input, string split_by, int times);

	static void writeNestedEvents(string s);
//	static void writeToFile(char* file,string s);

	static void AddFunction(void* node, string function);
	static void RemoveFunction(void* node, string function);
	static string GetFunctions(void* node);

	static void ResetDump();

	static void AddToDump(string s);
	static void setReplayLogMode(int input);

	static void DoDump(char* dumpName);

	static void addToThread(int pred, int curr);

	//lu: thread info from parser
	static void addArc(int from, int to, bool record, string color);
	static bool addToken(unsigned currEid, string htmlSource, int lineNumber,
			string token);
	static int getLastTokenIndex();
	static void logToFile(char* file, string content);
	static void beforeClose();
	static void addTimerEvent(int eid, string type);

	static int uniqueTimerId(string ptr);
	static int uniqueTimerId2(string ptr);
	static void timerStart(int id);
	static void timerEnd(int id);
	static int getLatestTimer();
	static std::map<int, int> getCurrentTimers();


//	static std::map<int, map<int, string> > timersSetInEvent;

//	static int getReplayLogMode();

//	static bool replayOriginal;
//	static bool replayTrace;
//	static bool archiveTrace;


	static void startEvent(int id);
	static void endEvent();
	static int eventWillBeNested();


private:
	replayTrace();
	virtual ~replayTrace();

};

