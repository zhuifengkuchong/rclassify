/*
 * racingEventRecognizing.h
 *
 *  Created on: Feb 24, 2014
 *      Author: jack
 */

#ifndef RACINGEVENTRECOGNIZING_H_
#define RACINGEVENTRECOGNIZING_H_

//#include <iostream>
#include "BasicDS.h"
using namespace std;

class SingleRaceEnforcer {
public:
	SingleRaceEnforcer();
	virtual ~SingleRaceEnforcer();

	int raceId;

	bool active;

	set<int> timerToPostpone;
	int eventStamp[2];
	int eventStampEvents[2][100];
	int tokenInfo[2];
	int eventToPostpone;
	bool sequenceWhetherFinished[2];
	int lastControlableChild[2];

//	bool postponeNextParsingTimer;
//	bool postponeOtherTimers;

	map<int, map<int, map<int,ChildInfo> > > childInfo;//map<token, map<chindIndex, map<sequenceId, ChildInfo> > >

	void insertPostponeTimer(int id);
	bool checkPostponeTimer(int id);
	void clearPostponeTimers();

	bool makeAMove(int sequenceId, int eid, int position);

	string printPostponeTimers();

	pair<int, ChildInfo> lastInterestingEvent;

};

class replayController {
public:
	replayController();
	virtual ~replayController();

	static replayController* Instance();

	bool isInterestingToken(int currEid, int lineNumber, int lineIndex);
	void isInterestingAcr(int from, int to);

	bool whetherPostponeTimer(int timerId);
//	void toPostponeTimer(int race, int timerId);
//	void notToPostponeTimer(int timerId);

	void setReplayMode(int mode);
	int getReplayMode(); //replay mode, 0-4

	int getReplayRaceId();

	void setReplayRaceId(int replayRaceId);

	int totalRaces;

	map<int, int> ancesstorToken; //which event belog to which

	map<int, std::string> timerEvents;

	bool enabled; //whether control is enabled

//	bool postponeNextParsingTimer;
//	bool postponeChildTimers;

	int replayMode;

	int replayRaceId;

	int parsingEventTimerId;

	map<int, SingleRaceEnforcer> raceEnforcers;

	map<int, map<int, int> > timersSetInEvent;
	map<int, map<int, int> > timersSetInEventReverse;
	map<int, string> timerPtyNameTable;


//private:

};

#endif /* RACINGEVENTRECOGNIZING_H_ */
