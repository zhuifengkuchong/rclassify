/*
 * racingEventRecognizing.cpp
 *
 *  Created on: Feb 24, 2014
 *      Author: jack
 */

#include "racingEventRecognizing.h"
#include "replayTrace.h"
#include <string>       // std::string
#include <iostream>     // std::cout
#include <sstream>

using namespace std;

SingleRaceEnforcer::SingleRaceEnforcer() {
	// TODO Auto-generated constructor stub

	cout << "SingleRaceEnforcer: \n";

	eventStamp[0] = -1;
	eventStamp[1] = -1;

	for (int i = 0; i < 100; ++i) {
		eventStampEvents[0][i] = 0;
		eventStampEvents[1][i] = 0;
	}

	sequenceWhetherFinished[0] = false;
	sequenceWhetherFinished[1] = false;

	eventToPostpone = 1; //postpone the second event in the replay trace, by default

//	postponeNextParsingTimer = false;
//	postponeOtherTimers = false;

	active = true;

	raceId = 0;

}

SingleRaceEnforcer::~SingleRaceEnforcer() {
	// TODO Auto-generated destructor stub
}

void SingleRaceEnforcer::insertPostponeTimer(int id) {
	timerToPostpone.insert(id);
	cout << "Race : #" << this->raceId << "  will postpone timer: " << id
			<< "\n";

}
bool SingleRaceEnforcer::checkPostponeTimer(int id) {
	if (timerToPostpone.find(id) != timerToPostpone.end())
		return true;
	else
		return false;

}
void SingleRaceEnforcer::clearPostponeTimers() {
	timerToPostpone.clear();
}
string SingleRaceEnforcer::printPostponeTimers() {
	std::stringstream ss;
	ss << "-----Timer to postpone: -----------\n";
	for (set<int>::iterator it = timerToPostpone.begin();
			it != timerToPostpone.end(); ++it) {
		ss << "|  " << (*it) << "  |\n";
	}
	return ss.str();
}

bool SingleRaceEnforcer::makeAMove(int sequenceId, int eid, int position) {

	cout << "-------------Make a move-----------\n" << "Race     :  " << raceId
			<< "\n" << "Sequence :  " << sequenceId << "\n" << "to Pos   :  "
			<< position << "\n" << "last cont:  "
			<< lastControlableChild[sequenceId] << "\n" << "Eid      :  " << eid
			<< "\n---------------------------------------\n";

//	replayController::Instance()->raceEnforcers[replayRaceId].lastControlableChild[eventSequenceId] =
//									childIndex;
	if (sequenceId == 0) {
		if (position == lastControlableChild[sequenceId]) {
			cout << "Should postpone the timer forked from here:  " << eid
					<< "\n";

			cout << "Timer position: "
					<< childInfo[tokenInfo[sequenceId]][sequenceId][lastControlableChild[sequenceId]].controlTimerIndex
					<< "\n";

			cout << "Timer type    : "
					<< childInfo[tokenInfo[sequenceId]][sequenceId][lastControlableChild[sequenceId]].timerType
					<< "\n";
//		cout << "Timer Type    : " << lastControlableChild[sequenceId] << "\n";

			lastInterestingEvent.first = eid;
			lastInterestingEvent.second =
					childInfo[tokenInfo[sequenceId]][sequenceId][lastControlableChild[sequenceId]];

		}
	}
	return false;
}
