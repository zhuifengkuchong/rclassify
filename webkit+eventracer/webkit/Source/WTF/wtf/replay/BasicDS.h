/*
 * replayTrace.h
 *
 *  Created on: Sep 27, 2013
 *      Author: jack
 */
#include <iostream>
#include <vector>
#include <map>
#include <set>
using namespace std;
//using namespace WebCore;

struct eventhandler {
	string nodePrt;
	string eventType;
	string functionName;
};

struct Arc {

	int m_tail;
	int m_head;
	// The duration of the arc, -1 if the duration is unknown.
	int m_duration;
};

class TokenInfo {
public:
	int eid;
	int lineNumber;
	string source;
	int lineIndex; //which token of the line

	bool equal(TokenInfo& other);

	TokenInfo() {

	}
	;

	string print();
};
struct TimerInfo {
	int eid;
	std::string type;
	int timerId;
	vector<int> forkTimers;
	int forkedFrom;

	string print();
};

struct ChildInfo {

	ChildInfo();

	int parent;
	int index;
	std::string timerType;
	bool done;

	int controlTimerIndex;

	string print();
	//////////////
//	int lineNumber;
//	string source;
//	int lineIndex; //which token of the line
};
