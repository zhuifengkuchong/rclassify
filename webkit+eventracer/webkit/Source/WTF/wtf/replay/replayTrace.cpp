/*
 * replayTrace.cpp
 *
 *  Created on: Sep 27, 2013
 *      Author: jack
 */
#include<stdlib.h>
#include<iostream>
#include<fstream>
#include <sstream>
#include <string>
#include "replayTrace.h"
#include <assert.h>
#include <fstream>
#include<sys/stat.h>
#include<sys/types.h>
#include<dirent.h>
#include <string.h>
#include <map>
#include <set>
#include <vector>
#include "WebKit/qt/Api/qwebframe.h"
//#include "WebCore/bindings/js/JSEventListener.h"

using namespace std;
using namespace WebCore;
//using namespace WebCore;
//using namespace WebCore;

char* fileName = "replay/configure.txt";

static int ReplayLogMode = 0;
static bool afterClose = false;
replayTrace* replayTrace::Instance() {
	static replayTrace* m_pInstance;
	if (!m_pInstance)   // Only allow one instance of class to be generated.
		m_pInstance = new replayTrace();
	return m_pInstance;
}

void replayTrace::clearReplayLog() {

	//	char* fileName = "replay/configure.txt";

	static bool done = false;
	if (!done) {
		ofstream myfile;
		myfile.open(fileName, ios::out);
		myfile << "";
		myfile.close();
		done = true;
	}
}

replayTrace::replayTrace() {

	logToFile("dot", "digraph vis{");

//	readReplayConfigure();
//	clearReplayLog();
//	appendToReplayLog("line");
	// TODO Auto-generated constructor stub

}

replayTrace::~replayTrace() {
//	logToFile("dot", "}");
	// TODO Auto-generated destructor stub
}

void replayTrace::initReplayRaces() {

	cout << "Replay Mode: " << replayController::Instance()->replayMode << "\n";
	cout << "Replay Race: " << replayController::Instance()->replayRaceId
			<< "\n";

	if (replayController::Instance()->replayMode > 0) {

		static bool readReplayLogDone = false;
		if (!readReplayLogDone) {
			readReplayLogDone = true;

			replayController::Instance()->enabled = true;

			if (replayController::Instance()->replayMode > 0) {

//				map<int, map<int, map<int, ChildInfo> > > &allE1E2 =
						replayTrace::readAllReplayTrace();

//				for (map<int, map<int, map<int, ChildInfo> > >::iterator it =
//						allE1E2.begin(); it != allE1E2.end(); ++it) {
//					cout << "Race: " << (*it).first << "\n";
//					for (map<int, map<int, ChildInfo> >::iterator it2 =
//							(*it).second.begin(); it2 != (*it).second.end();
//							++it2) {
//						cout << "--Racing event: " << (*it2).first << "\n";
//
//						for (map<int, ChildInfo>::iterator it3 =
//								(*it2).second.begin();
//								it3 != (*it2).second.end(); ++it3) {
//							cout << "----Child Index: " << (*it3).first << "\n";
//							cout << "------Child Info : index: "
//									<< (*it3).second.index << "\n";
//							cout << "------Child Info : Type : "
//									<< (*it3).second.timerType << "\n";
//
//							if ((*it3).second.controlTimerIndex != -1) {
////								cout << "Control timer Index : " << EI.controlTimerIndex  << "\n";
//								cout << "------Control timer Index : "
//										<< (*it3).second.controlTimerIndex
//										<< "\n";
//							}
//						}
//					}
//
//				}

			}

		}

	}
}

bool ReplayNotReady = true;
vector<string> getAllFile(char * dir) {
	printf("Read All Replay Trace\n");

	vector<string> retVal;

	DIR *d;
	struct dirent * entry;
	struct stat buf;
	if ((d = opendir(dir)) != NULL) {
		entry = readdir(d);
		while (entry != NULL) {
			if (0 != strcmp(".", entry->d_name) && //Skip those directories
					0 != strcmp("..", entry->d_name)) {

				char * name = entry->d_name;
				stat(name, &buf);

//				if (S_ISDIR(buf.st_mode))       //Check if sub directory
//						{
////					cout << "/";            //Formatting
//
//				} else {
//					cout << name << "\n";
//				}

				if (strstr(name, "replay") && strstr(name, ".log")) {
//					closedir(d);
					cout << name << "\n";

//					return name;
					retVal.push_back((string) name);

				}

			}
			entry = readdir(d);             //Next file in directory
		}
		closedir(d);
	}
//	ReplayNotReady = true;
//	return "";
	ReplayNotReady = false;
	return retVal;
}

char* getNextFile(char * dir) {
	printf("Read Next Replay Trace\n");

	DIR *d;
	struct dirent * entry;
	struct stat buf;
	if ((d = opendir(dir)) != NULL) {
		entry = readdir(d);
		while (entry != NULL) {
			if (0 != strcmp(".", entry->d_name) && //Skip those directories
					0 != strcmp("..", entry->d_name)) {

				char * name = entry->d_name;
				stat(name, &buf);

//				if (S_ISDIR(buf.st_mode))       //Check if sub directory
//						{
////					cout << "/";            //Formatting
//
//				} else {
//					cout << name << "\n";
//				}

				if (strstr(name, "replay") && strstr(name, ".log")) {
					closedir(d);
//					cout << name << "\n";
					ReplayNotReady = false;
					return name;
				}

			}
			entry = readdir(d);             //Next file in directory
		}
		closedir(d);
	}
	ReplayNotReady = true;
	return "";
}
//static vector<pair<string, int> > e1e2;
char replayLogPath[1024];
char replayLogName[1024];

char* replayTrace::getFileFullPath() {
	return replayLogPath;
}
char * getLogPureName(char* s) {
	char subbuff[1024];
	memcpy(subbuff, &s[0], strlen(s) - 4);
//	cout<<subbuff<<"\n";
	return subbuff;
}

bool TokenInfo::equal(TokenInfo& other) {
	if (this->lineNumber == other.lineNumber
			&& this->lineIndex == other.lineIndex)
		return true;
	else
		return false;
}

map<int, map<int, map<int, map<int, ChildInfo> > > > allRaceInfo;
map<int, map<int, map<int, map<int, ChildInfo> > > > & replayTrace::readAllReplayTrace() {

//	return allRaceInfo;

	static bool done = false;
	if (!done) {
		done = true;

		vector<string> allRaceLogs = getAllFile("replay/");

		replayController::Instance()->totalRaces = allRaceLogs.size();

		system("mkdir replay");
		system("mkdir replay/done");

		for (vector<string>::iterator it = allRaceLogs.begin();
				it != allRaceLogs.end(); ++it) {
			cout << "Race log: " << (*it) << "\n";

//			string temp1 = (*it).substr(0, (*it).length() - 4);
			string temp2 = (*it).substr(6, (*it).length() - 1);
//			cout<<"temp1: "<<temp1<<"\n";

//			cout << "temp2: " << temp2 << "\n";

			string temp1 = temp2.substr(0, temp2.length() - 4);
//			cout << "temp1: " << temp1 << "\n";

			int replayRaceId = atoi(temp1.c_str());

			cout << "replayLogMode: "
					<< replayController::Instance()->replayMode << "\n";
			cout << "replayLogId  : " << replayRaceId << "\n";
			cout << "replayRaceId : "
					<< replayController::Instance()->replayRaceId << "\n";

			if (replayController::Instance()->replayMode < 5) {
				if ((replayRaceId - replayController::Instance()->replayRaceId)
						!= 0) {
					cout << "skip\n";
					continue;
				}
			}

			sprintf(replayLogName, "%s", (*it).c_str());

//			if (ReplayNotReady)
//				return allRaceInfo;
			sprintf(replayLogPath, "replay/%s", replayLogName);

			cout << "Open file: " << (string) replayLogPath << "\n";

			string line;
			ifstream myReadFile;
			myReadFile.open(replayLogPath);

			int tokenCode = 0;
			int eventSequenceId = -1;
			if (myReadFile.is_open()) {
				while (getline(myReadFile, line)) {

					if (line.compare("Begin") == 0) {
						tokenCode = 0;
						continue;
					}
					if (line.compare("End") == 0) {
						cout << "\n";
						//					timer = 0;
						continue;
					}
					if (line.compare("Token:") == 0) {
						eventSequenceId++;
						cout << "Token     :   \n";

						getline(myReadFile, line);
						tokenCode += atoi(line.c_str()) * 100;

						cout << "LineNumber:   " << atoi(line.c_str()) << "\n";
						getline(myReadFile, line);
						tokenCode += atoi(line.c_str());

						cout << "LineIndex :   " << atoi(line.c_str()) << "\n";

						cout << "Timer value: " << tokenCode << "\n";

//						replayController::Instance()->tokenInfoAll[replayLogId][tokenCount] =
//								timer;

						replayController::Instance()->raceEnforcers[replayRaceId].tokenInfo[eventSequenceId] =
								tokenCode;
						replayController::Instance()->raceEnforcers[replayRaceId].raceId =
								replayRaceId;
						replayController::Instance()->raceEnforcers[replayRaceId].active =
								true;

						if (replayController::Instance()->getReplayMode() <= 5)
							if (replayRaceId
									== replayController::Instance()->replayRaceId) {
								replayController::Instance()->raceEnforcers[replayRaceId].eventToPostpone =
										0;
							}


						continue;
					}
					if (line.compare("Child :non-controllable") == 0) {
						cout << "Child :non-controllable     :   \n";
						getline(myReadFile, line);
						ChildInfo thisChild;
						thisChild.index = atoi(line.c_str());
						cout << "ChildIndex:   " << thisChild.index << "\n";

						getline(myReadFile, line);
						thisChild.timerType = line;
						cout << "ChildType :   " << thisChild.timerType << "\n";

						int childIndex =
								allRaceInfo[replayRaceId][tokenCode][eventSequenceId].size();

						allRaceInfo[replayRaceId][tokenCode][eventSequenceId][childIndex] =
								thisChild;

						replayController::Instance()->raceEnforcers[replayRaceId].childInfo[tokenCode][eventSequenceId][childIndex] =
								thisChild;
						cout << "Current child Index : " << childIndex << "\n";
						continue;

					}

					if (line.compare("Child :controllable") == 0) {
						cout << "Child :controllable     :   \n";
						getline(myReadFile, line);
						ChildInfo thisChild;
						thisChild.index = atoi(line.c_str());
						cout << "ChildIndex:   " << thisChild.index << "\n";

						getline(myReadFile, line);
						thisChild.timerType = line.substr(6, line.length()-6);
						cout << "ChildType :   " << thisChild.timerType << "\n";

						int childIndex =
								allRaceInfo[replayRaceId][tokenCode][eventSequenceId].size();

						allRaceInfo[replayRaceId][tokenCode][eventSequenceId][childIndex] =
								thisChild;

						getline(myReadFile, line);

						thisChild.controlTimerIndex = atoi(line.c_str());

						cout << "Control timer Index : "
								<< thisChild.controlTimerIndex << "\n";

						replayController::Instance()->raceEnforcers[replayRaceId].childInfo[tokenCode][eventSequenceId][childIndex] =
								thisChild;

						replayController::Instance()->raceEnforcers[replayRaceId].lastControlableChild[eventSequenceId] =
								childIndex;
						cout << "Current child Index : " << childIndex << "\n";
						cout << "Last controlable child of Race: "
								<< replayRaceId << ",  eventSequence: "
								<< eventSequenceId << ",  is " << childIndex
								<< "\n";
						continue;

					}

				}

				myReadFile.close();
			}

		}

	}

	return allRaceInfo;
}

//map<int, map<int, ChildInfo> > raceInfo;
//map<int, map<int, ChildInfo> >& replayTrace::readNextReplayTrace() {
//
//	static bool done = false;
//	if (!done) {
//		done = true;
////		e1e2.clear();
//		cout << "test0\n";
//		system("mkdir replay");
//		sprintf(replayLogName, "%s", getNextFile("replay/"));
//
////		if (strcmp(replayLogName, "") == 0)
////			return e1e2;
//		cout << "test1\n";
//		if (ReplayNotReady)
//			return raceInfo;
//		sprintf(replayLogPath, "replay/%s", replayLogName);
//
//		cout << "test2\n";
////		AddToDump("test");
////		AddToDump("test2");
////		DoDump("afterE2");
////
////		cout<<"Size of the log: "<<strlen(replayLogName)<<"\n";
////		cout<<replayLogName<<"\n";
//
//		cout << "Open file: " << (string) replayLogPath << "\n";
//
//		string line;
//		ifstream myReadFile;
//		myReadFile.open(replayLogPath);
//
//		int timer = 0;
//		int tokenCount = 0;
//		if (myReadFile.is_open()) {
//			while (getline(myReadFile, line)) {
//
//				if (line.compare("Begin") == 0) {
//					timer = 0;
//					continue;
//				}
//				if (line.compare("End") == 0) {
//					cout << "\n";
////					timer = 0;
//					continue;
//				}
//				if (line.compare("Token:") == 0) {
//					cout << "Token     :   \n";
//
//					getline(myReadFile, line);
//					timer += atoi(line.c_str()) * 100;
//
//					cout << "LineNumber:   " << atoi(line.c_str()) << "\n";
//					getline(myReadFile, line);
//					timer += atoi(line.c_str());
//
//					cout << "LineIndex :   " << atoi(line.c_str()) << "\n";
//
//					cout << "Timer value: " << timer << "\n";
//
////					replayController::Instance()->tokenInfo[tokenCount] = timer;
////					tokenCount++;
//					continue;
//				}
//				if (line.compare("Child:") == 0) {
//					cout << "Child     :   \n";
//					getline(myReadFile, line);
//					ChildInfo EI;
//					EI.index = atoi(line.c_str());
//					cout << "ChildIndex:   " << EI.index << "\n";
//
//					getline(myReadFile, line);
//					EI.timerType = line;
//					cout << "ChildType :   " << EI.timerType << "\n";
//
//					raceInfo[timer][raceInfo[timer].size()] = EI;
//
////					replayController::Instance()->racingEventType[tokenCount] =
////							EI.timerType;
//					continue;
//
//				}
////
//////			cout << line << "\n";
////				string type1 = line;
////
////				getline(myReadFile, line);
//////			cout << line << "\n";
////				int index1 = atoi(line.c_str());
////
////				getline(myReadFile, line);
//////			cout << line << "\n";
////				string type2 = line;
////
////				getline(myReadFile, line);
//////			cout << line << "\n";
////				int index2 = atoi(line.c_str());
////
////				if (ReplayLogMode == 1 || ReplayLogMode == 2) {
////					e1e2.push_back(make_pair(type1, index1));
////					e1e2.push_back(make_pair(type2, index2));
////				} else if (ReplayLogMode == 3 || ReplayLogMode == 4) {
////
////					e1e2.push_back(make_pair(type2, index2));
////					e1e2.push_back(make_pair(type1, index1));
////				}
//
//			}
//
//			myReadFile.close();
//		}
//		system("mkdir replay/done");
//
////		if (ReplayLogMode == 1 || ReplayLogMode == 3) {
////			char command[1024];
////			sprintf(command, "mv replay/%s replay/done/%s", replayLogName,
////					replayLogName);
////			system(command);
////		}
//	}
//	return raceInfo;
//}

//
////lu: add a function to read the configure.txt
vector<string> replayTrace::readReplayConfigure() {

	vector<string> ret;
	static bool done = false;
	if (done) {
		return ret;
	} else {
		done = true;
		cout << "Read replay log from : replay/configure.txt\n" << endl;
	}

//	char* fileName = "replay/configure.txt";
	string line;
	ifstream myReadFile;
	myReadFile.open(fileName);
	if (myReadFile.is_open()) {
		while (getline(myReadFile, line)) {
//			cout << line << endl;
			ret.push_back(line);

			vector<string> event = split_string(line, "|->|", 9);
			assert(event.size() == 2 && "Invalid event");
			events.push_back(make_pair(event[0], event[1]));
//			cout << "Event :" << event[0] << "  Target: " << event[1] << "\n";
		}
		myReadFile.close();
	}
	return ret;

}

void replayTrace::appendToReplayLog(string line) {
//	char* fileName = "replay/configure.txt";

	return;

	ofstream myfile;
	myfile.open(fileName, ios::out | ios::app);
	myfile << line << endl;
	myfile.close();

}

void replayTrace::addEvent(string element, string eventType) {
	events.push_back(make_pair(element, eventType));
	stringstream ss;
	ss << eventType << "|->|" << element << "|->|";
//	appendToReplayLog(element.append("-->>").append(eventType).append("\n"));
	appendToReplayLog(ss.str());
}

pair<string, string> replayTrace::pullEvent() {
	pair<string, string> ret;
	vector<pair<string, string> >::iterator it = events.begin();

	if (it != events.end()) {
		ret = *it;
		events.erase(it);
	} else {
		ret = make_pair("Empty", "Empty");
	}

	return ret;

}

vector<string> replayTrace::split_string(string input, string split_by,
		int times) {
	vector<string> str_list;
	if (input.size() < 1)
		return str_list;
	int comma_n = 0;
	for (int i = 0; i < times; i++) {
		std::string tmp_s = "";
		comma_n = input.find(split_by);
		if (-1 == comma_n) {
			tmp_s = input.substr(0, input.length());
			str_list.push_back(tmp_s);
			break;
		}
		tmp_s = input.substr(0, comma_n);
		input.erase(0, comma_n + split_by.size());
		str_list.push_back(tmp_s);
//		cout<<"tmp_s: "<<tmp_s<<endl;
	}
	return str_list;
}

void replayTrace::writeNestedEvents(string s) {
	static bool removeZlLog = false;
	if (!removeZlLog) {
		system("rm zlLog.txt");
		removeZlLog = true;
	}

	ofstream myfile("zlLog.txt", ios::app);
	if (myfile.is_open()) {
		myfile << s;
		myfile.close();
	} else
		cout << "Unable to open file";

}

//void replayTrace::writeToFile(char* file,string s) {
//	char rmCommand[1024];
//	sprintf(rmCommand, "rm %s", file);
//	static bool removeZlLog = false;
//	if (!removeZlLog) {
//		system(rmCommand);
//		removeZlLog = true;
//	}
//
//	ofstream myfile(file, ios::app);
//	if (myfile.is_open()) {
//		myfile << s <<"\n";
//		myfile.close();
//	} else
//		cout << "Unable to open file";
//
//}

static map<void*, set<string> > eventListeners;
void replayTrace::AddFunction(void* node, string function) {
	cout << "Add JSeventlistnener to node" << node << "\n" << function << "\n";
	eventListeners[node].insert(function);
}

string replayTrace::GetFunctions(void* node) {
	stringstream ss;
	set<string>::iterator it;
	if (eventListeners[node].size() > 0)
		ss << "Event handlers: \n";
	for (it = eventListeners[node].begin(); it != eventListeners[node].end();
			++it) {
		ss << *it << "\n";
	}
	return ss.str();

}

void replayTrace::RemoveFunction(void* node, string function) {
	cout << "Remove JSeventlistnener to node" << node << "\n" << function
			<< "\n";
	eventListeners[node].erase(function);
}

static vector<string> dumpContent;
void replayTrace::ResetDump() {
	dumpContent.clear();
}

void replayTrace::AddToDump(string s) {
	dumpContent.push_back(s);
	dumpContent.push_back("\n");
}

void replayTrace::setReplayLogMode(int input) {
	ReplayLogMode = input;
}

void replayTrace::addToThread(int pred, int curr) {

}

void replayTrace::DoDump(char* dumpName) {
	const char* dumpFileName;
	stringstream ss;

	string dumpPath = "Original";
	if (replayController::Instance()->replayMode > 0) {
//		if (!ReplayNotReady) {
//			cout << "replayLogPath: " << replayLogName << "\n";
//
//			dumpPath = (string) getLogPureName(replayLogName);
//		}
		stringstream ss2;
		ss2 << "replay" << replayController::Instance()->replayRaceId;
		dumpPath = ss2.str();

	}
	static bool done = false;
	if (!done) {

		stringstream command;
//		command << "cd replay; rm -rf " << dumpPath << "; mkdir " << dumpPath;
		command << "mkdir replay/" << dumpPath;

		system(command.str().c_str());
		done = true;
	}

	ss << "replay/" << dumpPath << "/" << dumpName;

	if (ReplayLogMode == 3 || ReplayLogMode == 4) {
		ss << "_ori";
	}

	dumpFileName = ss.str().c_str();

	cout << "Dump to file: " << dumpFileName << "\n";

	char Command[1024];
	static map<string, bool> done2;
//	static bool done = false;
	if (done2.find((string) dumpFileName) == done2.end()) {
		system("mkdir DebugInfo");
		for (int i = 10; i >= 2; --i) {
			sprintf(Command, "mv %s%d %s%d", dumpFileName, i, dumpFileName,
					i + 1);
			system(Command);
		}
//		sprintf(Command, "mv %s2 %s3", dumpFileName, dumpFileName);
//		system(Command);
		sprintf(Command, "mv %s %s2", dumpFileName, dumpFileName);
		system(Command);
		done2[(string) dumpFileName] = true;
	}

	ofstream myfile(dumpFileName);
	if (myfile.is_open()) {

		stringstream ss3;
		vector<string>::iterator it;

		for (it = dumpContent.begin(); it != dumpContent.end(); ++it) {
			myfile << *it;
			ss3 << *it;
		}
		myfile.close();
		logToFile("html", ss3.str());
	} else
		cout << "Unable to dump state to file\n";

	ResetDump();
}
struct tokenInfo {
	int currEid;
	std::string htmlSource;
	int lineNumber;
	std::string token;
	int lineIndex;
	bool operator==(tokenInfo& anotherToken) {
		if (this->htmlSource.compare(anotherToken.htmlSource) == 0) {
			if (this->lineNumber == anotherToken.lineNumber) {
				if (this->lineIndex == anotherToken.lineIndex)
					return true;
			}
		}
		return false;
	}
};
static int lastEid = -1;
static int tokenCount = 0;
static tokenInfo lastToken;
static map<int, int> tokenThreads;             //eid, thread id
static map<int, set<int> > eventHappenAfter;
static map<int, string> threadToToken;             //thread id, token
//static map<int, string> timerEvents;
static map<int, set<int> > arcs;

int replayTrace::getLastTokenIndex() {
	return tokenCount;
}

//void replayTrace::addTimerEvent(int eid, string type) {
//	timerEvents[eid] = type;
//}

void replayTrace::addArc(int from, int to, bool record = true, string color =
		"black") {
	if (!record) {
		return;
	}
	if (afterClose) {
		return;
	}

//	static bool done=false;
//	if(!done){
//		eventHappenAfter[1].insert(0);
//		done=true;
//	}

//	stringstream ss;
//
//	if (eventHappenAfter.find(from) != eventHappenAfter.end()) {
//		ss << "Arc: " << from << " -> " << to << "\n";
////		ss<<"Fork: Thread: "<<tokenThreads[from]<<" -> "<<to<<"\n";
//		eventHappenAfter[to].insert(eventHappenAfter[from].begin(),
//				eventHappenAfter[from].end());
//		for (set<int>::iterator it = eventHappenAfter[to].begin(), it2 =
//				eventHappenAfter[to].end(); it != it2; ++it) {
//			ss << "Event: " << to << " Belongs to Thread: " << *it << "\n";
//			ss << threadToToken[*it] << "\n";
//		}
//	}

//	stringstream ss2;
//	if (tokenThreads.find(from) != tokenThreads.end()) {
//		ss2 << tokenThreads[from] << " -> " << to;
//		replayTrace::Instance()->logToFile("dot", ss2.str());
//	} else {
//	ss2 << from << " -> " << to << "[color=\"" << color << "\"]";
//		replayTrace::Instance()->logToFile("dot", ss2.str());
//	}

//	if (timerEvents.find(from) != timerEvents.end()) {
//		ss2 << "Timer" << from;
//	} else {
//		ss2 << from;
//	}
//	ss2 << " -> ";
//	if (timerEvents.find(to) != timerEvents.end()) {
//		ss2 << "Timer" << to;
//	} else {
//		ss2 << to;
//	}
//	replayTrace::Instance()->logToFile("dot", ss2.str());

//	logToFile("thread", ss.str());

	stringstream ss2;
	ss2 << from << " -> " << to << "[color=\"" << color << "\"]";
	replayTrace::Instance()->logToFile("dot", ss2.str());
}

void replayTrace::beforeClose() {
	cout << "Do something before close\n";

	logToFile("dot", "}");

	afterClose = true;

}

bool replayTrace::addToken(unsigned currEid, string htmlSource, int lineNumber,
		string token) {
	++tokenCount;
//	cout << "htmlSource: " << htmlSource << "\n";
//	cout << "lineNumber: " << lineNumber << "\n";
//	cout << "token: " << token << "\n";

//	static string originalHtml = "";
//	static bool setOriginalHtml = false;
//	if (!setOriginalHtml) {
//		originalHtml = htmlSource;
//		setOriginalHtml = true;
//	}
//	if (htmlSource.compare(originalHtml) != 0)
//		return false;

//	static map<int, string> tokens;             //eid, string
//	static map<string, int> tokensReverse;
//	static map<int, int> lineNumberCount;         //the tokens parsed for a line
//	static int lastEid = -1;
//	static int tokenCount = 0;

//	stringstream ss;
////	ss << "Id: " << tokenCount << " -> ";
//	ss << lineNumber << " : ";
//	ss << htmlSource << "\n";
//	ss << token << "\n";
//	logToFile("token", ss.str());

//	tokens[tokenCount] = ss.str();
//	tokensReverse[ss.str()] = tokenCount;
//	lastEid = currEid;
//	lastToken.currEid = currEid;
//	lastToken.htmlSource = htmlSource;
//	lastToken.lineNumber = lineNumber;
//	lastToken.token = token;
//
//	if (lineNumberCount.find(lineNumber) == lineNumberCount.end()) { //the count of tokens for a line
//		lineNumberCount[lineNumber] = 1;
//	} else {
//		++lineNumberCount[lineNumber];
////		assert(lineNumberCount[lineNumber] < 1000);
//	}
//	int tokenThreadId = lineNumber * 1000 + lineNumberCount[lineNumber]; //line 44, the second token, thread id= 44*1000+2= 44002
//	tokenThreads[currEid] = tokenThreadId;
//
//	threadToToken[tokenThreadId] = ss.str();
//
////	assert(eventHappenAfter.find(currEid) == eventHappenAfter.end());
//	eventHappenAfter[currEid].clear();
//	eventHappenAfter[currEid].insert(tokenThreadId);
	stringstream ss2;
	ss2 << "Event id :  "<< currEid << "\n";
//	ss2 << "Thread: " << tokenThreadId << "\n";
	ss2 << "Line num :  "<<lineNumber << "\n";
	ss2 << "Html src :  "<<htmlSource << "\n";
//	ss2 << lineNumberCount[lineNumber] << "\n";
//	ss2<<"\n";
	ss2 << "Token src:  " << token << "\n";
	ss2 <<"--Token src end\n";

	cout << ss2.str() << "\n";
	logToFile("token", ss2.str());

//	return replayController::Instance()->isInterestingToken(currEid, lineNumber,
//			lineNumberCount[lineNumber]);
	return false;

}

void replayTrace::logToFile(char* file, string content) {

//	if(afterClose) return;

//	replayTrace::Instance();

	char Command[1024];

	static map<string, bool> done;
//	static bool done = false;
	if (done.find((string) file) == done.end()) {
		system("mkdir DebugInfo");
		sprintf(Command, "mv DebugInfo/%s2 DebugInfo/%s3", file, file);
		system(Command);
		sprintf(Command, "mv DebugInfo/%s DebugInfo/%s2", file, file);
		system(Command);
		done[(string) file] = true;
	}
	sprintf(Command, "DebugInfo/%s", file);
	ofstream myfile(Command, ios::app);
	if (myfile.is_open()) {
//				myfile << "countPumpTokenizer: " << countPumpTokenizer
//						<< "   , countAddArc: " << countAddArc << "\n";
//				myfile << countOriginalToken << " : " << LineNumber << " : "
//						<< sourceForToken(m_token).toStdString() << "\n";

		myfile << content << "\n";

		myfile.close();
	} else
		cout << "Unable to open file";
}

int replayTrace::uniqueTimerId(string ptr) {
	static map<string, int> table;
	if (table.find(ptr) == table.end()) {
		table[ptr] = table.size();
	}
	return table[ptr];
}

int replayTrace::uniqueTimerId2(string ptr) {
	static map<string, int> table;
	if (table.find(ptr) == table.end()) {
		table[ptr] = table.size();
	}
	return table[ptr];
}

static std::map<int, int> currentTimer;
int lastTimer = -1;
void replayTrace::timerStart(int id) {

//	if (currentTimer.find(id) == currentTimer.end()) {
//		currentTimer[id] = 1;
//	} else {
//		currentTimer[id]++;
//	}
	lastTimer = id;

}
void replayTrace::timerEnd(int id) {
//	if (currentTimer.find(id) == currentTimer.end()) {
//		assert(false);
//	}
//	currentTimer[id]--;
//	if (currentTimer[id] == 0) {
//		currentTimer.erase(id);
//	}
	lastTimer = -1;
}
int replayTrace::getLatestTimer() {
	return lastTimer;
}
std::map<int, int> replayTrace::getCurrentTimers() {
	return currentTimer;
}

//static vector<int> nestedEvents;

//void replayTrace::startEvent(int id) {
//	nestedEvents.push_back(id);
//}
//void replayTrace::endEvent() {
//	stringstream ss;
//	for (vector<int>::iterator it = nestedEvents.begin();
//			it != nestedEvents.end(); ++it) {
//		ss << "End  : " << *it << "\n";
//	}
//	ss << "--------------------------------------------------";
//	replayTrace::logToFile("event", ss.str());
//	nestedEvents.clear();
//}
//int replayTrace::eventWillBeNested() {
//	if (nestedEvents.size() == 0) {
//		return -1;
//	} else {
//		return nestedEvents[0];
//	}
//}
